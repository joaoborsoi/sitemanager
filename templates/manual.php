<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Untitled</title>
</head>

<body>

<hr unique width="50%" align="left"><br>
   Roadsend SiteManager<br>
   Copyright (c) 2001 Roadsend, Inc.&nbsp;<a href="www.roadsend.com">www.roadsend.com</a><br>
 <hr unique width="50%" align="left"><br>
 <br>
  This source file is subject to version 1.0 of the Roadsend Public<br>
  License, that is bundled with this package in the file <br>
  LICENSE, and is available through the world wide web at <br>
  <a href="roadsend.com/license/rpl1.txt">http://www.roadsend.com/license/rpl1.txt/</a><br>
 <br>
 <hr unique width="50%" align="left"><br>
  <b>Author(s):</b><br>
   <ul>
       Shannon Weyrick <a href="mailto:weyrick@roadsend.com">weyrick@roadsend.com</a><br>
       Jonathan Michel <a href="mailto:pym@roadsend.com">pym@roadsend.com</a><br>
   </ul>          
 <br>

<b>TABLE OF CONTENTS</b><br>
<hr unique width="50%" align="left"><br>
<li><a href="manual.php#overview">Overview</a>
<li><a href="manual.php#about">About Roadsend</a>
<li><a href="manual.php#features">Features</a>
<li><a href="manual.php#installation">Installation</a>
<ul>
<li><a href="manual.php#instructions">Instructions</a>
<li><a href="manual.php#security">Security Concerns</a>
</ul>
<li><a href="manual.php#directory">Site Directory Layout</a>
<li><a href="manual.php#root">SiteManager Root Class</a>
<li><a href="manual.php#configuration">Global Configuration System</a>
<li><a href="manual.php#globals">SiteManager Globals</a>
<li><a href="manual.php#smobject">The SM_object Base Class (and $directive variable)</a>
<li><a href="manual.php#directive">Directive Scripts (pages)</a>
<li><a href="manual.php#module">The Module System</a>
<li><a href="manual.php#template">The Template System</a>
<li><a href="manual.php#codeplate">CodePlates</a>
<li><a href="manual.php#smartforms">SmartForms</a>
<li><a href="manual.php#entities">Input Entities</a>
<li><a href="manual.php#filters">Filters</a>
<li><a href="manual.php#session">Session Management</a>
<li><a href="manual.php#database">Database Connectivity</a>
<li><a href="manual.php#error">Error Handling / Debugging</a>
<li><a href="manual.php#routines">Misc Routines</a>
<li><a href="manual.php#extending">Extending SiteManager</a>
<li><a href="manual.php#studio">SiteManager Studio (GUI)</a>
<li><a href="manual.php#faq">FAQ</a>
<li><a href="manual.php#help">Getting More Help</a>
</ul>

<hr unique width="50%" align="left"><br>
<a name="overview"><b>OVERVIEW</b></a>
<hr unique width="50%" align="left"><br>

This is the Roadsend SiteManager Documentation for 2.1x Development Series.<br>
<br>
To meet the demands of today's client, web developers require Rapid Site <br>
Development tools: <br>
<blockquote><br>
<ul>
	<li>to separate development tasks between developers and designers
    <li>to increase product lifecycle and decrease maintenance time 
   <ul>
		<li>     through reuse of code modules:
		<li>     using a comprehensive and integrated set of libraries
		<li>     using third party modules
		<li>     using stock modules and templates
		<li>     using a high-level development approach	
   </ul>
</ul>   
</blockquote>
Although PHP is a rapidly growing web development language, no existing PHP 
tool effectively integrates a set of core libraries into an IDE geared toward 
the implementation of shared, reusable code.
<p>
PHP Developers need to be able to:
<ul>
	<li>    access a large library of powerful components    
	<li>    customize library code to meet requirements of their development team
	<li>    utilize a Graphical User Interface for Rapid development and prototyping
	<li>    avoid wasting in-house development time 
	<ul>
		<li>        duplicating existing functionality 
		<li>        understanding component internals
	</ul>
</ul>
<p>
SiteManager creates an environment in which powerful modules may be created
and shared easily between projects and developers by implementing standards for 
creating, configuring, and maintaining modules of code. These modules are 
integrated into the site through a powerful template system that keeps the 
final HTML layout separate from the code that powers it. Database connectivity, 
user session management and SmartForm technology combine to give the developer 
a comprehensive set of libraries with which to develop their modules.
<p>
Benefits of Site Manager<br>
<blockquote>
    A set of common web application libraries designed to work together  
      including:
      
<ul>
	<li>        Database connectivity
	<li>        SmartForms
	<li>        User session management
	<li>        Security Routines
	<li>        Authentication Routines
	<li>        Data Manipulation Routines
	<li>        Debug and Error Handling Routines
</ul>
   
    A visual project explorer to keep code modules, layouts and web pages 
      organized<br>
    Tools to help create, configure, use, and share modules without editing 
      module code<br>
    Store configuration information for workstations, projects and modules.
</blockquote>
<hr unique width="50%" align="left"><br>
<a name="about"><b>ABOUT ROADSEND</b></a><br>
<hr unique width="50%" align="left"><br>

    Roadsend, Inc. is a programming company based in Fishkill, NY.
    Our website is <a href="www.roadsend.com">www.roadsend.com</a>, and you can reach us at
    <a href="mailto:info@roadsend.com">info@roadsend.com</a>
    <p>
    Roadsend made the switch from PERL to PHP as its main web programming
    language back in 1998 around the time 3.0 was coming out. From then on it
    was obvious PHP would make a quick rise to the top as the best (and of
    course freely available) website development language.
    <p>
    Over the course of 3 years, Roadsend developed many websites and libraries
    to support them. Over the past year it had become necessary to
    switch to a template/module system. SiteManager has grown out of that 
    system.
    <p>
    To view a production website currently using code based off of SiteManager,
    checkout <a href="www.mindedge.com">www.mindedge.com</a>. They are a great 
	online resource for finding online education. Roadsend developed the site, 
	and you can see in use all of the libraries mentioned here. You can also look at  
    <a href="boston.mindedge.com">boston.mindedge.com</a>, 
	<a href="iesi.mindedge.com">iesi.mindedge.com</a> and 
	<a href="unibr.mindedge.com">unibr.mindedge.com</a> to see just 
    how easy it is to create cobrands with the template system.
    <p>
    The primary authors of Roadsend SiteManager are:
    <ul>
        Shannon Weyrick <a href="mailto:weyrick@roadsend.com">weyrick@roadsend.com</a><br>
        Jonathan Michel <a href="mailto:pym@roadsend.com">pym@roadsend.com</a><br>
	</ul>	
<hr unique width="50%" align="left"><br>
<a name="features"><b>FEATURES</b></a><br>
<hr unique width="50%" align="left"><br>

   <a name="module"><b>Module System</b></a><br>
   <blockquote>
        A powerful module system allows programmers to design reusable modules
        of code to power their websites. Modules may be those designed in house,
        or downloaded from a third party. The module system allows for run time
        configuration, which lets the same module do different things depending
        on the context in which it was called. The system includes Style Sheet
        configuration functionality, which changes the output style of the 
        module on the fly. This increases the usability of the module by 
        keeping the functionality the same, but changing the way the output
        looks when necessary.
   </blockquote>      

   <b>Template System</b><br>
    <blockquote>
        The template system is not merely a "search and replace" variable 
        system. It allows one to define arbitrary "areas" in which to place 
        modules of code. There is no limit to the number of "areas" in a 
        template, and also no limit to the number of modules that may be added 
        to any one area. This powerful system allows Web Designers to design the 
        way the site looks without having to worry about the functionality. Once 
        a template is defined, the programmer then uses it to create "pages"
        by adding modules to the template areas. In addition, you may add other
        templates to areas in any template, allowing for unlimited depths of
        subtemplates.
    </blockquote>
   <b>CodePlates</b><br>
    <blockquote>
        CodePlates take the template system a step further. CodePlates solve the 
        problem of having to duplicate code when you use a similar 
        template/module layout scheme for multiple pages. Instead of copying and 
        pasting the same code over and over, which would load the same templates 
        and modules and put them in the same areas, CodePlates can be built. 
        They are templates in themselves, but already have certain modules 
        associated with their areas.
    </blockquote>
    <b>SmartForms</b><br>
     <blockquote>
        To truly create a "dynamic" website, input must be continually gathered
        from the user. This creates the need for a structured, consistent way
        to setup an input form, validate input, and gather the information. 
        Creating form elements and validating input by hand is always slow and 
        tedious.
        <p>
        SmartForms were created as a way to rapidly create an input form using 
        predefined input entities, validate the input using predefined filters, 
        and gather the information in a structured way. SmartForms includes
        all the standard input types (textbox, checkbox, select list, etc) as
        well as some not so standard, but still often used types (date select,
        combo box, state list, country list, etc). It also includes many 
        predefined filters (required, number, email, credit card, phone, 
        regexp + more) that you can attach to any input entity. You can attach
        any number of filters to any input entity. And, SmartForms are 
        "data-aware" we include entities such as DB Select list, which
        populates a  list from a database. All entities and filters
        are pluggable modules of code, where all you have to do to add
        a new entity to the system is drop it into the correct directory.
        <p>
        When using a SmartForm, you never have to worry about creating elements
        or checking for input validity by hand. You don't even have to worry 
        about outputting the form -it's all handled by SmartForms. All you
        do is define the form, run it, and ask it if all input was valid.        
     </blockquote>
   <b>Database Connectivity</b><br>
     <blockquote>
        Roadsend SiteManager uses the PEAR Database abstraction layer for 
        database connectivity. It is included with all version of PHP (v4 and 
        above). It allows connectivity to a number of different databases 
        through a common API. 
        <p>
        Even though SiteManager uses PEAR, it does include a middle layer
        of functionality that allows the PEAR DB system to be integrated within 
        the SiteManager system. That is, the database settings and creation of 
        the database object are within the framework for SiteManagers Global 
        Configuration System.
     </blockquote>
    <b>Global Configuration System</b><br>
     <blockquote>
        All libraries in SiteManager use a Global Configuration System based on
        XML configuration files. This allows configuration of all parts of 
        SiteManager to be done on a global level, and overridden on a site 
        level. Configuration files are standard (readable) XML text files.
     </blockquote>
    <b>User Session Management</b><br>
     <blockquote>
        SiteManager includes it's own user session management routines. The 
        session manager can use a database to store information on sessions. 
        Sessions are kept alive either through the use of cookies or a session 
        ID passed within all internal links to the site. You never have to worry 
        about including the session variables by hand when creates links, as 
        this is done for you both within FORMs and regular HTTP links. You may 
        add as many persistent variables to the session as you would like, even 
        if you don't use a database to store session information.
     </blockquote>
    <b>Support Routines</b><br>
     <blockquote>
        Many supporting routines are included with SiteManager to help create 
        your web application. These include:
        
	<ul>
		<li>            Authorization Routines
		<li>            Data Manipulation Routines
		<li>            Error Handling / Debug Routines
		<li>            Security Routines
		<li>            Misc Support Routines 
	</ul>       
    </blockquote>
    <b>Object Oriented Framework</b><br>
    <blockquote>
        The SiteManager framework is extensively object oriented, to promote 
        reusable code and easy maintenance.
    </blockquote>
    <b>Open Source</b><br>
    <blockquote>
        Roadsend SiteManager is an Open Source project. The entire system may 
        be distributed and used freely. The source is freely available for all 
        to learn, review and change. Please read the license agreement 
        available in the file LICENSE in the /doc directory, or on the web at
        <a href="http://www.roadsend.com/license/rpl1.txt">http://www.roadsend.com/license/rpl1.txt/</a>
        <p>
        We actively encourage all hackers to help us maintain and grow the 
        project. Please see CONTRIBUTE in the /doc directory for more 
        information.
        <p>
        We hope to create a community of developers activity creating Modules,
        SmartForm Entities and Filters, and helping maintain the project
        to ensure SiteManager's continued growth as the leading Web Application
        Framework for PHP.
        
	</blockquote>
<hr unique width="50%" align="left"><br>
<a name="installation"><b>INSTALLATION</b></a><br>
<hr unique width="50%" align="left"><br>

    Please see the INSTALL file in /doc directory for installation 
    instructions.
	<p>
    The INSTALL file also has instructions on settings up new projects
    that use SiteManager, and will walk you through a couple of example
    sites.
    <p>
    Please note, we use the following file extensions in SiteManager:
    <p>
<table border=1 cellpadding=2 cellspacing=2>
	<tr><td>.inc</td><td>code library</td></tr>
	<tr><td>.mod</td><td>SiteManager Module</td></tr>
	<tr><td>.tpt</td><td>SiteManager HTML Layout Template</td></tr>
	<tr><td>.stp</td><td>SmartForm FORM Layout Template</td></tr>
	<tr><td>.xsm</td><td>XML Configuration/Definition File</td></tr>
	<tr><td>.php</td><td>Web Page Loadable PHP Script (Directive Script)</td></tr>
	<tr><td>.cpt</td><td>CodePlate Object</td></tr>
</table>
	<p>
    <hr unique width="50%" align="left"><br>
    <a name="security"><b>SECURITY CONCERNS</b></a><br>
    <hr unique width="50%" align="left"><br>
    
    We recommend you make all of the above files unreadable by the webserver,
    except ".php" files. The rest are not meant to be seen by users.
	<p>
    The following line will accomplish this Apache:
    <ul>    
    # dont allow access to SiteManager files<br>
    	
	<ul>
		<li>        Order allow,deny
		<li>        Deny from all
	</ul>
    </ul>    

<hr unique width="50%" align="left"><br>
<a name="directory"><b>DIRECTORY LAYOUT</b></a><br>
<hr unique width="50%" align="left"><br>

    Every site run by SiteManager has a common directory structure (usually
    found in /var/www/ or /home/httpd/html/)<br>
    <table border=1 cellpadding=2 cellspacing=2>
     	<tr><td>/</td><td>(root)</td></tr>                     
    <tr><td>/admin</td><td>(admin / config directory)</td></tr> 
        <tr><td>/modules</td><td>(local site modules)</td></tr> 
        <tr><td>/templates</td><td>(local site templates)</td></tr> 
        <tr><td>/smartForms</td><td>(local site smartForms)</td></tr> 
        <tr><td>/config</td><td>(local site configuration files)</td></tr> 
        <tr><td>/codePlates</td><td>(local site codePlates)</td></tr> 
    	<tr><td>/home</td><td>(main page directory)</td></tr> 
    </table><br>
    All files in /admin should be protected from reading by the webserver.
    None of these files located in this directory or it's subdirectory should
    be readable by a user requesting them through the web. The best way to stop
    this is the stop the webserver from reading the file types found in these 
    directories.
    <p>
    See Installation Security Concerns for more information.
	<p>
    SiteManager itself contains the following directory structure (usually 
    found in /usr/local/lib/php/)<br>
    <table border=1 cellpadding=2 cellspacing=2>
    
    	<tr><td>/</td><td>(root)</td></tr>                       
    	<tr><td>/bin</td><td>(binary files/scripts for maintenance and automation)</td></tr>
    	<tr><td>/config</td><td>(configuration files, ie globalConfig.xsm)</td></tr>
    	<tr><td>/doc</td><td>(documentation, LICENSE, etc)</td></tr>
    	<tr><td>/lib</td><td>(main lib directory)</td></tr>
        <tr><td>/sfFilters</td><td>(SmartForm filters)</td></tr>
        <tr><td>/sfInputEntities</td><td>(SmartForm input entities)</td></tr>
    	<tr><td>/modules</td><td>(SiteManager core modules)</td></tr>
    	<tr><td>/skeletonSite</td><td>(skeleton site used for template when creating new site)</td></tr>
    	<tr><td>/smartForms</td><td>(sitewide SmartForms)</td></tr>
    	<tr><td>/tables</td><td>(MySQL reference tables)</td></tr>
    	<tr><td>/templates</td><td>(SiteManager core templates)</td></tr>
    	<tr><td>/testSite</td><td>(Fully functional test site)</td></tr>
	</table><br>

<hr unique width="50%" align="left"><br>
<a name="root"><b>SITEMANAGER ROOT CLASS</b></a><br>
<hr unique width="50%" align="left"><br>

    When a project includes 'siteManager.inc' for use in the site, an object 
    named $SM_siteManager is created automatically. You will use this object to
    perform high level functions in your site, such as:
    
<ul>
	<li>        Loading a Module
	<li>        Loading Layout Templates
	<li>        Creating a Database Connection
	<li>        Starting Sessions
	<li>        Accessing the Global Configuration System
	<li>        Debugging
</ul>

    Here is the list of relevant methods you may access from $SM_siteManager:
    <p>
    loadSite($file)
    <blockquote>
        Call this method to load in local configuration settings for your site.
        This is usually done in admin/common.inc
        See the next section, Global Configuration System, for more information
        on this. Also, have a look at the example common.inc in 
        skeletonSite/admin/ directory.
        <p>
        Please Note: The  tag in this file must match the
        $SM_siteID you define in admin/common.inc!
    </blockquote>
    &loadModule($moduleFile, $moduleName='')<br>
    <blockquote> 
        Call this method when you wish to load and create an instance of a 
        module. This function will look through your module paths (as defined in 
        the Global Config System) for the file named $moduleFile (automatically 
        adding a ".mod" extension). If the actual class name of the module is
        different from the file name of the module, you must specify it's name
        in $moduleName. For example, if the module file name you're loading is
        'testModule.mod', but the module class is 'myTestModule', you must use 
        the following command:
        <p>
        $module = &$SM_siteManager-loadModule('testModule','myTestModule');
        <p>
        If you wish to load a module that's contained in the file 'testModule',
        and the class name is also named 'testModule', you may use:
        <p>
        $module = &$SM_siteManager-loadModule('testModule');
        <p>
        The one exception are modules that are prefixed with "SM_". 
        These modules are part of the SiteManager package, such as "rawHTML". 
        The file is located in /modules/rawHTML.mod, but the class name 
        is SM_rawHTML. Normally you would have to solve this discrepancy by 
        using both parameters of loadModule(), but in this case, it will 
        find and load them correctly without. For example, to use the SM_rawHTML 
        module you need simply:
		<p>
        $htmlMod = &$SM_siteManager-loadModule('rawHTML');
		<p>
        PLEASE NOTE: The object returned from this function is return BY 
        REFERENCE: you MUST use the reference assignment operator ( =& ) as in 
        the examples above in order to configure and use the modules properties.
        <p>
        See the Module System section and example scripts for more information.
		</blockquote>
    &rootTemplate($fName) <br>
    	<blockquote>
        Call this method to set the root HTML layout template for use. It will
        search the template paths (as defined in the Global Config System) for
        the template file $fName (automatically adding '.tpt' extension for you)
        <p>
        Example:
        <p>
        $rootTemplate = &$SM_siteManager-rootTemplate('basicTemplate');
        <p>
        PLEASE NOTE: The object returned from this function is return BY 
        REFERENCE: you MUST use the reference assignment operator ( =& ) as in 
        the examples above in order to configure and use the template 
        properties.
        <p>
        See the Template System section and example scripts for more 
        information.
		</blockquote>
    &loadTemplate($fName) <br>
    	<blockquote>
        Call this method to load an HTML layout template. You should only call
        this method directly if you plan of loading a template to place inside
        of another template (see the Templates section for more information on
        templates within templates). Otherwise, you should be using 
        rootTemplate() to load a single root template. This method will search
        template paths. Note, this method returns a reference.
		</blockquote>
    &loadCodePlate($fName) <br>
    	<blockquote>
        Load a CodePlate for use. CodePlates are extensions of regular 
        tempaltes -see the CodePlate section for more information. Note, 
        this method returns a reference. Will search the codePlates paths.
		</blockquote>
    dbConnect()<br>
    	<blockquote>
        This method sets up a database connection for the rest of the system to
        use. It will use the database settings found in the Global 
        Configuration System.
        <p>
        This method does not take parameters, nor does it return anything. It 
        should simply be called once (usually in common.inc) to create the 
        database connection.
        <p>
        See the Database Connectivity section for more information.
    	</blockquote>
    startSessions()<br>
    	<blockquote>
        This method starts sessions. It should be used when you wish to keep 
        certain variables persistent between links and forms. You should call 
        this method in common.inc, and then define what variables you would like 
        to keep persistent using the function below (addPersistent).
        <p>
        See the Sessions section for more information.
		</blockquote>
    addPersistent($varName)<br>
    	<blockquote>
        Use this method to specify that a variable should be kept persistent 
        throughout the system. It should be called only after startSessions() is 
        called. For example, if you would like to keep the variable "userName" 
        persistent for a user:
        <p>
        $SM_siteManager-startSessions();<br>
        $SM_siteManager-addPersistent('userName');
        <p>
        All session-aware modules will now keep this variable intact 
        automatically, as soon as it gets a value.
		<p>
        Note, you may also setup persistent veriables in the Local Site 
        Configuration file. They should be in the "pVars" SECTION -see the 
        Global Configuration System section, or the 
        testSite/admin/config/localConfig.xsm example file for more information.
		</blockquote>
    completePage()<br>
        <blockquote>
        This method should be the last function called in a Directive Script. 
        It runs all modules, collects their output, and inserts in into the 
        templates.
        <p>
        See the Directive Script section for more information, or look at the 
        example scripts in skeletonSite/ and testSite/ directories.
		</blockquote>
    fatalDebugPage()<br>
    	<blockquote>
        This method will generate a very informative debug page. It should be 
        called in place of completePage() -ie, AFTER all modules have been 
        added and configured.
		<p>
        Instead of showing the final HTML output as completePage() does, it will
        show all information on how the page WOULD have been generated. That is, 
        it will list all Global Configuration data, what templates are in use, 
        which modules are loaded, how the modules are configured, what 
        SmartForms are in use in these modules, and how the SmartForms are 
        configured.<br>

<hr unique width="50%" align="left"><br>
<a name="configuration"><b>GLOBAL CONFIGURATION SYSTEM</b></a><br>
<hr unique width="50%" align="left"><br>

    SiteManager uses two main configuration files to configure paths,
    flags, and other options. 
    <p>
    Every site that uses SiteManager is assigned a SITEID ($SM_siteID). This ID
    is used by the configuration system to determine which settings to use
    for this site. It is also used by the debug system when outputting errors 
    and warnings.
    <p>
    $SM_siteID should be assigned in admin/common.inc BEFORE siteManager.inc is 
    included.
    <p>
    LOCAL settings are read from a file in the sites admin directory of every 
    site. The file is usually 'localConfig.xsm' but is definable in common.inc
	<p>
    GLOBAL settings are the default settings that should be used for all sites 
    that use siteManager. If a configuration value requested for a particular 
    SITEID doesn't exist, the GLOBAL setting will be used instead (if found).
    <p>
    When siteManager.inc is included, the global settings are read in by the 
    $SM_siteManager object. It reads it into a variable accessible through
    $SM_siteManager-siteConfig. It is an object of type SM_config 
    (smConfig.inc).
    <p>
    admin/common.inc will generally tell $SM_siteManager to read in the LOCAL
    settings using the $SM_siteManager-loadSite() method. 
    <p>
    Configuration is basically a list of key/value pairs, broken up into 
    various sections. Currently the sections are:<br>
    <table border=1 cellpadding=2 cellspacing=2>
    	<tr><td>dirs</td><td>directory settings</td></tr>                
        <tr><td>db</td><td>database settings</td></tr>
        <tr><td>flags</td><td>misc flags</td></tr>
        <tr><td>sessions</td><td>session settings</td></tr>
        <tr><td>pVars</td><td>persistent variables</td></tr>
    </table>    
    The easiest way to explain the settings is to take a look at the XML file 
    that actually stores the variables:
	<p>
        
        
            
            
                                    
            
                    
        
            
            
                
        
                        
            
                
    
     
    This excerpt from globalConfig.xsm shows 1) how the XML structure is laid 
    out 2) how variables are defined. 
    <p>
    To access a variable from $SM_siteManager-siteConfig in module code, you 
    would use one of two function calls:
    <blockquote>
        $SM_siteManager-siteConfig-getVar($section, $name, $mergeGlobal=false)
        	<p>
            This function should be used when a single variable is required. 
            For instance, to retrieve the main lib directory from section 
            'dirs', you would use:
            <p>
            $libDir = $SM_siteManager-siteConfig-getVar('dirs','libs');
            <p>
            $libDir would then be a single string variable that contains the 
            main lib directory.
            <p>
            Notice you can list VAR name's more than once in the config file 
            (such as the 'modules' VAR in the example above). When this happens, 
            an array is created. For example, to get the two 'modules' 
            directories from the global config you would use:
            <p>
            $moduleDirs = $SM_siteManager-siteConfig-getVar('dirs','modules');
            <p>
            $moduleDirs is now an array of values that contains the two values 
            listed in the configuration file.
            <p>
            The $mergeGlobal option is used to tell the SM_config object 
            whether you want to just use the LOCAL settings, or both the GLOBAL 
            and LOCAL settings. Usually, you want the local settings to override 
            global settings, but sometimes you'd like to have both. Use this 
            option to control that. When set to TRUE, you will receive an array 
            of values that consists of all values set for that variable name in 
            both GLOBAL and LOCAL context.
			<p>
        $SM_siteManager-siteConfig-getSection()
        	<p>
            Use this function to retrieve an entire section at a time. For 
            example:
            <p>
            $dbSettings = $SM_siteManager-siteConfig-getSection('db');
            <p>
            This will return a hash that can be used in the following way:
            <p>
            $dbType = $dbSettings['dbType'];<br>
            $pers   = $dbSettings['persistent'];
            <p>
            This function automatically merges GLOBAL and LOCAL settings.
            </blockquote>
    You may also SET a variable through the global configuration system. This 
    can be used to override a value read in from the config file.
    <p>
        $SM_siteManager-siteConfig-setVar('dbSettings','dbType','mysql');
    <p>
    Note that you CANNOT change GLOBAL variables this way. <br>   

<hr unique width="50%" align="left"><br>
<a name="globals"><b>SITEMANAGER GLOBAL VARIABLES</b></a><br>
<hr unique width="50%" align="left"><br>

    The following are global variables and objects setup and used by 
    SiteManager:
    <p>
    $SM_siteManager
    <blockquote> 
        The root SiteManager class. Use this class for high level control of
        the site, such as Creating a Database Connection, Loading a Module,
        Loading the Root Template, or Starting Sessions.    
	</blockquote>  
    $SM_siteID<br>
	<blockquote> 
        This is the 'site ID' for your project. It should be defined in 
        admin/common.inc. It is used for configuration and debug purposes.
        It is arbitrary, but all capital by convention. An example siteID
        is 'TESTSITE'.
        <p>
        Note that this siteID MUST match the  tag from your
        projects config/localConfig.xsm file!
	</blockquote>
    $SM_siteName<br>
    <blockquote> 
        This is the 'site name' for your project. It is arbitrary freetext,
        used for debugging purposes. 
        <p>
        An example $SM_siteName is 'Test Site 1'
	</blockquote> 
    $SM_sessionVars<br>
    <blockquote> 
        A string that contains the current valid session parameters, useful for
        using as GET parameters to a script. All registered persistent 
        variables will show up here.
	</blockquote> 
    $SM_debugOutput<br>
    <blockquote>
        An array with debug information. Each time a call to SM_debugLog is 
        made, and new item is added to the array. See the ERROR HANDLEING
        section for more information.
	</blockquote>
    $SM_rootDir<br>
    <blockquote>
        If you don't want to or can't place the SiteManager directory
        into the PHP include path, you can set this variable in siteManager.inc
        Point it to the main SiteManager root directory, and it will be able
        to find it's library files.
	</blockquote>
<hr unique width="50%" align="left"><br>
<a name="smobject"><b>THE SM_OBJECT BASE CLASS</b>(and the $directive variable)</a><br>
<hr unique width="50%" align="left"><br>

    Several SiteManager objects extend the SM_object base class.
    <p>
    This class gives a common set of methods used mainly for a common 
    configuration system, database access, and session access.
    <p>
    All classes that extend this class receive the follow methods:
    <table>
    	<tr><td>addDirective($key, $val)</td><td>add a key/value pair</td></tr>        
    	<tr><td>configure($cDir)</td><td>configure the object all at once</td></tr>               
    	<tr><td>loadConfig($configFile)</td><td>load configuration data from XML file</td></tr>        
    	<tr><td>dumpInfo()</td><td>debug class info dump</td></tr>                      
    </table>    
    They also include the following class variables:
    <blockquote>
        $directive<br>
        $sessionH<br>
        $dbH
    </blockquote>
    $directive is a standard associative array -ie, a list of key/value
    pairs. It is used to configure the object. addDirective, configure, and
    loadConfig all work to setup this array.
    <p>
    Therefore, any object that inherits this class, such as SM_modules, can
    be configured through:
    <p>
    $module-addDirective('key','value');
    <p>
    This system is used extensively throughout SiteManager, and we recommend 
    using it as an alternative to class variables when creating modules and 
    other code. There are benefits to doing so:
    <ol>
        <li> it's easier to create reusable code, because classes that have
           several parent classes are easier to maintain and configure    
        <li> common, extendable system for defining a configuration scheme
           for objects
	</ol>
    SM_object can also contain a reference to the current database handle and
    session handle, stored in $dbH and $sessionH. This is used by the module 
    system, SmartForms, etc. as a common way to access these features.
    <p>
    Therefore, when you need to create a class that needs access to the 
    SiteManager Global Configuration system, the SiteManager Database session, 
    or the SiteManager user session, you should extend this class.<br>

<hr unique width="50%" align="left"><br>
<a name="directive"><b>DIRECTIVE SCRIPTS and the Module/Template System Overview</b></a><br>
<hr unique width="50%" align="left"><br>

    Directive scripts are the link between Modules and Templates.
    <p>
    They are described here so that as you go through the Modules and Template
    section, you can better understand the parts they play.
    <p>
    A directive script is an actual "page". It will be the script that the
    webserver loads first when an URL is accessed.
    <p>
    The directive script is generally very short. It's job is to:
    <ol>
    	<li> Include the site's 'common.inc', which in turn sets up the configuration
       for this site and includes the SiteManager libraries
    	<li> Load the modules to be included in this page
    	<li> Configure the modules
    	<li> Load the template to be used as output for this page
    	<li> Add the modules to the correct areas in the correct order
    	<li> Output the page
    </ol>
    Generally, depending on the amount of configuration to be done to the 
    modules, this can be done in 10-15 lines of code.
	<p>
    When the command is given to "run" the page and output it, the following 
    happens:
    <ol>
    	<li> All modules run through their configuration routines (moduleConfig())
    	<li> All modules run through their "think" routines (moduleThink())
    	<li> HTML and JavaScript output is collected from each module
    	<li> The output from the modules is placed in the proper areas in the template
	</ol>
    A sample, annotated directive script follows (yes this is the entire page):
    <p>
    <hr unique width="50%" align="left">
    <p>
    // include site configuration file, which includes siteManager libs
    include('../admin/common.inc');
	<p>
    // create a new module. notice, returns a reference!!<br>
    $mod1 = &$SM_siteManager-loadModule('rawHTML','SM_rawHTML');<br>
    <p>
    // configure the module<br>
    $mod1-addDirective('output','Hello World');
	<p>
    // create root template. notice, returns a reference!!<br>
    $layout1 = &$SM_siteManager-rootTemplate("basicTemplate");
    <p>
    // add our module to area 'areaOne'<br>
    $layout1-addModule($mod1, "areaOne");
    <p>
    // finish display<br>
    $SM_siteManager-completePage();
	<p>
    <hr unique width="50%" align="left">
	<p>
<hr unique width="50%" align="left"><br>
<a name="module"><b>THE MODULE SYSTEM The SM_module Object</b></a><br>
<hr unique width="50%" align="left"><br>

    <hr unique width="30%" align="left"><br>
    <b>OVERVIEW</b><br>
    <hr unique width="30%" align="left"><br>

    Modules are pieces of code that get executed so that their output is placed 
    into a template. All modules are extended from the same base class, and 
    have a common structure.
    <p>
    The base class for all objects is SM_module
    <p>
    Extending this class to create a module gives that object several important
    features:
    <ol>
        <li> It allows the new object to be added to a template, where it's output
           will be added into an area of the template
        <li> It sets up the object to receive input from SmartForms and other
           modules
        <li> It allows the object to create and use a SmartForm
        <li> It allows javascript code to be taken from this module and included
           with the main JavaScript output section of the template this module
           will show up in
        <li> It allows code to use the database connection and session information
           available through SiteManager
    </ol>
    All modules, upon creation, are assigned a Module Prefix. This prefix is 
    used by the system to differentiate between modules. It is used, for 
    example, to avoid variable naming conflicts when using modules that 
    configure for themselves variables of the same name. The module prefix is 
    used behind the scenes, and generally should not have to be referenced at 
    all.
    <p>
    There is only one required method that must be implemented for the module to 
    work:
    <p>
    moduleThink() &nbsp;      the "brains" of the module
    <p>
    Although not required, generally a module will also implement:
    <p>
    moduleConfig() &nbsp;     configure the module <br>   
    
    <hr unique width="30%" align="left"><br>
    <b>MODULE CONFIGURATION</b><br>
     directives<br>
     incoming variables<br>
     style sheet variables<br>
     loadConfig()<br>
    <hr unique width="30%" align="left"><br>

    For a module to be reusable in different contexts, it should be able to 
    change it's output and/or the way it looks when it outputs on the fly. This 
    means it needs to be configurable at run time through directive scripts 
    (pages). This is done in the moduleConfig() method.
	<p>
    There are three main configuration options to be done in moduleConfig(). Of 
    course, the module designer may do any configuration code they need in this 
    space -what's important is that this function is executed BEFORE the 
    moduleThink function. Also, all modules on a page are ALL configured before 
    they are run.
	<p>
    Here is a sample moduleConfig() method:<br>
    
    <hr unique width="50%" align="left"><br>

    function moduleConfig() {<br>
	<blockquote>
        // load base configuration files<br>
        $this-loadConfig("moduleSettings");&nbsp;    // load some directives from XML file
		<p>
        // directives<br>
        // these are defined and overridden by DIRECTIVE SCRIPTS<br>
        $this-addDirective('tableName','');		// table name to operate on<br>
        $this-addDirective('maxVisibleSize',30);	// max size of a form element (default)<br>		
        $this-addDirective('dataField','');		// field to operate with (primary key)<br>
        $this-addDirective('siteName','Test Site'); // site name
        <p>
        // variables this module will use for configuration (these are all made up and arbitrary)<br>
        // thee are setup by GET and POST vars<br>
        $this-addInVar('widgetType',0); &nbsp;       // which widget type should we use?<br>
        $this-addInVar('format','basic'); &nbsp;     // which "format" to use
        <p>
        // styles we will use in this module<br>
        $this-addStyle('linkText'); &nbsp;           // style used when outputing HTTP links<br>
        $this-addStyle('baseText');  &nbsp;          // style used for plain text
        <p>
        // if this module requires database functionality, <br>
        // check for a proper connection<br>
        if (!isset($this-dbH)) {<br>
            SM_fatalErrorPage("this module requires a database connection, which wasn't present!");<br>
        }<br>
	</blockquote>
    } <br> 

    <hr unique width="50%" align="left"><p>


    <b>DIRECTIVES -</b><br>
    <p>
    Often a module designer wishes to let the user configure how the module 
    should act, such as whether it should output in a table, whether it should 
    be run in "advanced" or "basic" mode, whether it should end the script after 
    a form loads or transfer to another page, etc (all arbitrary examples). 
    These settings are done through the $directive variable and addDirective() 
    method.
    <p>
    Settings directives uses the functionality from SM_object. For example,
    to add a directive to a module:
    <p>
    $mod1-addDirective('siteName','Test Site');
    <p>
    Using a directive in the module code is as easy as referencing the 
    $directive variable. For example:
    <p>
    $this-say("Welcome to ".$this-directive['siteName'].'!');
    <p>
    is much more reusable in a module than hardcoding:
    <p>
    $this-say("Welcome to Test Site!");
    <p>
    It allows others to use the module and configure it without ever having to 
    look at the internals of the way it works.
	<p>
    You may also use the loadConfig() method inherited from SM_object. It allows 
    you to have an extenal file that defines directives. If you are going to use 
    loadConfig(), you should call this first in moduleConfig(), so that later 
    addDirectives will override those loaded in the external config file.
    <p>
    Using loadConfig(), you can setup certain directives that ALL modules should 
    use -such as "tableBorder" or other cosmic options. Then, simply call 
    loadConfig() in each of your modules. This way you only have to define those 
    options in one place, and when they need to change, you can change them all 
    in one place. See the SM_object section for more information on loadConfig()
	<p>
    Directives are different from INVARS in that they are meant to be 
    configured and overridden by a programmer who is using the module to create 
    a page.
	<p>
    INVARS -
	<p>
    If the module is to take any input (ie, from HTTP_POST_VARS of 
    HTTP_GET_VARS) it must declare which variables it will be looking for using 
    the class variable $inVar and method addInVar()
    <p>
    For example, if a module is expecting a "widget type" to be passed to it,
    through he "widgetType" variable such as:
    <p>
    http://localhost/testScript.php?widgetType=5
    <p>
    then it should declare that it will use this variable in moduleConfig():<br>
    $this-addInVar('widgetType',0);
    <p>
    The second parameter is the default value to use, if one wasn't passed to 
    the script. If this parameter is left blank, the default value is blank.
	<p>
    The reason variables are declared is so that the module system can know 
    which modules are looking for which variables, and identify conflicts. Also, 
    it is used by the SmartForm system to allow form variables to be passed to 
    modules easily.
	<p>
    NOTE: Variables passed to scripts should NOT be accessed directly, as in:    
    global $widgetType;<br>
    or -<br>
    $HTTP_GET_VARS['widgetType'];
    <p>
    This is incorrect use the getVar() function as follows:<br>    
    $widgetType = $this-getVar('widgetType');
    <p>
    Again, using this system lets the module system handle naming conflicts, 
    among other things.
	<p>
    INVARS are different from Directives in that they are meant to be variables 
    that change based on how the script was accessed, ie they get setup either 
    through GET or POST variables that were passed to the form.
    <p>
    STYLES -
    <p>
    In order to promote reusability, and module should be able to output itself
    in a manner that's configurable without touching the internals of the 
    module.
    <p>
    One way to do this is by using CSS Style Sheets when outputing HTML. This 
    allows a user to use the module and change the way it looks simply by 
    defining certain style sheet elements in their template. This way, they 
    never have to touch the module code.
	<p>
    All styles to be used in a module should be declared, in much the same way 
    as INVARS, but with the addStyle() method:
    <blockquote>
        $this-addStyle('linkText'); // style used when outputing HTTP links
    </blockquote>
    A module has a hash called $style. It contains a key/value pair that links 
    two properties:
        
<ol>
	<li>        Internally named style sheet element
	<li>        Externally named style sheet element
</ol>
        
    This is done so that the module can reference a particular style sheet 
    element name that may change later, when the module is configured. For 
    example, if the module is to output a link:
    <p>
    $this-say('getStyle('linkText').'"Test Link');
	<p>
    It can reference it's 'internal' name of linkText -but later on, when the 
    module is configured, the actual style sheet name it outputs may change. For 
    example, then the user configures the variable in the Directive Script 
    (page), they may wish to change "linkText" to be "redLink", which is what 
    they have defined in their style sheet:
    <p>
    $mod1-setStyle('linkText','redLink');
    
        
    <hr unique width="30%" align="left"><br>
    <b>MODULE "THINK" CODE</b><br>
    <hr unique width="30%" align="left"><br>

    The required method to implement for a module is called moduleThink(). This 
    method is what gives the module functionality. Here is a sample:
    <p>
    function moduleThink() {<br>
	<blockquote>
        // output what's contained in the directive 'output'
        $this-say($this-directive['output']);
	</blockquote>
    }
    <p>
    When this module is run, it will buffer output. NO MODULE SHOULD EVER USE 
    'echo' or 'print'! This is because the module must save it's output for 
    later when it will be placed into the correct area of the template.
    <p>
    ** NOTE ** A Module should never 'echo' output!
    <p>
    Instead of echoing output, use the method say() that's part of the 
    SM_module class:
    <p>
    $this-say("This is module output");
    <p>
    DO NOT use:
    <p>
    echo "This is module output";
    <p>
    If you're testing a module, and you see strange output that shows up before 
    everything else on the page, chances are you've put an echo or print 
    statement within moduleThink.
    <p>
    Another (special) method of output is the sayJS() function. This is used 
    for the module to output JavaScript code. It requires it's own function so 
    that the javascript code shows up in the correct JavaScript area of the 
    template. sayJS() works just like say(). You do NOT have to include any 
     tags when using sayJS, the module system does this for you.
    See the template section for more information on this.
    <p>
    Other than that, the moduleThink code may do anything you'd like. You of 
    course have the full power of PHP and all it's extensions at your disposal. 
    Here is a list of SiteManager helper functions that will be useful when 
    designing modules:<br>
    <table border=1 cellpadding=2 cellspacing=2>
    	<tr><td>$this-say();</td><td>use to buffer module output</td></tr> 
    	<tr><td>$this-sayJS();</td><td>use to buffer javascript output</td></tr>
    	<tr><td>$this-getVar();</td><td>use to access a variable declared as an INVAR</td></tr>
    	<tr><td>$this-newSmartForm();</td><td>create a new SmartForm for this module (see SmartForm section)</td></tr>
    	<tr><td>$this-getStyle();</td><td>retrieve a style sheet element name</td></tr>
    	<tr><td>$this-directive[];</td><td>retrieve a directive</td></tr>
    </table><br>
    <hr unique width="30%" align="left"><br>
    <b>DATABASE FUNCTIONALITY</b><br>
    <hr unique width="30%" align="left"><br>
    
    Accessing a database in the module's is easy. When you extend the SM_module 
    class, a class variable called $dbH is available automatically. Either this 
    variable is NULL, in which case there is currently no available database 
    connection, or it is a reference to the current database connection. There 
    is no need to make a connection by hand in a module.
    <p>
    If your module *requires* access to a database, you should check for 
    connectivity in moduleConfig, using:

    <hr unique width="50%" align="left"><br>   
        // if this module requires database functionality,<br> 
        // check for a proper connection<br>
        if (!isset($this-dbH)) {<br>
        &nbsp; SM_fatalErrorPage("this module requires a database connection, which wasn't present!");<br>
        }<br>
    <hr unique width="50%" align="left"><br>   
    
    Then, in moduleThink(), you are guaranteed access to the database. You may 
    use $this-dbH as a normal PEAR Database object. For example:<br>
    
    <hr unique width="50%" align="left"><br>   
        $rh = $this-dbH-simpleQuery("SELECT * FROM testTable");<br>
        if (DB::isError($rh)) {<br>
        &nbsp; SM_fatalErrorPage("There was a problem with the query!");<br>
        }<br>
        while ($rr = $this-dbH-fetchRow($rh)) {<br>
            $this-say($rr['firstName'].'');<br>
        }<br>
        $this-dbH-freeResult($rh);<br>
    <hr unique width="50%" align="left"><br>   

    You've now just written a data ware module that can be used in any 
    SiteManager system, and on any database system that PEAR supports.

    <hr unique width="30%" align="left"><br>   
    <b>SESSION FUNCTIONALITY</b><br>
    <hr unique width="30%" align="left"><br>

    Much like the database functionality, session functionality is contained 
    automatically in the variable $this-sessionH
    <p>
    To make your modules "session-aware", you should use the hLink function 
    contained with the session handler.
    <p>
    hLink($link, $text, $class='', $extra='')
    <blockquote>
        hLink will make a  link to $link, around text $text. It will 
        use a style of $class (from your $this-style array) if specified. 
        $extra will place extra text into the  tag if you need it (such as 
        TARGET="")
        <p>
        hLink will automatically append the current session variables onto your 
        link (even if $link contains variables of it's own).
    </blockquote>    
    It should be used in the following way:
    <blockquote>
        $this-say($this-sessionH-hLink("newScript.php?user=1","Link To New Script"));
    </blockquote>    
    Using hLink instead of hard coded $this-say("    your module session-aware.

	<p>
    Things to remember:
    <blockquote>
        moduleThink() should never exit, unless it's a fatal error
        if using header("Location: "), make sure you exit(); afterwards
        don't use echo or print, use $this-say()
        use $this-sessionH-hLink() for A HREF links to make your module 
          "session-aware"
	</blockquote> 
    Take a look at the /modules directory for some examples of modules.

    <hr unique width="30%" align="left"><br>
    <b>MODULES WITHIN MODULES</b><br>
    <hr unique width="30%" align="left"><br>
    
    Inside of the moduleThink method, you can of course use any standard PHP 
    functions, as well as function libraries you may have written and included. 
    But another way to increase the reusability of your code is to write 
    SiteManager modules that are to be used specifically inside of other 
    modules. The benefits of doing this instead of writing normal functions and 
    included them is that you receive the same Global Configuration System 
    abilities as other modules, plus you have access to the current Database and 
    Session handles.
	<p>
    To use a module within a module:
    <p>
    // declare $SM_siteManager to be global
    global $SM_siteManager;
 	<p>
    // load the module through SM_siteManager like normal (make sure you've<br>
    // defined it as global though!)<br>
    $testModule = $SM_siteManager-loadModule("testModule");
    <p>
    // call the run() method on the module<br>
    $tOutput = $testModule-run();
    <p>
    // gather it's output, and say() it into your own<br>
    $this-say($tOutput);<br>
    $this-sayJS($testModule-getJS());
	<p>
    The code for the module to be loaded can be any normal module code. However, 
    when designing a module to be run within other modules, make sure you turn  off
    the outputInTable directive -otherwise you'll get two module tables inside 
    of one module.
	<p>
    // (in sub-module's moduleConfig())<br>
    $this-addDirective('outputInTable',false);<br>    


<hr unique width="50%" align="left"><br>
<a name="template"><b>THE TEMPLATE SYSTEM The SM_layoutTemplate object</b></a><br>
<hr unique width="50%" align="left"><br>

    The template system is used to separate the module code from the way 
    it will look to the end user. 
    <p>
    Roadsend SiteManager allows you to take a "template" of static HTML,
    define arbitrary "areas", and define which modules should go in which
    areas, and in what order.
    <p>
    You may have as many areas as you like, and any number of modules in
    any area. You can also add other templates to areas (see Templates in
    Templates).
    <p>
    A "template" is really just a static HTML file with a custom 
    tag placed in them:<p>
    
            define an area
        directly replace this tag with a 
                                              module
                      replace with JavaScript code 
                                              collected from modules
    @@sessionVars@@                         special tag. directly replaces 
                                              itself with current session 
                                              variables
    <p>
    Here is a simple example template:<br>
    
    <hr unique width="30%" align="left"><br>
    
    
    
    
    
    
    
    
    <b>Test Link With Session Variables</b><br>

    
     
    
    
    

    
    
    <hr unique width="30%" align="left"><br>

    This simple template sets up 2 areas, a javascript area, and a footer 
    module call.
    <p>
    All area names are arbitrary. Here we've used "areaOne" and "areaOne", we 
    could just as easily have used "leftArea", "rightArea". The area name will 
    be reference in the directive script, when you are adding modules.
    <p>
    The javascript tag is optional, but if it appears it should be in the 
     section of the HTML. This will allows modules and smartforms to 
    plugin javascript functionality.
    <p>
    @@sessionVars@@ is a direct search-and-replace convenience tag, so one does 
    not have to create a module merely to output static links with session 
    variables included.
	<p>
    If an area is listed in a template, but a module is not added to it by the
    time the page is displayed:
    <ul>
        1) The template system will try to create a module by the same name as 
           the area name. If it is successful, it replaces that areaname with 
           the module output. This only happens if the global flag<br>
           loadBlankAreaAsModule is set to "true" (see Global Config System)
        2) If it was not successful, the areaName is cleared and ignored -it 
           will not show up in the final output of the page.<br>
	</ul>
    A template is loaded in the following fashion (usually in a directive 
    script):
    <p>
    $template = &$SM_siteManager-rootTemplate('templateName');
    <p>
    This will search your template path for the requested template, and load it
    once found.
    <p>
    From there you may add a previously loaded and configured module to the 
    template using the addModule() method:
    <p>
    $template-addModule($modulePointer, 'areaOne');
    <p>
    This adds the module created into $modulePointer to area areaOne, and will
    cause it's output to show up in the corresponding area.
	<p>
    Finally, the completeDisplay() method is called to run the modules and
    output the finished page.
	<p>
    See the Directive Script section for more information on creating a 
    template and adding modules to it.<br>


    <hr unique width="50%" align="left"><br>  
    <b>TEMPLATES WITHIN TEMPLATES (sub-templates)</b><br>
    <hr unique width="50%" align="left"><br>
    
    Templates can act recursively that is, you can have templates within 
    templates.
    <p>
    To do this, you use the SM_siteManager root class function loadTemplate().
    Then add the template that gets returned into another template through the 
    addTemplate() method (in much the same way as adding a module). 
    For example (directive script excerpt):
    
    <hr unique width="50%" align="left"><br> 

    // create root template. notice, returns a reference!!<br>
    $layout1 = &$SM_siteManager-rootTemplate("welcome");<br>
    <p>
    // add our module to root template area<br>
    $layout1-addModule($mod1, "leftDirectory");
    <p>
    // load subtemplate, returns reference!<br>
    $layout2 = &$SM_siteManager-loadTemplate("subTemplate");
    <p>
    // add module to subtemplate<br>
    $layout2-addModule($mod2, "areaOne");
	<p>
    // add sub template to root template<br>
    $layout1-addTemplate($layout2, "intro");
	<p>
    // finish display<br>
    $SM_siteManager-completePage();<br>
    
    <hr unique width="50%" align="left"><br>

    Every loaded template has an addTemplate() function, which allows you to
    add an entire template (modules and all) into an area in another template.
    When a root template displays, if it encounters a template inside one of
    it's areas, and it run recursively, gathering and returning output.
    <p>
    Computer resources are the only limit to the depth of recursion.
    <p>
    Also note, you can use CodePlates instead of templates, which also act
    recursively. See the next section on CodePlates.

<hr unique width="50%" align="left"><br>
<a name="codeplate"><b>CODE PLATES</b></a><br>
<hr unique width="50%" align="left"><br>

    CodePlates take templates a step further. They are templates that have
    modules associated with them.
    <p>
    Often, you'll want to use a template that has the same (or almost the same)
    module layout in several pages. For example, on a typical website you
    might have one main layout that includes a menu on the top, another menu
    in a left column, and the content in the center changes depending on the 
    page loaded. One way to do this in SiteManager is to create a directive
    script for each page, and load and add the same menu modules on each page,
    then load a different module to place in the center section. But what if
    you want to change which module gets loaded in the left column? You would
    have to go through each directive script to change it.
    <p>
    CodePlate solve this problem. To handle the above example, you would create
    a code plate that always has the menu in the top and left areas of the 
    template. Then, instead of loading the template and modules in the directive
    scripts, you would load just the code plate, and the center module. Place
    the center module in the code plate, and you're all done. Since the menu
    modules were automatically added by the code plate, you don't have to worry
    about them (although you can still configure them if you wish, per directive
    script).
    <p>
    CodePlates are actually a subclass of the SM_layoutTemplate class -they
    have all the same methods and functionality of a regular template. You
    associate a CodePlate with an HTML template, and you can add modules to
    the areas in the same way. However, CodePlates have one extra method, and 
    this is the method that defines which modules get loaded, and where they
    get placed.
    <p>
    To use a code plate, you must create a class that extends the SM_codePlate
    class. This piece of code may be in an external file, and loaded in
    your sites common.inc
    <p>
    Here is an example codePlate (taken from the testSite):<br>
    
    <hr unique width="50%" align="left"><br>

    /**<br>
     * a test code plate<br>
     *<br>
     */<br>
    class mainLayout extends SM_codePlate {<p>
    <blockquote> 
    
        /**<br>
         *<br>
         * define which modules i want where in my template<br>
         *<br>
         */<br>
        function codePlateConfig() {<br>

            // -THE FOLLOWING THREE LINES ARE REQUIRED FOR ALL<br>
            // -CODE PLATES TO WORK!
    		<p>
            // MUST declare this as global for access<br>
            global $SM_siteManager;
    		<p>
            // REQUIRED: define which HTML template we're using<br>
            $this-htmlTemplate = "welcome";
    		<p>
            // parse the template<br>
            $this-parseTemplate();
    		<p>
            // -THE REST OF THE FUNCTION CONFIGURES AND PLACES<br>
            // -MODULES IN THE TEMPLATE. IT IS IDENTICAL TO<br>
            // -TO THE WAY IT IS DONE IN DIRECTIVE SCRIPTS,<br>
            // -EXCEPT $this WHEN ADDING MODULES (see below)
			<p>
            // load modules<br>
            $mod2   = &$SM_siteManager-loadModule('directory','dirMod');<br>
            $mod6   = &$SM_siteManager-loadModule('docViewer');<br>
            $mod7   = &$SM_siteManager-loadModule('directory','dirMod');
            <p>
            // configure the modules<br>
            $mod2-addDirective('format','extended');<br>
            $mod2-addDirective('centerInTable',false);<br>
            $mod2-addDirective('bullet','');<br>
            $mod6-addDirective('centerInTable',false);<br>
            $mod7-addDirective('format','compact');
            <p>
            // set styles<br>
            $mod2-setStyle('viewSource','yellowViewSource');<br>
            $mod6-setStyle('viewSource','yellowViewSource');<br>
            $mod7-setStyle('viewSource','yellowViewSource');
            <p>       
            // add our modules into their areas<br>
            $this-addModule($mod2, "leftDirectory");<br>
            $this-addModule($mod6, "leftDirectory");<br>
            $this-addModule($mod7, "bottomDirectory");
    </blockquote>
    &nbsp; }
    <p>	
    }<br>


    <hr unique width="50%" align="left"><br>

    The method codePlateConfig() must be overridden when creating a codePlate.
    <p>
    As you can see, there are three lines of code that are required. They are:
<ol>
	<li>    declare SM_siteManager global for access
	<li>    declare which HTML template to use
	<li>    parse the HTML template
</ol>
    
    After these three things are done, the rest of the function is used to 
    load, configure, and add the modules. This is done in the same way as
    a directive script, except the $this qualifier must be used when adding
    modules (remember, a codePlate is subclassed from a regular SM_layoutTemplate,
    so it's actually adding modules to itself)
    <p>
    Once you have a CodePlate defined and included in your site, you use it in
    your directive scripts much like a template. For example (this is a full
    directive script):<br>
    
    <hr unique width="50%" align="left"><br>
    
    // include load site common<br>
    include ("../admin/common.inc");
    <p>
    // load a module<br>
    $mod1   = &$SM_siteManager-loadModule('detailedForm');
    <p>
    // load code plate as root template<br>
    $layout = &$SM_siteManager-rootTemplate("mainLayout.cpt");
    <p>
    // add form module into codeplate<br>
    $layout-addModule($mod1, "intro");
    <p>
    // finish display<br>
    $SM_siteManager-completePage();<br>

    <hr unique width="50%" align="left"><br>

    As you can see, you can set a CodePlate to be the rootTemplate, in the 
    same way as setting a normal template. You can also add modules to a 
    CodePlate in the exact same manner.
    <p>
    In general, you can use a CodePlate in the same way as a regular template.
    That is, you can have CodePlate within CodePlates, templates within CodePlates,
    CodePlates within templates, etc.

<hr unique width="50%" align="left"><br>
<a name="smartforms"><b>SMARTFORMS</b></a><br>
<hr unique width="50%" align="left"><br>

    With SmartForms, you define a form by telling it exactly what you need and 
    how the information needs to be formatted. SmartForms handles all other
    aspects of the form. 
    <p>
    SmartForm Feature List:
    <ol>
        <li> Generation of all HTML tags and other code, including all form 
           elements, the table the form appears in for layout, and javascript 
           functions.
        <li> Handles "enter information-submit-correct information-submit" 
           loop automatically, highlighting which elements need to be corrected
           and why           
        <li> Handles checking for valid input based on a list of filters for 
           each for entity
        <li> Ability to use a template for form output, so the layout of the
           form elements are to your exact specification
        <li> Library of common Input Entities: Check Box, Combo Box, Country 
           List, Date Picker, DB Combo Box, DB Select, Static Text, Radio, 
           Select List, State List, Switch Box, Text Area, Text
        <li> Ability to create new drop in Input Entities and filters, or use 
           entities and filters designed by others by doing nothing more than 
           placing a .inc file in the correct directory.
        <li> Library of Filters: Credit Card, Email, User Function, Number, 
           Phone, Regular Expression, Required, Variable Comparison
        <li> Can add any number of filter requirements to any entity
        <li> Filters and entities can contain optional JavaScript output for
           increased functionality. Javascript will go to correct JavaScript
           section of output template
        <li> Load a precreated SmartForm from an XML configuration file
    <p>     
    Coming Soon:
    <p>
        <li> Create SmartForms in our GUI editor, save them out, and load them 
           into you modules with one function call.
	</ol>
    Now that you know a little more about what a SmartForm can do, here's how 
    to use them.<br>
    
    <hr unique width="30%" align="left"><br>
     <b>USAGE</b><br>
    <hr unique width="30%" align="left"><br>

    Quick Usage:
    
<ol>
	<li>    Create New Smart Form
	<li>    Configure The Form
	<li>    Add Elements
	<li>    Configure Elements (setup options, add filters)
	<li>    Run Form
	<li>    Data Verification Test
</ol>

    <hr unique width="30%" align="left"><br>
    <b>CREATE A NEW FORM</b><br>
    <hr unique width="30%" align="left"><br>

    First is to create a new SmartForm. This is done in a module through the
    newSmartForm() method:
    <p>
    $myForm = &$this-newSmartForm();
    <p>
    $myForm is now a SmartForm object. NOTE it is returns a reference! You MUST 
    use the reference assignment operator ( =& ) as above.
        
    <hr unique width="30%" align="left"><br>
    <b>CONFIGURE FORM</b><br>
    <hr unique width="30%" align="left"><br>

    You can configure SmartForm directives through the standard addDirective() 
    method call.
    <p> 
    You can, for example, tell the SmartForm that the value of the Reset button 
    on the form should be "RESET FORM":
    <p>
    $myForm-addDirective('resetButton','RESET FORM');
	<p>
    Since a SmartForm extends SM_object, you can use loadConfig() to load 
    directives from an external file. See the SM_object section for more info.
	<p>
    A full commented list of directives is available in the library 
    lib/smartForm.inc

    <hr unique width="30%" align="left"><br>
    <b>ADD ELEMENTS</b><br>
    <hr unique width="30%" align="left"><br>

    If you are loading the form from an external file, you can
    skip this step, as the elements will be created for you
    from the XML configuratio file. See the LOADING A SMARTFORM
    section below.
	<p>
    Next, add some elements. This is done through the add() method.
    <p>
    $myForm-add('varName','Input Title','text');
    <p>
    There are many parameters, but the first three are all that are required. 
    They are: 
    1) The variable name to use in the form<br>
    2) The title to use, which will tell the user what they are expected to 
       enter<br> 
    3) The type of input entity to create (in this case, a text box)
    <p>
    The add() function returns a reference to the input entity created. We can 
    use this, for example, if we added a select list:
    <p>
    $selectList =& $myForm-add('sList','Select A Color','select');<br>
    $selectList-addOption('Red');<br>
    $selectList-addOption('Blue');<br>
    $selectList-addOption('Green');<br>
    <p>
    NOTE: add() returns an object reference! You must use the reference 
    assignment operator ( =& ) when fetching the return value from this
    function.

    <hr unique width="30%" align="left"><br>
    <b>LOADING A SMARTFORM</b><br>
    <hr unique width="30%" align="left"><br>
    
    Instead of manually add()'ing the elements to the SmartForm in your
    module, you can call the SM_smartForm::loadForm() method. It will
    read in an SmartForm XML definition file and configure the form
    as indicated in that file. 
    <p>
    For now, the files must be created by hand. By the next release
    we should have tools available to automate this process.
    <p>
    For an example SmartForm definition file, see 
    testSite/admin/smartForms/smTest.xsm
    <p>
    In that example, the form and it's directives are configured all
    externally.
    <p>
    Once you call the loadForm() method in your module, you can still
    configure and override all settings, using the addDirective() 
    method as normal.
    <p>
    For example:<br>

    <hr unique width="50%" align="left"><br>   
	<blockquote>
        // create form<br>
		$myForm = &$this-newSmartForm();
        <p>
        // load from external file<br>
        $myForm-loadForm("smTest.xsm");
        <p>
        // override configuration directives that<br>
        // were loaded in from the XML form
        <p>
        // one of the variables loaded in from the file<br>
        // was 'userName'. we'll configure that here.<br>
        $myForm-setArgs('userName',array('size'='20'));<br>
        $myForm-addFilter('userName','number','Must be a number');
		<p>
        // run the form<br>
		$myForm-runForm();<br>
        $this-say($myForm-output());<br>
    </blockquote>   
    <hr unique width="50%" align="left"><br> 

    <hr unique width="30%" align="left"><br>
    <b>CONFIGURE ELEMENTS</b><br>
    <hr unique width="30%" align="left"><br>

    You may also use the input entity returned from the add() call to setup 
    directives for this entity:
    <p>
    $myPW = $myForm-add('passWord','Pass Word','text');<br>
    $myPW-addDirective('passWord',true);&nbsp;   // this tells the text entity to be of type PASSWORD
                                            // so it will show asterisk when someone types<br>
    $myPW-addDirective('size','20'); &nbsp;      // make it size 20 characters                                            
        
	<p>
    Adding filters to an element is just as easy. To make the above field match 
    a phone number pattern, we would:
    <p>
    $myPW-addFilter('phone','Must be in the form of a phone number');<br>
    $myPW-setFilterArgs('phone',array('format'='(XXX) XXX-XXXX'));    
	<p>
    This would add the phone filter, and if the filter didn't pass, it would 
    display the above message to the user as to why it didn't.
    <p>
    You may add any number of filters to any entity.<br>
    
    <hr unique width="30%" align="left"><br>
    <b>RUN FORM / DATA VERIFIED CHECK</b><br>
    <hr unique width="30%" align="left"><br>
    
    Once the form is defined and configured, you must run the form:
    <p>
    $myForm-runForm();
    <p>
    This applies all filters and does some background magic. After the form has 
    been run, you will know if the data has been verified or not. The check 
    would be:
    <p>
    if ($myForm-dataVerified()) {
    <blockquote>
        // the form has all required input, and the input has been validated
        // against all filters
		<p>
        // send email, write data to database, or do whatever necessary with<br>
        // form data. access the variables with -getVar function:<br>
        $this-say($myForm-getVar('passWord').' was passed and verified!');
        <p>
        // or, dump all form input<br>
        $this-say($myForm-dumpFormVars());
	</blockquote>
    }<br>
    else {<br>
    <blockquote>
        // the form either does not have all required input, or some filters<br>
        // did not pass
        <p>
        // display the form<br>
        $this-say($myForm-output());
	</blockquote>
    }

	<p>
    Here is a simple, complete SmartForm (when copied and pasted into
    a siteManager module, it should run correctly):<br>
    
    <hr unique width="50%" align="left"><br>

    <blockquote>
        // create form<br>
        $myForm = &$this-newSmartForm();
        <p>
        // configure form<br>
        $myForm-addDirective("requiredStar","***");
		<p>
        // add a new text entity<br>
        $myForm-add('userName','User Name','text',true,'',array('size'='20','maxLength'='25'),0);
        <p>
        // add a couple more<br>
        $myForm-add('firstName','First Name','text',false,'',undef,1,"RIGHT");<br>
        $myForm-add('phone','Phone Number','text',false,'',undef,2);
        <p>
        // add filter<br>
        $myForm-addFilter('phone','phone','Not a valid phone number',array('format'='(XXX) XXX-XXXX'));
		<p>
        // run the form<br>
        $myForm-runForm();
		<p>
        // was data verified? <br>       
        if ($myForm-dataVerified()) {<br>
        <blockquote>
            // yes!<br>
            $this-say("thanks for the input!!");<br>
        </blockquote>    
        }<br>
        else {<br>
        <blockquote>
            // no! output the form<br>
            $this-say($myForm-output());<br>
        </blockquote>    
        }<br>
	</blockquote>
    <hr unique width="50%" align="left"><p>
    

    <hr unique width="30%" align="left"><br>
    <b>FORM TEMPLATES</b><br>
    <hr unique width="30%" align="left"><br>
    
    SmartForms by default will generate the table and layout for the form. 
    There are many options for formatting the output of this table (see 
    smartForm.inc library file for more information).
 	<p>
    However, sometime you want your form to be in a specific layout 
    SmartForms handles this with templates.
   	<p>
    Here is a sample SmartForm template:<br>
    
    <hr unique width="50%" align="left"><br>
    

    {userName.title}{userName.input}<br>
    {firstName.title}{firstName.input}<br>
    {lastName.title}{lastName.input}<br>
    {submit.Enter Data}<br>
    
    <hr unique width="50%" align="left"><br>

    You tell a SmartForm to use a template by calling the method:<br>
    $myForm-setTemplate('templateName.stp');
    <p>
    You should call this before you call runForm()
	<p>
    There are two parts to setting up the template: you must tell SmartForms
    where to place the Title and the Input for each form entity. The format
    to do so is
    <p>
    {varName.title} {varName.input}
    <p>
    "varName" should be the same variable name you used when call the add() 
    method to add your elements.
    <p>
    The {submit} tag is a special tag which will place the Submit button.
    The text that shows up after the "." in the submit tag will be the text
    on the Submit button (in the example above, the submit button would read
    "Enter Data")
    <p>

    Please note, there are many other options and configuration settings for
    SmartForms. Please reference the JavaDoc documentation in the following 
    files (see the API documentation files):
    <p>
    lib/smartForm.inc<br>
    lib/sfInputEntities.inc<br>
    lib/sfFormTntity.inc<br>
    lib/sfFilters.inc<br>
    lib/sfFilters/*<br>
    lib/sfInputEntities/*<br>

    <hr unique width="30%" align="left"><br>
    <b>SESSION AWARE FORMS</b><br>
    <hr unique width="30%" align="left"><br>
    
    All forms are automatically "session-aware". That is, if persistent 
    variables have been setup using the SiteManager session code, they will 
    automatically be included as HIDDEN fields in the form when necesary.<br>

<hr unique width="50%" align="left"><br>
<a name="session"><b>SESSION MANAGEMENT</b></a><br>
<hr unique width="50%" align="left"><br>

    Sessions are used to maintain "states" for users when they access your
    site. For example, if a client accesses your site and enters a user name,
    it would be nice if next time they came to the site, they didn't have to
    enter the username again. This is where the SiteManager sessions system
    comes in.
	<p>
    There are several ways to configure the sessions system, but there is 
    always one common element: the persistent variables. These are the variables
    you would like to keep persistent between sessions for each user. For example,
    "userName" could be a persistent variable that gets defined in a SmartForm.
    If you make this variable persistent, once the user submits the SmartForm,
    the value will keep throughout their session. To access this variable
    through a module, simply declare "userName" as a module InVar (see the
    Module section).
    <p>
    Possible ways to configure the session system:
    <ol>
      <li> Don't use database or cookies to store variables. This is the simplest
       method. In this system, all declared persistent variables are passed
       around throughout all the links on the site (through the hLink function).
       This method would let the user see all of the persistent variables in
       the URL of the script.
      <li> Use database to store variables, but don't use cookies. This method 
       will store all declared persistent variables in a database. The session
       ID is passed around in all links throughout the site (through the
       hLink function). This method would let the user see their sessionID in
       the URL links, but not the different variables being kept for their
       session.
      <li> Use database to store variables, use cookie to store session ID. This 
       is the most "transparent" way to use sessions. There is no extra
       information passed in the link URLs. All information is stored behind
       the scenes. The downside is, the user will have to have Cookies turned
       on for this to work.
    </ol>   
    DATABASE NOTICE -<br>
    <blockquote>
        Even though the session code uses the same PEAR database functionality
        as the rest of the SiteManager system, database structures and therefor
        SQL queries vary with each database type.
		<p>
        Currently, MySQL is the only tested database engine. The code is written
        in such a way as to easily be able to handle other database structures.
        If you add support for another database layout, please submit it to us.
		<p>
        The current sessions table format can be found in the tables/ directory.
	</blockquote>
    CONFIGURING SESSIONS -
    <p>
    There are several options for configuring the sessions system to work with 
    your site. Look in config/globalConfig.xsm in the "sessions" SECTION,
    for a commented list of all available options.
	<p>
    USING THE SESSIONS SYSTEM IN YOUR SITE -
	<p>
    To use the session system, simply call $SM_siteManager-startSessions() in
    your site's admin/common.inc. Then, use the -addPersistent() method to 
    tell SiteManager which variables you would like to keep persistent.
    <p>
    For Example (from common.inc):
    <p>
    // use sessions<br>
    $SM_siteManager-startSessions();<br>
    // add a variable to keep persistent<br>
    $SM_siteManager-addPersistent("userName");<br>
    $SM_siteManager-addPersistent("firstName");<br>
	<p>
    You may also set which variables you want to keep persistent in the local 
    site configuration file (localConfig.xsm). See the Global Configuration 
    System section for more info.
    <p>
    Now, when "userName" or "firstName" get passed to a script (either through 
    command line GET or from a SmartForm) the value will be remembered and
    passed around between links and forms in the site.
	<p>
    To accomplish this, modules need to me "session-aware". See the Module
    System section for more information on this.<br>

<hr unique width="50%" align="left"><br>
<a name="database"><b>DATABASE CONNECTIVITY</b></a><br>
<hr unique width="50%" align="left"><br>

    Database connectivity in SiteManager uses the PEAR database abstraction 
    layer. This keeps the interface to the database routines the same, while 
    allowing multiple database engines to power the site.
    <p>
    Connecting to a database should be done through the $SM_siteManager object.
    Doing it this way allows other parts of the SiteManager system (modules,
    smart forms, input entities, etc) to be database aware.
    <p>
    Connecting to the database using the database settings found in the Global
    Config System is a simple matter of calling $SM_siteManager-dbConnect()
    in your sites common.inc. See skeletonSite/admin/common.inc for an example.    
    <p>
    Once connected, you may directly access the PEAR database object using
    $SM_siteManager-dbH
    <p>
    This is rarely done, however, since most of the database work is done 
    inside of modules. See the Module section on how to make a module database 
    aware.
    
    <hr unique width="30%" align="left"><br>
    <b>DATABASE CONNECTION SETTINGS></b><br>
    <hr unique width="30%" align="left"><br>
    
    The settings that may be defined in the config files are:<p>
    
        
            
            
            
            
            
            
        

    This may be defined in EITHER the globalConfig.xsm or per site in 
    localConfig.xsm
    <p>
    Note that you can define either (userName, passWord, and hostName) OR use 
    DSN. If DSN is available, it will always use that.
	<p>
    dbType should match up to one of the valid PEAR database types. See the 
    PEAR DB documentation for more information on that.
	<p>
    For more help with the PEAR Database layer, see http://pear.php.net or 
    /usr/local/lib/php/DB.php and /usr/local/lib/php/DB/*

<hr unique width="50%" align="left"><br>
<a name="error"><b>ERROR HANDLING / DEBUGGING</b></a><br>
<hr unique width="50%" align="left"><br>

    Error handling and debugging is in it's early stages currently. We have
    many features planned that we were not able to implement for this release.
    <p>
    Currently, the main functions provided and used are SM_fatalErrorPage and
    SM_debugLog.
    <p>
    SM_fatalErrorPage($msg)<br>
    <blockquote>
        It takes one argument, a message, which will end the current script 
        session with a fatal message. It will also display the current debug 
        log output.<br>
    </blockquote>    
    SM_debugLog($msg, $obj=NULL)<br>
    <blockquote>
        This is used for non-fatal debug output. $msg is the message to log,
        $obj is the calling object, if any. If it's specified, the object's
        class name will be logged.<br>
	</blockquote>
    <hr unique width="50%" align="left"><br>
    
    Other functions available for debugging:<br>
	<table border=0 cellpadding=2 cellspacing=2>
    	<tr><td>SM_dumpVars()</td><td>global function that dumps variables in</td></tr>      
    	<tr><td></td><td>HTTP_POST_VARS and HTTP_GET_VARS</td></tr>                              
    	<tr><td>$smartForm-dumpFormVars()</td><td>call this SmartForm method to dump value of<br>
                                  all form variables</td></tr>
    	<tr><td>$SM_module-dumpInVars()    call this SM_Module method to dump value of<br> 
                                  all declared inVars</td></tr>
    	<tr><td>$SM_siteManager-siteConfig-dumpConfig()</td><td><br>
                                dump current Global Site Configuration values</td></tr>
                               
    	<tr><td>$SM_siteManager-fatalDebugPage called in place of completeDisplay() to</td><td><br> 
                                      view all current configuration values for<br> 
                                      all templates, modules, etc.</td></tr>
	</table><br>
    SmartForms contains it's own debug output log. If flags-sfDebug is true
    in Global Config, you should be able to view the source of an page
    that includes a directive script to see the debug output from a form.
	<p>
    Planned is a system for interactively debugging modules as you write them.
    This should appear in the next few releases: see CHANGELOG in /doc<br>

<hr unique width="50%" align="left"><br>
<a name="routines"><b>MISC ROUTINES</b></a><br>
<hr unique width="50%" align="left"><br>
<blockquote>
    Other supportive routines are included in the SiteManager system. Please
    see the following library files and the API documentation for more 
    information:<br>
	<table border=1 cellpadding=2 cellspacing=2>
    	<tr><td>Authentication Routines</td><td>(auth.inc)</td></tr>
    	<tr><td>Security Routines</td><td>(security.inc)</td></tr>
     	<tr><td>Support Routines</td><td>(support.inc)</td></tr>
     	<tr><td>Data Manipulation Routines</td><td>(dataManip.inc)</td></tr>
	</table>
</blockquote>
<hr unique width="50%" align="left"><br>
<b>USING THE TEST SITE</b><br>
<hr unique width="50%" align="left"><br>

    The testSite/ directory contains a full SiteManager site used for testing.
    It is currently online at <a href="http://www.roadsend.com/siteManager">www.roadsend.com/siteManager</a>    
	<p>
    To use it fully, you need to setup a database connection. Use the following
    settings:
	<p>
    Setup MySQL:
 <ol>
	<li>     Create 'test' database if not already there.    
	<li>     GRANT ALL PRIVILEGES ON test.* TO testUser@localhost IDENTIFIED BY 'testPass'
	<li>     Create the test tables using the file test.db in tables/ directory
</ol>
	
    In testSite/admin/config/localConfig.xsm, the database settings are already setup 
    to reflect this. Other database options are possible, but left as an 
    excercise to the reader :)
    <p>
    Otherwise, if SiteManager has been setup correctly on your system, and 
    testSite is in your web document path somewhere, you may point your browser 
    at the main directory and try it out.<br>

<hr unique width="50%" align="left"><br>
<a name="extending"><b>EXTENDING SITEMANAGER</b></a><br>
<hr unique width="50%" align="left"><br>

    There are many ways to extend SiteManager, and much needed documentation on
    how to do so. A couple of options include:
    
<ol>
	<li>     Creating modules
	<li>     Creating templates
	<li>     Creating XML SmartForm Files
	<li>     Creating SmartForm Input Entities
	<li>     Creating SmartForm Filters
</ol>
    
    For now, you may look at the current modules, entities and filters for 
    example code. 
	<p>
<hr unique width="50%" align="left"><br>
<a name="studio"><b>SITEMANAGER STUDIO (GUI)</b></a><br>
<hr unique width="50%" align="left"><br>

    Roadsend is currently developing SiteManager Studio.
    <p>
    SiteManager Studio is a Graphical User Interface for creating and 
    maintaining SiteManager projects. It is being written in Borland Kylix.
    <p>
    SiteManager Studio will handle all ascpets of designing, creating, and 
    maintaining a SiteManager site, include:
    <ol>
        <li> Project Manager
        	<p>
            Keep track of all resouces for a site, including templates,
            modules, configuration files, SmartForms, CodePlates, etc.
            
        <li> Module Designer
        	<p>
            The GUI will keep track of which modules you have available 
            on your system, and allow you to easily edit them. It will
            include an interface for inserting templates of common code into 
            modules, such as creating a SmartForm.
            
        <li>Page Designer
        	<p>
            The page designer will allow you to graphically view the areas
            in your template, and drag and drop various modules into them.
            From there you can configure the added modules.
            
        <li>SmartForm Designer
        	<p>
            A graphical interface for designing SmartForms. Drag and drop
            the elements you wish to appear on your forms. Edit their 
            properties and add filters. Connect to a database, and generate
            a form based on the table layout.
            
        <li>more coming soon...
    </ol>    
    We're looking for feedback and beta testers. We'd like to know what features
    you would like to see. Please contact us at siteManager@roadsend.com for
    more information.

<hr unique width="50%" align="left"><br>
<a name="faq"><b>FAQ</b></a><br>
<hr unique width="50%" align="left"><br>

    Q: How did you generate the API documentation from your code comments?
    <p>
    A: We used PHPDoc to genearte the API documentation. It requires you to
       comment your code in a particular way, but it's well worth it.
       You can find this program at: <a href="http://phpdocu.sourceforge.net">http://phpdocu.sourceforge.net</a><br>

<hr unique width="10%" align="left"><br>

    Q: I keep seeing a message "Warning: Cannot add header information 
       headers already sent by ...". This probably means you've left a
       blank line after a module class, or in some other included library.
    <p>   
    A: It's extremely important that no library or class output anything 
       before it's time. You should never use "echo" in a module, for example,
       only the say() function.
       <p>
       Another overlooked problem is an extra newline at the bottom of a
       module, after the closing php tag ?. Check to make sure all of your
       modules and bits of code do not echo anything.
<p>
<hr unique width="10%" align="left"><br>
    
    Q: Can I install SiteManager if I don't have root access?
    <p>
    A: Yes, but you must have access to .htaccess (apache). You can
       do everything in this file you do normally through the main
       apache config file, and the main php file. The .htaccess file
       should be located in your root web document directory.
       <p>      
       It should contain the following lines:<br>

       <hr unique width="30%" align="left"><br>      
       
       # dont allow access to SiteManager files
       <blockquote></blockquote>
            Order allow,deny<br>
            Deny from all
       </blockquote> 
       
       # include sitemanager directory<br>
       php_value include_path /var/www/users/username/siteManager:/usr/local/lib/php<br>

       <hr unique width="30%" align="left"><br>
       
       Alternatively, you can leave out the php_value include_path statement,
       and edit siteManager.inc. Edit the $SM_rootDir directory to point to
       the main SiteManager root directory. If you do this, you don't need
       to have it in the PHP path.
	   <p>
       If you do it this way, you will also have to specify the full path to
       siteManager.inc in your admin/common.inc file<br>

<hr unique width="10%" align="left"><br>

    Q: I'm trying to use the $SM_siteManager class, but I keep getting errors.
    <p>
    A: Make sure it's declared as global. Remember, PHP requires you to declare
       globals variables inside of functions and classes. It doesn't automatically
       give you access to all global variables. Since SM_siteManager is global,
       if you're in a function or a function within a class, you must declare it
       global before gaining access to it:
	   <p>
       // declare SM_siteManager global<br>       
       global $SM_siteManager;<br>

<hr unique width="50%" align="left"><br>
<a name="help"><b>GETTING MORE HELP</b></a><br>
<hr unique width="50%" align="left"><br>

    The documentation we've included so far should help you get started using
    and extending SiteManager.But there's also a lot that's not covered.
    <p>
    We recommend going through the library source files to get a better 
    understanding of the library system. All files are heavily documented, 
    using a JavaDoc like layout.
    <p>
    You may also use our online resources to get help. Head over to
    <p>
    <a href="http://www.roadsend.com/siteManager">http://www.roadsend.com/siteManager</a>
    <p>
    and our SourceForge page at:
    <p>
    <a href="http://sourceforge.net/projects/sitemanager">http://sourceforge.net/projects/sitemanager</a>
    <p>
    Here you will find the latest information on SiteManager, including links
    to Message Boards, Mailing Lists, Support Emails, and the like.
    <p>
    When in doubt, you can also rip us off an email:<br>    
    <a href="mailto:siteManager@roadsend.com">siteManager@roadsend.com</a>
    <p>
    Thanks.



</body>
</html>
