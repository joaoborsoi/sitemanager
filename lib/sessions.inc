<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

// defines
define("SM_SESSION_REGEXP","/^[0-9abcdef]{32}$/");  // session regexp pattern
define("SM_MEMBER_PVAR"   ,"__SM_mpv__");           // persistent member data pvar

/**
 *
 * the SM_session class is used to maintain a user session, in which is can store
 * persistent information about the user currently on the site. the information it
 * stores is arbitrary and expandable - ie, any variable can be passed around as part
 * of the session.
 *
 * cookies are used, if requested, to automically create a valid session if the user
 * has logged in previously.
 *
 * technical notes:
 *
 *  1) $persistentVars is a hash, but the VALUE of the hash is only used for defaults
 *     this means that the value is NOT updated to reflect loaded (from database) or
 *     passed (from URL) persistent values
 *
 *
 */
class SM_session extends SM_object {

    /**  (array) a list of variables to keep persistent */
    var $persistentVars;

    /** (array) the original state of peristents as read in from a database */
    var $origDBP;

    /**
     * (array) a list of local module inVars that need to be maintained for this user session
     *         these are NOT kept as persistent variables, only for links and forms
     */
    var $moduleVars;

    /** (string) output of the java script, to be added to template javaScript section */
    var $jsOutput;

    /** (string) the current session ID */
    var $sessionID;

    /** (object) the membersystem object */
    var $memberSystem;

    /** (string) ID of the database connection we're using */
    var $dbID;

    /**
     * constructor. initialize, define persistents
     * @param &$siteConfig (obj ref) reference to Global Site Configuration object
     * @param &$dbH (obj ref) reference to database object
     */
    function __construct(&$siteConfig, &$dbH, &$dbHL) {

        // database reference
        $this->dbH      =& $dbH;
        $this->dbHL     =& $dbHL;

        // retrieve session configuration
        $this->directive            = $siteConfig->getSection('sessions');
        $this->directive['pVars']   = $siteConfig->getSection('pVars');
        if ($this->directive['dataBaseID'] == '')
            $this->directive['dataBaseID'] = 'default';
        $this->dbID = $this->directive['dataBaseID'];
        $this->directive['dbType']  = $siteConfig->getVar('db','dbType',false,$this->dbID);
        $this->directive['usePersistentMembers'] = $siteConfig->getVar('members','usePersistentMembers');

        if ((!isset($this->dbHL[$this->dbID]))&&($this->directive['useDatabase'])) {
            SM_fatalErrorPage("database connection ID '$this->dbID' does not exist. check dataBaseID in your config.",$this);
        }

        // setup local sessionID variable
        $sessionIDName          = $this->directive['sessionIDName'];
	//        global $$sessionIDName;
        $this->sessionID = $_COOKIE[$sessionIDName];

        // if we're using cookies to check for session ID, and the current sessionID
        // is either not there or invalid, try to check for the cookie and setup
        // the current session ID with it's value now
        if ( (!preg_match(SM_SESSION_REGEXP, $this->sessionID)) && (!empty($this->directive['sessionCookie']))) {

            // yes we should check the client for a session cookie
            $sC = $this->directive['sessionCookie'];
            global $$sC;
            // now if it's a valid session, use that
            if (preg_match(SM_SESSION_REGEXP,$$sC)) {
                $this->sessionID = $$sC;
                $$sessionIDName = $$sC;
                $this->debugLog("session ID obtained through cookie");
            }

        }

        // the name of the session ID variable. this one is required.
        $sN = $this->directive['sessionIDName'];
        $this->persistentVars[$sN] = '';

        if ($this->directive['usePersistentMembers']) {
            // add persistent member pvar
            $this->persistentVars[SM_MEMBER_PVAR] = NULL;
            // for security, dont let user POST or GET this variable
            $mpv = SM_MEMBER_PVAR;
            global $HTTP_POST_VARS, $HTTP_GET_VARS;
            unset($HTTP_POST_VARS[$mpv]);
            unset($HTTP_GET_VARS[$mpv]);
            unset($GLOBALS[$mpv]);
        }

        // read in persistents from siteconfig
        if (is_array($this->directive['pVars'])) {
            foreach ($this->directive['pVars'] as $pV=>$val) {
                $this->persistentVars[$pV] = $val;
            }
        }

        // update our list
        $this->_updatePersistents();

        // double check - we need at least 1 persistant (session ID)
        if (sizeof($this->persistentVars) == 0)
            SM_fatalErrorPage("persistentVars not set for session config",$this);

    }

    /*************************** PERSISTENTS **********************************/


    /**
     * add a persistent var to the list
     * @param $varName (string) the variable name to keep persistent
     * @param $varValue (string) [optional] default value for this variable
     */
    function addPersistent($varName, $varValue='') {
        $this->persistentVars[$varName] = $varValue;
        $this->_updatePersistents();
    }


    /**
     * set the current value of a persistent variable.
     * using this function will cause all futher calls to hLink, puLink and
     * SmartForms to use the value you specify to keep persistent. It will also
     * save this value as the new value in the database, if that's turned on.
     *
     * @param $varName (string) persistent variable to change
     * @param $varValue (string) [optional] value for this variable. leave blank to reset
     */
    function setPersistent($varName, $varValue='') {
        global $$varName;
        $$varName = $varValue;
        $this->_updatePersistents();
    }

    /**
     *  a private utility function to update values of "real" (global PHP) variables with
     *  either module prefixed versions of the same, or defaults if one exists and the global
     *  isn't set
     */
    function _updatePersistents() {


        // add to persistent vars variables names that turned up in HTTP_POST_VARS
        // or HTTP_GET_VARS that was in our list of persistent vars, but were
        // prefixed
        global $HTTP_POST_VARS, $HTTP_GET_VARS;
        $vList = array_merge((array)array_keys($HTTP_POST_VARS),
			     (array)array_keys($HTTP_GET_VARS));
        foreach ($vList as $v) {
            foreach (array_keys($this->persistentVars) as $p) {
                if (preg_match("/^$p\_SM[0-9]+$/",$v)) {
                    // found a match, if the regular (non prefixed) version isn't set,
                    // set it with the value of the prefixed version
                    global $$p, $$v;
                    if (!isset($$p) || $$p == '')
                        $$p = $$v;
                }
            }
        }

        // sync persisentVars hash with their global values. passed variables override defaults
        foreach ($this->persistentVars as $p => $v) {
            global $$p;
            if (!isset($$p)) {
                $$p = $v;
            }
        }

        // setup global $SM_sessionVars variable for the rest of the scripts to use
        global $SM_sessionVars;
        $SM_sessionVars = $this->getSessionVars();

    }


    /*************************** LINKS **********************************/


    /**
     * generate a url encoded list of persistents suitable for passing in a URL
     * @param $eList (array) a list of variable to exclude from the link
     * @param $retType (string) return type: FORM or URL
     */
    function _urlEncodePersistents($eList,$retType) {

        // if we're using a database, we just need sessionID since the persistents
        // are kept in the database
        if ($this->directive['useDatabase']) {
            if ($retType == 'form')
                return "<INPUT TYPE=\"HIDDEN\" NAME=\"".$this->directive['sessionIDName']."\" VALUE=\"".$this->sessionID."\">\n";
            else
                return $this->directive['sessionIDName'].'='.$this->sessionID;
        }

        // for each variable we want to keep persistent
        $pURL = '';
        foreach ($this->persistentVars as $p=>$v) {

            // ignore $v (it's a default used elsewhere)
            global $$p;
            if (isset($$p)&&($$p != '')) {
                if ( (!is_array($eList)) ||(is_array($eList) && (!in_array($p,$eList))) ) {
                    if ($retType == 'form') {
                        $pURL .= "<INPUT TYPE=\"HIDDEN\" NAME=\"".$p."\" VALUE=\"".$$p."\">\n";
                    }
                    else {
                        $pURL .= $p.'='.urlencode($$p).'&';
                    }
                }
            }
        }

        if (($retType == 'url')&&(substr($pURL, strlen($pURL)-1,1) == '&'))
            $pURL = substr($pURL, 0, strlen($pURL)-1);

        return $pURL;

    }

    /**
     * generate a url encoded list of module inVars suitable for passing in a URL
     * @param $eList (array) a list of variable to exclude from the link
     * @param $retType (string) return type: FORM or URL
     */
    function _urlEncodeInVars($eList,$retType) {

        global $SM_siteManager;

        $ivArray = $SM_siteManager->_getModuleInVars();

        if (!is_array($ivArray))
            return;

        $ivURL = '';
        foreach ($ivArray as $p) {

            // check prefix and non prefixed verion
            // prefix takes precedence over non-prefixed
            // $p will already be prefixed, we have to remove to check non
            if (preg_match('/^(.+)_SM[0-9]+$/',$p,$m))
                $nonP = $m[1];
            global $$p, $$nonP;
            if ((isset($$p)&&($$p != '')) || (isset($$nonP)&&($$nonP != ''))){

                // take prefixed over non
                if (isset($$p)&&($$p != ''))
                    $uVal = $p;
                else
                    $uVal = $nonP;

                if ( (!is_array($eList)) || (is_array($eList) && (!in_array($uVal,$eList))) )
                    if ($retType == 'form') {
                        $ivURL .= "<INPUT TYPE=\"HIDDEN\" NAME=\"".$uVal."\" VALUE=\"".$$uVal."\">\n";
                    }
                    else {
                        $ivURL .= $uVal.'='.urlencode($$uVal).'&';
                    }
            }
        }

        if (($retType == 'url')&&(substr($ivURL, strlen($ivURL)-1,1) == '&'))
            $ivURL = substr($ivURL, 0, strlen($ivURL)-1);

        return $ivURL;

    }


    /**
     * this function returns a list of variables we want added to all links and redirects
     * it will NOT return the begining ? or & needed after the scriptname
     * @param $eList (array) a list of variable to exclude from the link
     */
    function getSessionVars($eList=null) {

        $pLink  = $this->_urlEncodePersistents($eList,'url');
        $ivLink = $this->_urlEncodeInVars($eList,'url');

        if ($ivLink != '') {
            $urlLink = $pLink.'&'.$ivLink;
        }
        else {
            $urlLink = $pLink;
        }

        // update SM_sessionVars while we're here
        global $SM_sessionVars;
        $SM_sessionVars = $urlLink;

        return $urlLink;

    }

    /**
     * pull and return a list of variable names from a URL link
     * @return (array) list of variable names
     */
    function _getLinkVars($link) {

        if (strstr($link, '?')) {
            $link = substr($link, strpos($link,'?')+1);
        }

        $lvList = array();
        $vList = split('&',$link);
        foreach ($vList as $v) {
            if (preg_match('/^(\w+)=.*$/',$v,$m)) {
                $lvList[] = $m[1];
            }
        }

        return $lvList;

    }

    /**
     * this function creates a link to the desired URL, and adds the session ID if available
     * @param $link (string) the URL location to link to
     * @param $text (string) the text to link (ie, the text between <A></A>
     * @param $class (string) [optional] the CLASS (ie, style sheet element) to use for this text
     * @param $extra (string) [optional] extra data to stick into the <A> tag. may be CLASS, TARGET, etc.
     * @param $display (string) [optional] (default false) if set to true, it will echo immediately. otherwise, only returns output
     */
    function hLink($link, $text, $class='', $extra='') {

        // is link blank? use default script
        if ($link[0] == '?') {
            $link = $this->directive['defaultScript'].$link;
        }

        // include default search location
        (eregi($this->directive['scriptWithVars'],$link)) ? $connect = '&' : $connect = '?';

        // class?
        ($class != '') ? $cText = 'CLASS="'.$class.'"' : $cText = '';

        // generate variable exclude list
        $eList = $this->_getLinkVars($link);

        $output = "<A HREF=\"$link$connect".$this->getSessionVars($eList)."\" $cText $extra>$text</A>";

        return $output;

    }

    /**
     * this function returns a hidden field in a form that includes persistent variables
     * @param $eList (bool) list of variables to exclude from the HIDDENs
     * @return (string) a list of hidden INPUT's for inclusion in a form
     */
    function formID($eList) {

        return $this->_urlEncodePersistents($eList,'form').$this->_urlEncodeInVars($eList,'form');

    }


    /**
     * create a link using java popup window
     * if you use this function, make sure your template has a TYPE="javascript" SM tag
     *
     * @param $link (string) the URL to link to
     * @param $text (string) the text to link
     * @param $width (string) [optional] (default 640) the width of the popup window
     * @param $height (number) [optional] (default 480) the height of the popup window
     * @param $class (string) [optional] the CLASS to use for this text (ie, style sheet element)
     * @param $extra (string) [optional] extra data to stick into the <A> tag. may be CLASS, TARGET, etc.
     */
    function puLink($link, $text, $width=640, $height=480, $class='', $extra='') {

        // is link blank? use default script
        if ($link[0] == '?') {
            $link = $this->directive['defaultScript'].$link;
        }

        // include default search location
        if (eregi($this->directive['scriptWithVars'],$link))
            $connect = '&';
        else
            $connect = '?';

        if (empty($this->jsOutput)) {

            $this->jsOutput = "

            <SCRIPT LANGUAGE=\"JavaScript\">
            // popup window
            function pop(url,width,height) {
                    Project = window.open(url, 'Project', 'width='+width+',height='+height+',scrollbars=yes');
            }
            </SCRIPT>

            ";

        }

        if ($class != '') {
            $cText = 'CLASS="'.$class.'"';
        }
        else
            $cText = '';

        // generate variable exclude list
        $eList = $this->_getLinkVars($link);

        $output .= "<A HREF=\"javascript:pop('$link$connect".$this->getSessionVars($eList)."', '$width','$height')\" $cText $extra>$text</a> ";

        return $output;

    }


    /**
     * returns the current valid session ID
     */
    function getSessionID() {

        return $this->sessionID;

    }


    /*************************** MEMBERS **********************************/

    /**
     * check the status of the user in this session
     *
     * @return (bool) true if the current session is a valid member session
     */
    function isMember() {
        if ($this->directive['useMemberSystem'])
            return $this->memberSystem->isMember;
    }

    /**
     * check the status of the user in this session
     *
     * @return (bool) true if the current sessions is a non-member session
     */
    function isGuest() {
        if ($this->directive['useMemberSystem'])
            return $this->memberSystem->isGuest;
    }

    /**
     * retrieve information on the current logged in member
     * @return (hash) a key/value list of data from the members database
     */
    function getMemberData() {
        if ($this->directive['useMemberSystem'])
            return $this->memberSystem->memberData;
    }

    /**
     * attempt to create a member session with the username/password combo provided
     * just pass it on the to member system here, if it's active
     * @param $userName (string) the username to use for authentication
     * @param $passWord (string) the password to use for authentication
     * @return (bool) true if the user authenticated succesfully, false otherwise
     */
    function attemptLogin($userName, $passWord) {
        if ($this->directive['useMemberSystem'])
            return $this->memberSystem->_attemptLogin($userName, $passWord);
        else
            return false;
    }


    /**
     * attempt to remove a current valid member session
     * @return nothing
     */
    function attemptLogout() {
        if ($this->directive['useMemberSystem'])
            return $this->memberSystem->_attemptLogout();
    }

    /**
     * flush and reload member information from member system
     * call this if you update the member database
     * @return nothin
     */
    function flushMemberInfo() {

        if (!$this->directive['useMemberSystem'])
            return;

        global $SM_siteManager;
        unset($this->memberSystem->memberData);
        $SM_siteManager->sessionH->setPersistent(SM_MEMBER_PVAR);

        // try to recache member info
        if ($this->memberSystem->isMember) {
            $this->memberSystem->_attemptLoadMember();

            // if they are using persistent members, update var
            if ($this->memberSystem->directive['usePersistentMembers']) {
                $SM_siteManager->sessionH->setPersistent(SM_MEMBER_PVAR, $this->memberSystem->memberData);
            }
        }

    }


    /*************************** SESSION LOADING/SAVING **********************************/

    /**
     * load session variables from a database. uses PEAR
     *
     * please submit comments and patches for other databases
     *
     * see docs for tables/ directory for database format
     */
    function _loadDBSessionVars() {

        global $SM_siteManager;

        // define some configured variables we'll be using
        $sessionTable       = $this->directive['sessionTable'];

        // get database settings
        $dbType             = $this->directive['dbType'];

        // good session, load persistents
        switch ($dbType) {
        case 'pgsql':
            $SQL = "SELECT \"dataKey\", \"dataVal\" FROM \"$sessionTable\" WHERE \"sessionID\"='$this->sessionID'";
            break;
        case 'mysql':
        case 'oracle':
            $SQL = "SELECT dataKey, dataVal FROM $sessionTable WHERE sessionID='$this->sessionID'";
            break;
        default:
            SM_fatalErrorPage("you are attemping to use database sessions with an unsupported database type: ($dbType)",$this);
            break;
        }

	$rows = $this->dbHL[$this->dbID]->GetAll($SQL);

        // gather stored persistents
        foreach ($rows as $rr) {

            $varName = $rr['dataKey'];
            $dataVal = $rr['dataVal'];

            // save value for later comparision, to optimize write to database
            $this->origDBP[$varName] = $dataVal;

            // need to unserialize?
            if ($this->directive['serialize']){
                $dataVal = unserialize($dataVal);
            }

            $this->persistentVars[$varName] = $dataVal;
            global $$varName;
            $$varName = $dataVal;

        }

        // update persistents
        $this->_updatePersistents();

        $this->debugLog("valid session, persistant variables loaded from database");

        return true;

    }

    /**
     * save session variables to a database. uses PEAR.
     * see tables/ directory for database format
     */
    function _saveDBSessionVars() {

        global $REMOTE_ADDR, $SM_siteManager;

        // define some configured variables we'll be using
        $sessionTable       = $this->directive['sessionTable'];

        // get database settings
        $dbType             = $this->directive['dbType'];

        // we should have all the information we need to write a session to database
        // write session variables

        // for each variable we want to keep persistent
        foreach ($this->persistentVars as $p=>$v) {
            global $$p;
            if (isset($$p) && ($p != $this->directive['sessionIDName'])) {

                // if the data to store is an array, and the array is blank, don't
                // store it
                if (is_array($$p) && (sizeof($$p) == 0))
                    continue;

                // serialize the data?
                $dataVal = $$p;
                if ($this->directive['serialize']) {
                    $dataVal = serialize($dataVal);
                }

                // if the data we're about to write is the same as what's already
                // in the database, skip the query
                if ($this->origDBP[$p] === $dataVal)
                    continue;

                // compute hash for this sessionID+dataKey combo
                // using this hash saves us a query by replacing
                // vars that are already in the database
                $hashID = md5($this->sessionID.$p);

                switch ($dbType) {
                case 'pgsql':
                    // remove query
                    $dSQL = "
                             DELETE FROM
                                \"$sessionTable\"
                             WHERE
                                \"hashID\"='$hashID'
                            ";
                    // add query
                    $aSQL = "INSERT INTO
                                $sessionTable
                            (
                                \"sessionID\",
                                \"hashID\",
                                \"remoteHost\",
                                \"createStamp\",
                                \"dataKey\",
                                \"dataVal\"
                            )
                            VALUES (
                                '$this->sessionID',
                                '$hashID',
                                '$REMOTE_ADDR',
                                timestamp('now'),
                                '$p',
                                '".addslashes($dataVal)."'
                            )
                           ";
                    break;
                case 'oracle':
                    // remove query
                    $dSQL = "
                             DELETE FROM
                                $sessionTable
                             WHERE
                                hashID='$hashID'
                            ";
                    // add query
                    $aSQL = "INSERT INTO
                                $sessionTable
                            (
                                sessionID,
                                hashID,
                                remoteHost,
                                createStamp,
                                dataKey,
                                dataVal
                            )
                            VALUES (
                                '$this->sessionID',
                                '$hashID',
                                '$REMOTE_ADDR',
                                NOW(),
                                '$p',
                                '".addslashes($dataVal)."'
                            )
                           ";
                    break;
                case 'mysql':
                    // no remove query
                    $dSQL = '';
                    // add (ie, replace) query
                    $aSQL = "REPLACE INTO
                                $sessionTable
                            SET
                                sessionID='$this->sessionID',
                                hashID='$hashID',
                                remoteHost='$REMOTE_ADDR',
                                createStamp=NOW(),
                                dataKey='$p',
                                dataVal='".addslashes($dataVal)."'
                           ";
                    break;
                default:
                    SM_fatalErrorPage("you are attemping to use database sessions with an unsupported database type: ($dbType)",$this);
                    break;
                }

                // remove query, if necesary
                if ($dSQL != '') {
		  $this->dbHL[$this->dbID]->ExecSQL($dSQL);
                }

                // add query
                $rh = $this->dbHL[$this->dbID]->ExecSQL($aSQL);
            }
        }

    }

    /**
     * this function creates a new session for member members_idxNum, using cookies if desired
     * @return (string) new 32 character MD5'd session string
     */
    function _createSession() {

        // create a session ID. hopefully unique!
        $this->sessionID = md5(uniqid(rand()));

        // setup session ID
        $sessionIDName          = $this->directive['sessionIDName'];

        //global $$sessionIDName;
	//  $$sessionIDName = $this->sessionID;
	$_COOKIE[$sessionIDName] = $this->sessionID;

        // update persistents
        $this->_updatePersistents();

        // a little dump info for debug
        $this->debugLog("created new session (either none passed, or invalid)");

        // return for caller to use
        return $this->sessionID;

    }

    /**
     * this function is called when the page is complete, and will save all session information
     */
    function saveSession() {

        // use database structure to save and load persistent variable ?
        if ($this->directive['useDatabase']) {
            $this->_saveDBSessionVars();
        }

        // if we are to use cookies, save it here
        if ($this->directive['sessionCookie'] != '')
	  setcookie($this->directive['sessionCookie'], $this->sessionID, time()+$this->directive['cookieTTL'],dirname($_SERVER['PHP_SELF']));

    }

    /**
     * run the session system.
     * after this session exits, either a member or guest session will exist
     * @return nothing
     */
    function runSession() {

        // check for valid session ID syntax
        if (!preg_match(SM_SESSION_REGEXP, $this->sessionID)) {

            // NEW SESSION

            // session ID not passed, or invalid syntax
            // the cookie logic is done at creation of the session object,
            // so we don't have to check it here (ie, if the we are using
            // cookies and they had one, it would have been in $this->sessionID
            // already, so we know this is a new session)
            $this->_createSession();

        }
        elseif ($this->directive['useDatabase']) {

            // use database to save and load persistent variables
            // if it returns false, the sessionID was invalid and we should create
            // a new one
            if (!$this->_loadDBSessionVars())
                $this->_createSession();

        }
        else {

            // if we get here, then they have a valid sessionID, and they're not using
            // database for persistent variable storage, which means all variables must
            // be passed on the URL. since that's the case, we don't need to do
            // anything more.

        }

        // by the time we get here, we have a session ID and know if we're a guest or member

        // setup global $SM_sessionVars variable for the rest of the scripts to use
        global $SM_sessionVars;
        $SM_sessionVars = $this->getSessionVars();

        // setup the memberSystem if it's turned on
        if ($this->directive['useMemberSystem']) {

            // create the member system
            $this->memberSystem = new SM_memberSystem($this->dbH, $this->sessionID, $this->dbHL);

            // load up the member info if this is a member session
            $this->memberSystem->_attemptLoadMember();

            // if a user defined function was specified for loading additional member information,
            // run here
            if ($this->memberSystem->isMember && isset($this->directive['memberLoadFunction']) && function_exists($this->directive['memberLoadFunction']) ) {
                $mlF = $this->directive['memberLoadFunction'];
                $mlF($this->memberSystem->memberData);
            }

            // maintain tables
            $this->memberSystem->_maintainTables();

        }

    }

    /*************************** DEBUG **********************************/

    /**
     * dump debug session information
     *
     */
    function dumpInfo() {

        $op = "current session ID: $this->sessionID<br><br>\n";
        $op .= $this->getClassLog();

        $op .= $this->dumpDirectives();

        $op .= "persistant variables:<br>\n";
        $op .= "<ul>";

        foreach ($this->persistentVars as $pVars=>$v) {
            $op .= $pVars."<br>\n";
        }

        $op .= "</ul><br>";

        if ($this->directive['useMemberSystem']) {
            $op .= "member system is on:<br><br>";
            $op .= $this->memberSystem->dumpInfo();
        }
        else {
            $op .= "member system is off<br><br>";
        }

        return $op;

    }

}

?>