<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 * form entity class. a form entity contains a title and and input entity,
 * and the input entity optionall contains filters.
 *
 */
class SM_formEntity extends SM_object {

    /** (string) the title of this entity, which will be displayed to the user */
    var $title;

    /** (string) the variable name we'll use, which will be the POST variable */
    var $varName;

    /** (string) the type of input form this is */
    var $type;

    /** (mixed) the variables current value */
    var $value;

    /** (object) object of the actual entity */
    var $inputEntity;

    /** (bool) is this entity required to complete the form successfully? */
    var $required;

    /** (num)
     *  entity layout
     *  0 = Title on left, entity on right
     *  1 = Entity on left, title on right
     *  2 = Title on top<br>entity on bottom
     */
    var $layout;

    /** (string) alignment of the title. CENTER, RIGHT, or LEFT */
    var $titleAlign;

    /** (string) alignment of the input, CENTER, RIGHT, or LEFT */
    var $inputAlign;

    /** (bool) show background color or not */
    var $showBgColor;

    /** (string) variable prefix. from module prefix */
    var $varPrefix;

    /** (bool) use javascript for this input entity */
    var $useJS;

    /** (obj ref) parent smartform */
    var $parentSmartForm;

    /** (bool) if true, the filters will always be run for this input entity */
    var $forceFilter;

     /**
      * initialize a formEntity
      * @param $vn  (string)        - the variable name
      * @param $ty  (string)        - input entity type
      * @param $req (bool)          - required or not. if true, adds required filter
      * @param $vP  (string)        - var prefix
      * @param $dbH (obj ref)       - SiteManager default database handler
      * @param $sessionH (obj ref)  - SiteManager session handler
      * @param $dbHL (hash)         - SiteManager database handler hash
      * @param $parentSmartForm     - the main smartForm parent
      * @param $lay (int)           - layout
      * @param $val (mixed)         - initial value
      * @param $tA  (string)        - title alignment
      * @param $iA  (string)        - input entity alignment
      * @param $uJS (bool)          - use javascript or not for this form entity
      */
    function __construct($vn, $tt, $ty, $req, $vP, &$dbH, &$sessionH, &$dbHL, &$parentSmartForm, $lay=0, $val='', $tA="LEFT", $iA="LEFT", $uJS=true) {

        SM_sfLog($this, "formEntity('$vn','$tt','$ty','$req','$vP','$lay','$val','$tA','$iA')");

        // setup session handlers
        $this->dbH      =& $dbH;
        $this->dbHL     =& $dbHL;
        $this->sessionH =& $sessionH;

        // parent smartForm
        $this->parentSmartForm =& $parentSmartForm;
        // configure self
        $this->configure($this->parentSmartForm->directive);

        // setup some locals
        $this->varName      = $vn;
        $this->title        = $tt;
        $this->type         = $ty;
        $this->required     = $req;
        $this->value        = $val;
        $this->layout       = $lay;
        $this->titleAlign   = $tA;
        $this->inputAlign   = $iA;
        $this->varPrefix    = $vP;
        $this->showBgColor  = true;
        $this->useJS        = $uJS;

        $this->forceFilter  = false;

        // load entity
        $this->inputEntity =& $this->loadEntity($ty, $req);

    }


    /**
     * function to load and setup an instance of an entity
     * @param $eClass (string) the entity class to load
     * @param $req    (bool) [default false] whether this entity is required or not
     * @param $vName  (string) override variable name to something besides the one for this form entity
     *
     */
    function &loadEntity($eClass, $req=false, $vName='') {

        global $HTTP_POST_VARS;

        if ($vName == '')
            $vName = $this->varName;

        // create the input entity. it should be valid since smartForm does the checking
        $entityClass = $eClass."Entity";
        $newEntity = new $entityClass($this->dbH, $this->sessionH, $this->dbHL, $this);

        // if $val isn't set, but $$vn is, use that
        //if ((empty($val))&&(!empty($HTTP_POST_VARS[$vn]))) {
        if (isset($HTTP_POST_VARS[$vName])) {
            if (!is_array($HTTP_POST_VARS[$vName]))
                $val = stripslashes($HTTP_POST_VARS[$vName]);      // prevent recuring slashes
            $newEntity->setValue($vName, $this->varPrefix, $val, $this->useJS);
        }
        else {
            $newEntity->setValue($vName, $this->varPrefix, $this->value, $this->useJS);
        }

        // setup default class tag for the entities use
        $newEntity->addDirective('entityClassTag',$this->directive['entityClassTag']);

        // if this entity is required input, automatically add a requiredFilter
        if ($req)
            $newEntity->addFilter('required', 'Required Field');

        return $newEntity;

    }


     /**
      * return output from our input entity
      * @return (string) output from entity
      */
    function output() {

        (isset($this->directive['entityPrefixWrap'])) ?
            $_prefix = $this->directive['entityPrefixWrap'] :
            $_prefix = '';

        (isset($this->directive['entityPostfixWrap'])) ?
            $_postfix = $this->directive['entityPostfixWrap'] :
            $_postfix = '';

        return $_prefix.$this->inputEntity->entityThink().$_postfix;
    }

    /**
     * return any javascript code this entity and filter contains
     * @return (string) javascript output from entity and filters
     */
    function getJS() {
        return $this->inputEntity->getJS();
    }

     /**
      * apply filters that are attatched to this entity
      * don't apply them, however, if there was no data entered,
      * and it wasn't required
      */
    function applyFilters() {
        $vName = $this->varName;
        global $$vName;
        $data = $$vName;

        // only run filter if this field is required OR data isn't blank
        if ((($this->required)||($data != ''))||($this->forceFilter))
            return $this->inputEntity->applyFilters();
    }

     /**
      * get title from entity. if the inputEntity tells us the current value is bad, we'll mark it
      * as red. also, if its invalid, we should have a fix
      * message which we'll place underneith the title
      * @param $requiredStar if this is not blank, we'll prefix required entities titles with it
      * @param $normalClassTag font tag for normal text, ie, no problems
      * @param $badClassTag font tag for "bad" text, ie, there was a problem with the data
      * @param $correctMsgClassTag font tag for the message that tells them how to correct mistake
      * @return (string) title from entity, formatted with class tags and such
      */
    function getTitle() {

        // if the title is blank, return blank
        if ($this->title == '')
            return '';

        if ($this->required)
            $prefix = $this->directive['requiredStar'].' ';
        else
            $prefix = '';

        $title = '';

        if ($this->inputEntity->isValid)
            $title .= "<DIV CLASS=\"{$this->directive['normalClassTag']}\">$prefix".$this->title;
        else {
            $title .= "<DIV CLASS=\"{$this->directive['badClassTag']}\">$prefix$this->title<DIV CLASS=\"{$this->directive['correctMsgClassTag']}\">{$this->inputEntity->fixMessage}</DIV></DIV>";
        }

        return $title;

    }

     /**
      * return the layout order of this entity
      * @return (int) current layout
      */
    function getLayout() {
        return $this->layout;
    }

     /**
      * return the title alignment
      * @return (string) title alignment
      */
    function getTitleAlign() {
        return $this->titleAlign;
    }


     /**
      * return the input alignment
      * @return (string) input entity alignment
      */
    function getInputAlign() {
        return $this->inputAlign;
    }

    /**
     * show bgColor or not for this form entity
     * @param $val (bool) true or false
     */
    function setShowBgColor($val) {
        $this->showBgColor = $val;
    }

    /**
     * return whether we want a background color or not
     * @return (bool) whether we should show background color or not
     */
    function getShowBgColor() {
        return $this->showBgColor;
    }

}

?>
