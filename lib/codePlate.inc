<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 * a class to create pre-configured templates ("canned templates") that have
 * roughly the same template and module layout, but are used in multiple
 * pages (directive scripts). this saves you having to create the same
 * layout system in multiple directive scripts.
 *
 * this class should be extended for use. override codePlateConfig() and
 * codePlateThink() for functionality.
 */

class SM_codePlate extends SM_layoutTemplate {

    /**
     * constructor. setup a new codeplate. assign database and session handlers.
     * constructor will call SM_codePlate::codePlateConfig() which the extending
     * class must define. codePlateConfig() should call setMyTemplate()
     *
     * @param $dbH (obj ref)       - SiteManager default database handler
     * @param $sessionH (obj ref)  - SiteManager session handler
     * @param $dbHL (hash)        -  SiteManager database handler hash
     */
    function __construct(&$dbH, &$sessionH, &$dbHL) {

        // setup handlers
        $this->dbH      =& $dbH;
        $this->dbHL     =& $dbHL;
        $this->sessionH =& $sessionH;

        // call codePlateConfig(), which should be a method
        // defined by the extending class
        // in this class a call to setMyTemplate() should be made
        $this->codePlateConfig();

    }

    /**
     * set the HTML template this codePlate will be based on. it
     * will attempt to parse the file given. it will search template
     * paths for the file specified, automatically adding .tpt extension
     *
     * @param $fName (string) the file name of the HTML template
     * @return (nothing)
     */
    function setMyTemplate($fName) {

        $this->htmlTemplate = $fName;
        $this->parseTemplate();

    }

    /**
     * load a module for use in this codeplate. optionally add it immediately to
     * an area in my template. it will search module paths. note, returns by reference.
     * if $areaName isn't specified, the module must be added by hand later through
     * $this->addModule()
     *
     * @param $moduleFile (string) the module file to load (.mod file)
     * @param $areaName (string) [optional] add this module to an area of the template
     * @param $moduleName (string) [optional] if the class name is different from the module
     *                             file name, specify it here
     * @return (object: SM_module) the loaded module
     */
    function &loadModule($moduleFile, $areaName='',$moduleName='') {

        global $SM_siteManager;

        $newModule =& $SM_siteManager->loadModule($moduleFile,$moduleName);

        if ($areaName != '') {
            $this->addModule($newModule,$areaName);
        }

        return $newModule;

    }


    /**
     * load a template for use in this codeplate. optionally add immediately it to
     * an area in my template. it will search template paths. note, returns by reference
     * if $areaName isn't specified, the template can be added later by hand through $this->addTemplate()
     *
     * @param $templateFile (string) the template file to load (.tpt file)
     * @param $areaName (string) [optional] add this template to an area of the codePlate's template
     * @return (object: SM_layoutTemplate) the loaded template
     */
    function &loadTemplate($templateFile, $areaName='') {

        global $SM_siteManager;

        $newTemplate =& $SM_siteManager->loadTemplate($templateFile);

        if ($areaName != '') {
            $this->addTemplate($newTemplate,$areaName);
        }

        return $newTemplate;

    }


    /**
     * load a (sub) codePlate for use in this codeplate. optionally add immediately it to
     * an area in my template. it will search codePlate paths. note, returns by reference
     * if $areaName isn't specified, the codePLate can be added later by hand through $this->addTemplate()
     *
     * @param $codePlateFile (string) the template to load
     * @param $areaName (string) [optional] add this codePlate to an area of my template
     * @return (object: SM_codePlate) the loaded codePlate
     */
    function &loadCodePlate($codePlateFile, $areaName='') {

        global $SM_siteManager;

        $newCodePlate =& $SM_siteManager->loadCodePlate($codePlateFile);

        if ($areaName != '') {
            $this->addTemplate($newCodePlate,$areaName);
        }

        return $newCodePlate;

    }

    /**
     * override SM_layoutTemplate::_runModules and allow codePlate to run it's
     * codePlateThink() method
     */
    function _runModules() {

        if (method_exists($this,'codePlateThink')) {
            $this->codePlateThink();
        }

        parent::_runModules();

    }


}

?>
