<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  input entity suitable for date entry. the month
 *  and day are select boxes - the year may either
 *  be a select box or years, or text input box
 *   
 *  the final date returned is in the format YYYY/DD/MM
 *  
 *  directives:
 *  
 *   yearSelect  - when set to true, the year will be a select
 *             box rather than a text input box. if using
 *             this feature, be sure to set yearStart and
 *             yearEnd, which will define what years are
 *             included in the select box.
 *             default (true)
 *             
 *   yearStart   - the year the select box will start at
 *             default (current year)
 *   
 *   yearEnd     - the year the select box will end at
 *             default (current year+5)
 *             
 *   monthType   - either 'long' or 'short'. affects displayed month name
 *
 *  
 */
class dateEntity extends SM_inputTypeEntity {

    var $dSel;
    var $mSel;
    var $ySel;

    var $dVarName;
    var $mVarName;
    var $yVarName;
    
    /** setup this input entity */
    function entityConfig() {

        global $HTTP_POST_VARS;
        
        $varName   = $this->varName;
        $varNameSP = $this->varNameSansPrefix;        
        global $$varName;        

        // defaults
        $this->directive['yearSelect']  = true;
        $this->directive['yearStart']   = date("Y");
        $this->directive['yearEnd']     = date("Y")+5;      
        $this->directive['monthType']   = 'M';
                
        $this->dSel         = '';
        $this->mSel         = '';
        $this->ySel         = '';

        $this->dVarName = $varNameSP.'-d_'.$this->varPrefix;
        $this->mVarName = $varNameSP.'-m_'.$this->varPrefix;
        $this->yVarName = $varNameSP.'-y_'.$this->varPrefix;
        
        /* if seperate date vars are available for this varName, grab them here */
        if (isset($HTTP_POST_VARS[$this->mVarName]))
            $this->mSel = $HTTP_POST_VARS[$this->mVarName];
        if (isset($HTTP_POST_VARS[$this->dVarName]))
            $this->dSel = $HTTP_POST_VARS[$this->dVarName];
        if (isset($HTTP_POST_VARS[$this->yVarName]))
            $this->ySel = $HTTP_POST_VARS[$this->yVarName];

        // if we already have a value, and no single values, parse it out
        if (($this->value != '')&&(($this->mSel == '')||($this->dSel=='')||($this->ySel==''))) {
                
            /* try to identify different formats */
            /* [M]M-[D]D-YY[YY] */
            if (preg_match("/(\d\d*)[-\/](\d\d*)[-\/](\d\d\d*\d*)/",$this->value, $regs)) {
                $this->mSel = $regs[1];
                $this->dSel = $regs[2];
                $this->ySel = $regs[3];
            }
            
            /* YYYY-[M]M-[D]D */
            if (preg_match("/(\d\d\d\d)[-\/](\d\d*)[-\/](\d\d*)/",$this->value, $regs)) {
                $this->ySel = $regs[1];
                $this->mSel = $regs[2];
                $this->dSel = $regs[3];
            }
        
            settype($this->mSel,'integer');
            settype($this->dSel,'integer');
            settype($this->ySel,'integer');
                                       
        }
        

        /* if we have any of the *Sel vars now, setup the main one as well */
        if (($this->mSel != '')&&($this->dSel!='')&&($this->ySel!='')) {
            $$varName                           = $this->ySel.'-'.$this->mSel.'-'.$this->dSel;
            $HTTP_POST_VARS[$this->varName]     = $this->ySel.'-'.$this->mSel.'-'.$this->dSel;
        }

        // if the YEAR is not all numeric, complain
        if (($this->ySel != '')&&(!preg_match("/^\d\d\d\d$/",$this->ySel))) {
            $this->isValid = false;
            $this->fixMessage = "Year is invalid";
        }

        SM_sfLog($this, "setupEntity results - dSel: $this->dSel, mSel: $this->mSel, ySel: $this->ySel");
        
    }   


    /** output */
    function entityThink() {
           
        $dSelO[$this->dSel] = 'SELECTED';
        $mSelO[$this->mSel] = 'SELECTED';
        $ySelO[$this->ySel] = 'SELECTED';

        $output = '';
        $output .= "<SELECT NAME=\"{$this->mVarName}\">\n";
        for ($i=1; $i<13; $i++) {
            $monthName = date($this->directive['monthType'], mktime(0,0,0,$i,1,1999));
            (isset($mSelO[$i])) ? $s = $mSelO[$i] : $s = '';
            $output .= "<OPTION VALUE=\"$i\" $s>$monthName\n";
        }
        $output .= "</SELECT>\n";
        $output .= "<SELECT NAME=\"{$this->dVarName}\">\n";
        for ($i=1; $i<32; $i++) {
            (isset($dSelO[$i])) ? $s = $dSelO[$i] : $s = '';
            $output .= "<OPTION VALUE=\"$i\" $s>$i\n";
        }
        $output .= "</SELECT>\n";

        /* year select or text box */
        if ($this->directive['yearSelect']) {
            $output .= "<SELECT NAME=\"{$this->yVarName}\">\n";
            for ($i=$this->directive['yearStart']; $i<$this->directive['yearEnd']; $i++) {          
                (isset($ySelO[$i])) ? $s = $ySelO[$i] : $s = '';
                $output .= "<OPTION VALUE=\"$i\" $s>$i\n";
            }
            $output .= "</SELECT>\n";
        }
        else {
            /* text box. there will be a filter on it */
            $val = $this->ySel;
            $output .= "<INPUT TYPE=TEXT SIZE=\"4\" MAXLENGTH=\"4\" NAME=\"{$this->yVarName}\" VALUE=\"$val\">\n";
        }
    
        return $output;
    
    }

}

?>
