<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *  text area input box.
 *  
 *  directives:
 *  
 *      rows        - rows high
 *                  default (5)
 *
 *      cols        - columns wide
 *                  default (40)
 *            
 *       wrap       - can be sort of hard
 *                  default (soft)
 */
class textAreaEntity extends SM_inputTypeEntity {

    /** constructor */
    function entityConfig() {
            
        // setup some defaults
        $this->directive['rows']    = 5;
        $this->directive['cols']    = 40;
        $this->directive['wrap']    = 'SOFT';
        
    }

    /** output */
    function entityThink() {

        return "<TEXTAREA NAME=\"$this->varName\" ROWS=\"{$this->directive['rows']}\" COLS=\"{$this->directive['cols']}\" WRAP=\"$this->directive['wrap']}\">$this->value</TEXTAREA>";
    
    }

}

?>
