<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  select input object. may be muliple select. used as base class
 *  for many other entities.
 *  
 *  directives:
 *  
 *      multiple  - when set to true, the select box will
 *                allow multiple values to be selected
 *                at once. the $$varName will end up
 *                being an array once all input has
 *                been verified.
 *                default (false)
 *
 *  size        - the size of the select box (verticle)
 *            only valid for multiple selects
 *            default (1)
 *            
 *  addNoOption - when true, one of the options will be "no option",
 *                  ie, you can select a blank option to specify
 *                  the value is set with noOptionData
 *                  
 *  noOptionData    - defaults to blank.
 *  noOptionDisplay - defaults to -----         
 *                  
 *   
 *  populateList()  (optional)
 *  
 *      if this function is defined, it will be run by
 *      the output function before doing any work. it is
 *      usefull for classes that inherit this one, that
 *      need to populate the select list in various ways
 *      (ie, see dbSelectEntity or stateListentity)
 *      
 */
class selectEntity extends SM_inputTypeEntity {

    /**  setup entity */
    function entityConfig() {

        // defaults
        $this->directive['multiple']        = false;
        $this->directive['size']            = 1;
        $this->directive['addNoOption']     = false;
        $this->directive['noOptionData']    = "";
        $this->directive['noOptionDisplay'] = "-----";
    
    }

    /**     
     * add an option to the list
     * @param $title (string) title of the new option
     * @oaran $data (string) [optional] data of the new option. if same as title, leave blank
     */
    function addOption($title, $data='') {  
    
        SM_sfLog($this, "addOption('$title','$data')");

        if ($data === '')
            $data = $title;

        $this->directive['optionList'][] = array($title=>$data); 

    }

    /**
     * return the number of options currently in the list
     * @return (int) number of options in the list
     */
    function optionCount() {
        return count($this->directive['optionList']);
    }

    /**
     * set the list directly with an array (hash) passed to us
     * array should be $title => $data
     * @param $newList (hash) complete data set that should appear in the list
     */
    function setList($newList) {
        
        if (!is_array($newList)) {
            SM_sfLog($this,"setList: variable passed was not an array", true);
            return false;
        }
        

        foreach ($newList as $title => $data) {
            $this->addOption($title,$data);
        }
        
    }
        
    /**
     * output the entity
     */
    function entityThink() {
        
        // if we have a function in our class to populate the list, run it
        // this is useful for class that inherit this class, for them to setup
        // the list (after it has been configured)
        if (method_exists($this, 'populateList'))
            $this->populateList();
        
        // if no options, dont display
        if ((!isset($this->directive['optionList']))||(!is_array($this->directive['optionList']))) {
            SM_sfLog($this,"output: no options for select to output", true);        
            return 'N/A';
        }
    
        if ($this->directive['multiple']) {
            $braces = '[]';
            $mult   = 'MULTIPLE';
            if ($this->directive['size'] == 1)
                $this->directive['size'] = 3;
        }
        else {
            $braces = '';
            $mult = '';
        }

        /* do we already have one (or more) selected? */
        // check passed vars
        global $$this->varName;
        $selVar = $this->varName;
        $selArray = $$selVar;
        
        if ((!is_array($selArray))&&(!preg_match("/\d+\,*\d*/",$this->value))) {
            $selected[$this->value] = 'SELECTED';
        }
        elseif (is_array($selArray)) {
            foreach($selArray as $v) 
                $selected[$v] = 'SELECTED';             
        }
        
        // check for comma delimated list in $this->value
        if (preg_match("/\d+\,*\d*/",$this->value)) {
            $sList = explode(',',$this->value);
            foreach ($sList as $sV) 
                $selected[$sV] = 'SELECTED';
        }
        
        $output = "<SELECT NAME=\"{$this->varName}{$braces}\" $mult SIZE=\"{$this->directive['size']}\">\n";

        if ($this->directive['addNoOption']){

            // No Option Data
            if (isset($this->directive['noOptionData'])) {
               $noOptionData = $this->directive['noOptionData'];
            } else {
               $noOptionData = "";
            }
            // No Option Display Value
            if (isset($this->directive['noOptionDisplay'])) {
               $noOptionDisplay = $this->directive['noOptionDisplay'];
            } else {
               $noOptionDisplay = "------";
            }
            $output .= "<OPTION VALUE=\"$noOptionData\">$noOptionDisplay</OPTION>\n";
        }
        foreach ($this->directive['optionList'] as $item) {

            $title = key($item);
            $data  = $item[$title];

            if (isset($selected[$data]))
                $sel = $selected[$data];
            else
                $sel = '';

            $output .= "<OPTION VALUE=\"$data\" $sel>$title</OPTION>\n";

        }
        $output .= "</SELECT>\n";
        return $output;
        
    }

}

?>
