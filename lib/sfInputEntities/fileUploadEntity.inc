<?php


/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Jonathan Michel (pym@roadsend.com)
*
*/

/**
 *
 *  file upload box.
 *  
 *  directives:
 *  
 *      size        -   size of the text input box
 *                      on screen
 *                      default (20)
 *
 */
class fileUploadEntity extends SM_inputTypeEntity {

    /** setup entity */
    function entityConfig() {
            
        // setup some defaults
        $this->directive['size']       = 20;
        
    }

    /** output */
    function entityThink() {

        return "<INPUT TYPE=\"file\" NAME=\"$this->varName\" SIZE=\"{$this->directive['size']}\" MAXLENGTH=\"{$this->directive['maxLength']}\" VALUE=\"$this->value\">";
    
    }

}

?>
