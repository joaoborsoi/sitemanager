<?php


/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  text input box. should also
 *  be used for password input.
 *  
 *  directives:
 *  
 *      size        -   size of the text input box
 *                      on screen
 *                      default (20)
 *
 *      maxLength   - maximum length the total input
 *                    can be
 *                    default (20)
 *            
 *      passWord    - when true, input type will actually
 *                  be PASSWORD, so the characters will
 *                  be hidden when typed.
 *                  default (false)
 *
 */
class textEntity extends SM_inputTypeEntity {

    /** setup entity */
    function entityConfig() {
            
        // setup some defaults
        $this->directive['size']       = 20;
        $this->directive['maxLength']  = 20;
        $this->directive['passWord']   = false;
        
    }

    /** output */
    function entityThink() {
        
        if ($this->directive['passWord']) {
           $type = "PASSWORD";
           $val = '';
        } else {
           $type = "TEXT";
           $val = $this->value;
        }
        return "<INPUT TYPE=\"$type\" NAME=\"$this->varName\" SIZE=\"{$this->directive['size']}\" MAXLENGTH=\"{$this->directive['maxLength']}\" VALUE=\"$val\">";
    
    }

}

?>
