<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Jon Michel (pym@roadsend.com)
*
*/

SM_sfLoadEntity('select');      // load our parent


/**
 *
 *  a select box which populates itself from a database pull
 *  INHERITS selectEntity
 *  
 *  directives:
 *          same as selectEntity
 *          - plus -
 *          
 *          
 *          $rootDir      - the base directory (required)
 *          $filter       - file type *.* default
 */
class fileSelectEntity extends selectEntity {
    
    /**
     * Set up the defalt vaues for the entity
     * make sure the root directory exists and is readable
     */
    function entityConfig() {
            
        // setup some defaults
        $this->directive['rootDir']         = "/home/user";
        $this->directive['extension']       = "";
        $this->directive['includePath']     = false;


        if (empty($this->directive['rootDir'])) {
            SM_fatalErrorPage("attempt to use fileSelectEntity without root directory!",$this);
        }

    }

    /** populate the list with the database pull */
    function populateList() {

        // Get a list of all the files in the directory
        $root = $this->directive['rootDir'];
        $dh = dir($root);
        $ext = $this->directive['extension'];
        
        while ($entry = $dh->read()) {
            
            // build the absolute file name for later use 
            if ($this->directive['includePath'] == true) {
                $file  = $root ."/". $entry;
            } else {
                $file = $entry;
            }

            // Filter Testing when required
            if (empty($ext)) {
                $this->addOption($entry,$file);
            } elseif (preg_match("/\.$ext$/i",$entry)) {
                $extLength = -(strlen($ext)+1);

                $this->addOption(substr($entry,0,$extLength),$file);
            } 
        }
    } // end function

}

?>
