<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

SM_sfLoadEntity('comboBox');        // load our parent


/**
 *
 * a select box with a list of options, plus a text field to
 *  input a new option, populates itself from a database pull
 *  INHERITS comboBoxEntity. 
 *
 *  directives:
 *          same as comboBoxEntity
 *          - plus -
 *           
 *          $dataBaseID     - which database connection to use. default is 'default'
 *          table       -- table to pull from
 *          viewField   -- field to use as input
 *                         
 */ 
class dbComboBoxEntity extends comboBoxEntity {
        
    /** setup entity. requires database access */
    function entityConfig() {
    
        // setup some defaults
        $this->directive['tableName']       = '';
        $this->directive['viewField']       = '';

        if ($this->directive['dataBaseID'] == '')
            $this->directive['dataBaseID'] = 'default';       

        if (empty($this->dbHL[$this->directive['dataBaseID']])) {
            SM_fatalErrorPage("attempt to use dbComboBoxEntity without a default database connection!",$this);
        }

    }


    /** populate the list with the database pull */
    function populateList() {
        
        if (($this->directive['tableName'] == '')||($this->directive['viewField'] == '')) {
            SM_sfLog($this, "table or viewField  was not set!", true);
            return;
        }
                    
        // order by
        $oB = 'ORDER BY '.$this->directive['viewField'];
        
        $SQL = "SELECT DISTINCT {$this->directive['viewField']} FROM {$this->directive['tableName']} $oB";
        
        SM_sfLog($this, $SQL);
                
        $rh = $this->dbHL[$this->directive['dataBaseID']]->simpleQuery($SQL);
        if (DB::isError($rh)) {        
            SM_debugLog($SQL, $this);
            SM_fatalErrorPage("query failed",$this);
        }

        while ($rr = $this->dbHL[$this->directive['dataBaseID']]->fetchRow($rh)) {
            $this->addOption($rr[0]);
        }
        $this->dbHL[$this->directive['dataBaseID']]->freeResult($rh);        
    
    }
    


}

?>
