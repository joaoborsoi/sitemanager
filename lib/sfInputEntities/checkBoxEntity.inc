<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  input object for check boxes. $$varName will end
 *  up being an array when the data has been verified.
 *  
 *  directives:
 *  
 *      optionBreak - (bool) when set to true, when the options are 
 *                      printed, they will each be on their own
 *                      line, ie, each will be followed by a 
 *                      <BR> (default false)
 *  
 */
class checkBoxEntity extends SM_inputTypeEntity {
    

    /** 
     *  configure this entity. run in constructor of inputTypeEntity
     *
     */
    function entityConfig() {

        // setup some defaults
        $this->directive['optionBreak'] = false;
        $this->directive['preOut']      = '';
        $this->directive['postOut']     = '';

    }

    /**
     * add an option to the list
     * @param $title (string) title to use for new option
     * @param $data (string) data to use for new option. if same as title, leave blank
     */
    function addOption($title, $data='') {  
    
        if ($data === '')
            $data = $title;

        SM_sfLog($this, "addOption('$title','$data')");
        $this->directive['optionList'][] = array($title=>$data); 

    }

    /**
     * set the list directly with an array (hash) passed to us
     * array should be $title => $data
     * @param $newList (hash) new option list
     */
    function setList($newList) {
        
        if (!is_array($newList)) {
            SM_sfLog($this,"setList: variable passed was not an array", true);
            return false;
        }
        
        foreach ($newList as $title => $data) {
            $this->addOption($title,$data);
        }
        
        
    }

    
    /**
     * output the entity
     */
    function entityThink() {


        // go ahead and return if there's no options
        if (!is_array($this->directive['optionList'])) {
            SM_sfLog($this,"output: no options for checkbox to output", true);
            return 'N/A';
        }
        
        if ($this->directive['optionBreak'])
            $br = '<BR>';
        else
            $br = '';

        /* do we already have one (or more) selected? */
        // check passed vars
        global $$this->varName;
        $selVar = $this->varName;
        $selArray = $$selVar;
        
        if ((!is_array($selArray))&&(!preg_match("/\d+\,*\d*/",$this->value))) {
            $selected[$this->value] = 'CHECKED';
        }
        elseif (is_array($selArray)) {
            foreach($selArray as $v) 
                $selected[$v] = 'CHECKED';              
        }
        
        // check for comma delimated list in $this->value
        if (preg_match("/\d+\,*\d*/",$this->value)) {
            $sList = explode(',',$this->value);
            foreach ($sList as $sV) 
                $selected[$sV] = 'CHECKED';
        }
        
        $output = '';
        
        // if there is more than one checkbox, make it an array. otherwise, just a variable
        (sizeof($this->directive['optionList']) > 1) ? $makeArray = '[]' : $makeArray = '';
        
        foreach ($this->directive['optionList'] as $item) {       
            $title = key($item);
            $data  = $item[$title];
            (isset($selected[$data])) ? $sel = 'CHECKED' : $sel = '';
            $output .= "{$this->directive['preOut']}<INPUT TYPE=\"checkbox\" NAME=\"{$this->varName}{$makeArray}\" VALUE=\"$data\" $sel> ";

            if (!empty($this->directive['entityClassTag']))
                $output .= '<SPAN CLASS="'.$this->directive['entityClassTag'].'">';

            $output .= $title;

            if (!empty($this->directive['entityClassTag']))
                $output .= '</SPAN>';

            $output .= "$br{$this->directive['postOut']}\n";
        }

        return $output;
    }

}

?>
