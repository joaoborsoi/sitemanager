<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  input object for radio buttons. almost identical
 *  to checkBoxes, however $$varName will NOT be an
 *  array when data is verified.
 *  
 *  directives:
 *      preOut, postOut - wrap INPUT tags between these strings
 *      optionBreak - when set to true, when the options are 
 *                printed, they will each be on their own
 *                line, ie, each will be followed by a 
 *                <BR>
 *            default (false)
 *   
 */

class radioEntity extends SM_inputTypeEntity {


    /** configure the entity */
    function entityConfig() {

        // setup some defaults
        $this->directive['optionBreak'] = false;
        $this->directive['preOut']      = '';
        $this->directive['postOut']     = '';

    }

    /**  
     * add an option
     * @param $title (string) - title of new option
     * @param $data (string) - data of new option. if same as title, leave blank
     */
    function addOption($title, $data='') {  
    
        if ($data === '')
            $data = $title;
        SM_sfLog($this, "addOption('$title','$data')");
        $this->directive['optionList'][] = array($title=>$data);

    }

    /** 
     * set the list directly with an array (hash) passed to us
     * array should be $data => $title
     * @param $newList (hash) has of option list to use
     */
    function setList($newList) {
        
        if (!is_array($newList)) {
            SM_sfLog($this,"setList: variable passed was not an array", true);
            return false;
        }
        
        foreach ($newList as $title => $data) {
            $this->addOption($title,$data);
        }
        
    }

    
    /** output */
    function entityThink() {


        /* go ahead and return if there's no options */
        if (!is_array($this->directive['optionList'])) {
            SM_sfLog($this,"output: no options for radio to output", true);
            return 'N/A';                      
        }
                            
        ($this->directive['optionBreak']) ? $br = '<BR>' : $br = '';

        /* do we already have one (or more) selected? */
        $selected[$this->value] = 'CHECKED';
        
        $output = '';
        foreach ($this->directive['optionList'] as $item) {
            $title = key($item);
            $data  = $item[$title];
            (isset($selected[$data])) ? $sel = 'CHECKED' : $sel = '';

            $output .= "{$this->directive['preOut']}<INPUT TYPE=\"radio\" NAME=\"$this->varName\" VALUE=\"$data\" $sel> ";
            
            if (!empty($this->directive['entityClassTag']))
                $output .= '<SPAN CLASS="'.$this->directive['entityClassTag'].'">';

            $output .= $title;            

            if (!empty($this->directive['entityClassTag']))
                $output .= '</SPAN>';

            $output .= "$br{$this->directive['postOut']}\n";

        }

        return $output;
    }

}

?>
