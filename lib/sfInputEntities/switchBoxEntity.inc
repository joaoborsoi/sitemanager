<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 * a group of checkboxes inside one entity
 *  
 *  directives:
 *  
 *      returnVal   - the default value each box will return if it's on
 *                  default "ON"
 *      numPerRow   - how many boxes to add per row
 *
 */
class switchBoxEntity extends SM_inputTypeEntity {

    /** (hash) a hash of name => title for each checkbox in the box */
    var $boxList;

    /** (hash) which check boxes are "on" (checked) */
    var $selected;

    /** setup entity */
    function entityConfig() {
    
        // setup some defaults
        $this->directive['returnVal']       = 'ON';
        $this->directive['numPerRow']       = 3;
        
    }


    /**    
     * use this function to add a checkbox to the list in the box
     * $data will be the VALUE="", $title will be displayed
     * to the user.
     * @param $name (string) varname of checkbox
     * @param $title (string) title for this checkbox
     * @param $checked (bool) when true, the checkbox will be on by default
     *
     */
    function addBox($name, $title, $checked=false) {
    
        SM_sfLog($this, "addBox('$name','$title')");
        $this->boxList[$name] = $title;
        
        if ($checked)
            $this->selected[$name] = 'CHECKED';
    
    }
    
    /** output */
    function entityThink() {

        // go ahead and return if there's no options
        if (!is_array($this->boxList)) {
            SM_sfLog($this,"output: no options for switchBox to output", true);
            return 'N/A';
        }
        
        $output = "<table>\n";
        $numRows = sizeof($this->boxList) / $this->directive['numPerRow'];

        reset($this->boxList);
        for ($i=0; $i < $numRows; $i++) {       
            $output .= "<tr>";
            
            for ($p = 0; $p < $this->directive['numPerRow']; $p++) {
                if ((($i+1) * ($p+1)) > sizeof($this->boxList))
                    break;
                $title = current($this->boxList);
                $name  = key($this->boxList);
                (isset($this->selected[$name])) ? $sel = 'CHECKED' : $sel = '';
                $output .= "<td><INPUT TYPE=\"checkbox\" NAME=\"{$name}_{$this->varPrefix}\" VALUE=\"{$this->directive['returnVal']}\" $sel> $title</td>\n";
                next($this->boxList);
            }
            
            $output .= "</tr>\n";
        }
        $output .= "</table>\n";
        
        return $output;
    }

}

?>

