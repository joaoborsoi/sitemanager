<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

SM_sfLoadEntity('select');      // load our parent


/**
 *
 * a select box with a list of options, plus a text field to
 * input a new option. INHERITS selectEntity
 *
 * directives:
 *      same as selectEntity
 *          - plus -
 *          textSize        - size of the text input
 *          textMaxLength   - maxlen of the text input
 *     
 */ 
class comboBoxEntity extends selectEntity {

    /** 
     * (bool)
     * set to not show value in the text box, in the case where the entity     
     * has a default value which we want to be default in the select box,
     * but no value in the text box for easy editing
     */
    var $noShowText; 
        
    /**  setup this input entity */
    function entityConfig() {

        global $HTTP_POST_VARS;
        $vName   = $this->varName;
        $vNameSP = $this->varNameSansPrefix;
        global $$vName;
        
        $tName = $vNameSP.'-t_'.$this->varPrefix;
        $sName = $vNameSP.'-s_'.$this->varPrefix;

        // setup some defaults
        $this->directive['textSize']        = 20;
        $this->directive['textMaxLength']   = 20;
        $this->noShowText                   = false;

        // if our text box is set, set that to the main value
        if ($HTTP_POST_VARS[$tName] != '') {
            $HTTP_POST_VARS[$this->varName] = $HTTP_POST_VARS[$tName];
            $$vName = $HTTP_POST_VARS[$tName];
        }
        elseif (isset($HTTP_POST_VARS[$sName]))  {
            // otherwise, set the select box to it's previous value
            $HTTP_POST_VARS[$this->varName] = $HTTP_POST_VARS[$sName];         
            $$vName = $HTTP_POST_VARS[$sName];
        }
        else {          
            // show default value, not forms value
            $HTTP_POST_VARS[$this->varName] = $this->value;
            $$vName = $this->value;
            $this->noShowText = true;
        }

    }   


    /** output function, overridden from selectEntity */
    function entityThink() { 
    
        // setup
        $tName = $this->varNameSansPrefix.'-t_'.$this->varPrefix;
        $sName = $this->varNameSansPrefix.'-s_'.$this->varPrefix;        

        // must reset value, because it might have been updated in setupEntity
        global $HTTP_POST_VARS;
        $this->value = $HTTP_POST_VARS[$this->varName];
        
        // if we have a function in our class to populate the list, run it
        // this is useful for class that inherit this class, for them to setup
        // the list (after it has been configured)
        if (method_exists($this, 'populateList'))
            $this->populateList();
    
        // if no options, dont display
        if ((!isset($this->directive['optionList']))||(!is_array($this->directive['optionList']))) {
            SM-sfLog($this,"output: no options for select to output", true);        
            return 'N/A';
        }
    
        /* do we already have one (or more) selected? */
        // check passed vars
        global $$this->varName;
        $selVar = $this->varName;
        $selArray = $$selVar;
        
        if ((!is_array($selArray))&&(!preg_match("/\d+\,*\d*/",$this->value))) {
            $selected[$this->value] = 'SELECTED';
        }
        elseif (is_array($selArray)) {
            foreach($selArray as $v) 
                $selected[$v] = 'SELECTED';             
        }
        
        // check for comma delimated list in $this->value
        if (preg_match("/\d+\,*\d*/",$this->value)) {
            $sList = explode(',',$this->value);
            foreach ($sList as $sV) 
                $selected[$sV] = 'SELECTED';
        }
    
        // text box     
        if (!$this->noShowText)
            $val = $this->value;
        else
            $val = '';
        $output = "<INPUT NAME=\"{$tName}\" VALUE=\"$val\" TYPE=\"TEXT\" SIZE=\"{$this->directive['textSize']}\" MAXLENGTH=\"{$this->directive['textMaxLength']}\"><BR>";
        
        // select box
        $output .= "<SELECT NAME=\"{$sName}\" SIZE=\"{$this->directive['size']}\">\n";

        $output .= "<OPTION VALUE=\"\">-----</OPTION>\n";
        
        foreach ($this->directive['optionList'] as $item) {

            $title = key($item);
            $data  = $item[$title];

            if (isset($selected[$data]))
                $sel = $selected[$data];
            else
                $sel = '';
            $output .= "<OPTION VALUE=\"$data\" $sel>$title</OPTION>\n";
        }
        $output .= "</SELECT>\n";
        return $output;
    }

}

?>
