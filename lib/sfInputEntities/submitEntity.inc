<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *  SUBMIT input entity.
 * 
 *  directive: 
 *
 *     value - the VALUE tag
 *     image - bool (default false). when true, type is IMAGE and SRC must be set
 *     extra - freeform extra text
 *     src   - image SRC, if type is IMAGE
 *
 */
class submitEntity extends SM_inputTypeEntity {
    

    /** configure the entity */
    function entityConfig() {

        // clean layout up, we'll do our own
        $this->parentFormEntity->layout = SF_LAYOUT_SINGLENOBR;
        
        // SUNMIT or IMG
        $this->directive['image'] = false;

    }

    /** output:  return static text */
    function entityThink() {
    
        if ($this->directive['image'])
            $sType = 'IMAGE';
        else
            $sType = 'SUBMIT';

        $output = '<INPUT TYPE="'.$sType.'" NAME="'.$this->varName.'"';

        if (isset($this->directive['value']))
            $output .= " VALUE=\"{$this->directive['value']}\" ";

        if (isset($this->directive['extra']))
            $output .= " {$this->directive['extra']} ";

        if (isset($this->directive['src']))
            $output .= " SRC=\"{$this->directive['src']}\" ";

        $output .= '>';
        return $output;

    }

}

?>
