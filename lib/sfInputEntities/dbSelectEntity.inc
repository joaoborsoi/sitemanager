<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

SM_sfLoadEntity('select');      // load our parent


/**
 *
 *  a select box which populates itself from a database pull
 *  INHERITS selectEntity. 
 *  directives:
 *          same as selectEntity
 *          - plus -
 *          
 *          $dataBaseID     - which database connection to use. default is 'default'
 *          $tableName      - table to pull from
 *          $whereClause    - optional where clause to add
 *          $viewField      - field to "show" in select list  (if not set, it will be the second field in the table)
 *          $dataField      - field to use as data in select list
 *          $orderBy        - field(s) to order by
 */
class dbSelectEntity extends selectEntity {
    
    /**
     * make sure we have database connectivity
     *
     */
    function entityConfig() {

        if ($this->directive['dataBaseID'] == '')
            $this->directive['dataBaseID'] = 'default';       

        if (empty($this->dbHL[$this->directive['dataBaseID']])) {
            SM_fatalErrorPage("attempt to use dbSelectEntity without a default database connection!",$this);
        }

    }

    /** populate the list with the database pull */
    function populateList() {
    
        if (($this->directive['tableName'] == '')||($this->directive['dataField'] == '')) {
            SM_sfLog($this, "table, viewField or dataField was not set!", true);
            return;
        }
        
        if (isset($this->directive['whereClause'])) {
            if (!eregi("WHERE",$this->directive['whereClause']))
                $wC = 'WHERE '.$this->directive['whereClause'];
            else
                $wC = $this->directive['whereClause'];
        }
        else {
            $wC = '';
        }
        
        $vF = $this->directive['viewField'];
        
        // order by viewField by default
        if ($this->directive['orderBy'] != '')
            $oB = 'ORDER BY '.$this->directive['orderBy'];
        elseif ($this->directive['orderBy'] != '*')
            $oB = 'ORDER BY '.$this->directive['viewField'];
        else 
            $oB = '';
        

        // check for a dbOverRide
        if(isset($this->directive['dbOverRide']))
           $dbName = $this->directive['dbOverRide'];
        else
           $dbName = $GLOBALS['dbName'];


        $SQL = "SELECT {$this->directive['viewField']}, {$this->directive['dataField']} AS _id FROM {$this->directive['tableName']} $wC $oB";
        
        SM_sfLog($this, $SQL);
        
        $rh = $this->dbHL[$this->directive['dataBaseID']]->simpleQuery($SQL);
        if (DB::isError($rh)) {        
            SM_debugLog($SQL, $this);
            SM_fatalErrorPage("query failed".mysql_Error(),$this);
        }
        while ($rr = $this->dbHL[$this->directive['dataBaseID']]->fetchRow($rh)) {
            $this->addOption($rr[$vF], $rr['_id']);
        }
        $this->dbHL[$this->directive['dataBaseID']]->freeResult($rh);        
    
    }

}

?>
