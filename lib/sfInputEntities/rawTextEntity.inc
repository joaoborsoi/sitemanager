<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *  raw text, no input.
 * 
 *  directive: 
 *
 *     staticText - the text to display
 *
 */
class rawTextEntity extends SM_inputTypeEntity {
    
    /** output:  return static text */
    function entityThink() {
        if (isset($this->directive['staticText']))        
            return $this->directive['staticText'];
        else
            return '';
    }

}

?>
