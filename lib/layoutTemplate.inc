<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/


/**
 *
 * the SM_layoutTemplate class is the base class for creating a
 * layout template in which to add modules
 *
 */
class SM_layoutTemplate extends SM_object {

    /** (array) list of areas available in the template */
    var $areaList;

    /** (multihash) multidimensional hash of module->area mappings */
    var $areaMapping;

    /** (string) the actual HTML template file name, with encoded AREA information */
    var $htmlTemplate;

    /** (string) the final html output. one big string. */
    var $htmlOutput;

    /** (string) the final javascript output. one big string. */
    var $jsOutput;

    /** (hash) this is the SM tag list to replace with various bits of final output */
    var $tagList;

    /** (string) the HTML template file name we're using to define areas */
//  var $htmlTemplate;

    /** (hash) area output, gathered from all modules in this area */
    var $areaOutput;

    /** (int) counter of modules in template */
    var $numModInTemplate = 0;

    /** (hash) SM tag parameters */
    var $tagParms;

    var $caseSensitive;
     /**
      * constructor loads in and parses an HTML template. also sets up
      * database and session handlers
      *
      * @param $htmlTemplate (string) the full path of the HTML template to open
      * @param $dbH (obj ref)       - SiteManager default database handler
      * @param $sessionH (obj ref)  - SiteManager session handler
      * @param $dbHL (hash)         - SiteManager database handler hash
      * @return (nothing)
      */
    function __construct($htmlTemplate, &$dbH, &$sessionH, &$dbHL, $caseSensitive=true) {
      $this->caseSensitive = $caseSensitive;
        // defaults
        $this->htmlTemplate = $htmlTemplate;

        // setup session handlers
        $this->dbH      =& $dbH;
        $this->sessionH =& $sessionH;
        $this->dbHL     =& $dbHL;

        // parse the template
        $this->parseTemplate();

	if(!$this->caseSensitive)
	  $this->areaList = array_change_key_case($this->areaList,CASE_LOWER);

    }

    /**
     * parse the HTML template file. find and handle all SM tags
     *
     * @return (nothing)
     */
    function parseTemplate() {

        global $SM_siteManager;

        // if this->htmlTemplate doesn't have a path, use SM_siteManager to figure it out
        if (!preg_match('/[\/\\\]/',$this->htmlTemplate))  {

            // no path, so search siteManager paths. error if not found.
            $this->htmlTemplate = $SM_siteManager->findSMfile($this->htmlTemplate, 'templates', 'tpt');

        }

        // load the template in
        // get list of areas from template
        $fileArray = file($this->htmlTemplate);

        // if we couldn't load the file, it's a fatal error
        if (sizeof($fileArray) == 0)
            SM_fatalErrorPage("couldn't access HTML template: $this->htmlTemplate",$this);

        // go line by line and handle SM tags, swapping out areaName's for a uniq id
        // you CAN have multiple SM tags on one line
        // but you CANNOT break them across multiple lines!
        $this->htmlOutput   = '';
        $this->jsOutput     = '';

        foreach ($fileArray as $line) {

            // use this to determine what kind of tag we're dealing with
            $tagType = '';
            $areaSub = '';

            if (preg_match_all("/<\s*SM\s(.+)\s*>/Ui",$line,$smTagMatch)) {

                // for each match we found on this line...
                for ($i=0; $i < sizeof($smTagMatch[1]); $i++) {

                    // grab TYPE parameter
                    $smParms = $smTagMatch[1][$i];

                    if (preg_match("/TYPE\s*=\s*[\"\'](.+)[\"\']/Ui",$smParms,$smTypeMatch)) {
                        $tagType = $smTypeMatch[1];
                    }
                    else {
                        // warning -- type not found, ignore
                        SM_debugLog("warning: SM tag had no TYPE parameter");
                        continue;
                    }

                    // now handle each type
                    switch (strtoupper($tagType)) {
                    case 'AREA':

                        // area tag
                        // requires NAME tag
                        if (preg_match("/NAME\s*=\s*[\"\'](.+)[\"\']/Ui",$smParms,$smNameMatch)) {
                            $areaSub = $smNameMatch[1];
                        }
                        else {
                            // warning -- type not found, ignore
                            SM_debugLog("warning: SM tag type AREA had no NAME parameter");
                            continue;
                        }

                        // save it for later.
                        $this->areaList[$areaSub] = $smTagMatch[0][$i];

                        // save tag parameters
                        $this->tagParms['AREA'][$areaSub] = $smParms;
                        break;
                    case 'MODULE':

                        // increase counter
                        $this->numModInTemplate++;

                        // module tag
                        // requires NAME tag
                        if (preg_match("/NAME\s*=\s*[\"\'](.+)[\"\']/Ui",$smParms,$smNameMatch)) {
                            $moduleName = $smNameMatch[1];
                        }
                        else {
                            // warning -- type not found, ignore
                            SM_debugLog("warning: SM tag type MODULE had no NAME parameter");
                            continue;
                        }

                        // generate an areaID
                        $areaID = $moduleName.$this->numModInTemplate;

                        // optional directives tag
                        if (preg_match("/DIRECTIVES\s*=\s*[\"\'](.+)[\"\']/Ui",$smParms,$smNameMatch)) {
                            $directives = $smNameMatch[1];
                        }
                        else {
                            $directives = '';
                        }

                        // save it for later.
                        // area name is same as module name
                        $this->areaList[$areaID] = $smTagMatch[0][$i];

                        // save tag parameters
                        $this->tagParms['MODULE'][$areaID] = $smParms;

                        // now load that module and stick it in the new area
                        $baMod =& $SM_siteManager->loadModule($moduleName);
                        if (is_object($baMod)) {

                            // yes! convert it into a module and add it in
                            $this->areaMapping[$areaID][] =& $baMod;

                            // if there were directives, decode them and set them up
                            if ($directives != '') {

                                $dArray = split(',',$directives);
                                foreach ($dArray as $dPair) {
                                    list($dKey, $dVal) = split('=',$dPair);
                                    $baMod->addDirective($dKey,$dVal);
                                }

                            }

                        }
                        else {
                            // if it wasn't an object (ie, the module failed to load)
                            // it will be ignored by the template
                            SM_debugLog("warning: SM tag type MODULE, module name ($moduleName) failed to load, ignoring");
                        }

                        break;
                    case 'JAVASCRIPT':
                        // javascript section
                        $this->tagList['js'] = $smTagMatch[0][$i];
                        break;
                    case 'HTMLTITLE':

                        // HTML document title
                        $this->tagList['htmlTitle'] = $smTagMatch[0][$i];

                        // optional NAME tag
                        if (preg_match("/NAME\s*=\s*[\"\'](.+)[\"\']/Ui",$smParms,$smNameMatch)) {
                            $dTitle = $smNameMatch[1];

                            // default title
                            if (empty($this->directive['htmlTitle']))
                                $this->directive['htmlTitle'] = $dTitle;
                        }

                        break;
                    case 'HTMLBODY':
                        // generate <BODY> tag
                        $this->tagList['htmlBody'] = $smTagMatch[0][$i];
                        break;
                    default:
                        SM_debugLog("warning: SM tag has TYPE we didn't know how to handle ($tagType) ignoring");
                        continue;
                    }

                }

            }


            // add it to our final output string
            $this->htmlOutput .= $line;

        }

    }


    /**
     * return true if the areaName passed exists in the template
     * @param $areaName (string) areaName to check
     * @return (bool) true if areaName passed exists in template
     */
    function areaExists($areaName) {
      return array_key_exists($areaName,$this->areaList);
    }


    /**
     * return a list of area names in this template
     *
     * @return (array) list of areas in this template
     */
    function getAreaList() {
        return array_keys($this->areaList);
    }

    /**
     * return the attributes that were in the SM AREA tag
     * for a given area as a hash, where the key is the attribute name and
     * the value is the value of the attribute. note that the attribute key
     * will be converted to uppercase
     * @param $areaName (string) the area to lookup
     * @return (hash) the attributes from the SM AREA tag in the template for $areaName
     */
    function getAreaAttributes($areaName) {

        // parse the attributes
        preg_match_all('/(\w+)\s*=\s*[\"\'](.+)[\"\']/Ui',$this->tagParms['AREA'][$areaName],$matchList);

        for ($x = 0; $x<count($matchList[1]); $x++) {
            $key = strtoupper($matchList[1][$x]);
            $aList[$key] = $matchList[2][$x];
        }

        return $aList;

    }


     /**
      * add a module to an area in the template
      *
      * @param $module (object: SM_module) a reference to a module
      * @param $areaName (string) the arbitrary area name this module should appear in
      *
      * @return (nothing)
      */
    function addModule(&$module, $areaName) {

        // error checking
        if (!is_object($module))
            SM_fatalErrorPage("module added to area \"$areaName\" is not a valid SM_module!",$this);

        // check for valid area name
        if (empty($this->areaList[$areaName])) {
            SM_debugLog("area name not found, ignoring request to add ".get_class($module)." to $areaName",$this);
            return;
        }

        // add module to $areaMapping for valid area
        $this->areaMapping[$areaName][] =& $module;

    }


     /**
      * add a subtemplate to an area in the this template
      *
      * @param $template (object: SM_layoutTemplate) a template object
      * @param $areaName (string) the arbitrary area name this template (and it's contents) should appear in
      *
      * @return (nothing)
      */
    function addTemplate(&$template, $areaName) {
	if(!$this->caseSensitive)
	  $areaName = strtolower($areaName);

        // error checking
        if (!is_object($template))
            SM_fatalErrorPage("subtemplate added to area \"$areaName\" is not a valid SM_layoutTemplate!",$this);

        // check for valid area name
        if (empty($this->areaList[$areaName])) {
            SM_debugLog("area name not found, ignoring request to add sub-template to $areaName");
            return;
        }

        // add template to $areaMapping for valid area
        $this->areaMapping[$areaName][] =& $template;

    }

    /**
     * add text (string) to an area in this template.
     * @param $output (string) the string to add
     * @param $areaName (string) the arbitrary area name this template (and it's contents) should appear in
     *
     * @return (nothing)
     */
    function addText($output, $areaName) {

	if(!$this->caseSensitive)
	  $areaName = strtolower($areaName);

        // error checking
        if (!is_string($output))
            SM_fatalErrorPage("data added to area \"$areaName\" is not a valid string!",$this);

        // check for valid area name
        if (empty($this->areaList[$areaName])) {
            SM_debugLog("area name not found, ignoring request to add string to $areaName");
            return;
        }

        // add template to $areaMapping for valid area
        $this->areaMapping[$areaName][] = $output;

    }


    /**
     * private function that will do the work of running modules
     * and gathering their output
     *
     * @return (nothing)
     */
    function _runModules() {

        global $SM_siteManager;

        // if no modules have been added, we'll just go through and remove
        // our unique id's and output a blank template
        if ((!isset($this->areaMapping))||(sizeof($this->areaMapping) == 0)) {

            foreach ($this->areaList as $areaName => $areaID) {
	      $this->htmlOutput = str_replace($areaID,'',$this->htmlOutput);
            }

            echo $this->htmlOutput;
            return;

        }


        // for each area, run modules associated with this area.
        // save module output for later.
        // will also collect javascript code from modules
        foreach ($this->areaMapping as $areaName => $moduleList) {

            // clear area output for this area before we run modules for this this area
            $this->areaOutput[$areaName] = '';

            // run through all modules in this area
            foreach ($moduleList as $moduleName => $areaData) {

                // is this actually a module or a subtemplate/codeplate?
                if ((strtolower(get_class($areaData)) == 'sm_layouttemplate') ||
                    (is_subclass_of($areaData,'sm_layouttemplate'))) {

                    // either template or codeplate

                    // run template
                    $areaData->run();

                    // get html output
                    $mOutput = $areaData->htmlOutput;

                    // get javascript
                    $this->jsOutput .= $areaData->jsOutput;

                }
                elseif (is_subclass_of($areaData,'sm_module')) {

                    // module

                    // run module
                    $mOutput = $areaData->run();

                    // get javascript code from this module, if any
                    $this->jsOutput .= $areaData->getJS();

                }
                elseif (is_string($areaData)) {

                    // string output
                    $mOutput = $areaData;

                }
                else {
                    SM_debugLog("unknown object added to template area [$areaName] (was not decendent of SM_module or SM_layoutTemplate): ignoring",$this);
                    SM_debugLog("object appeared to be: ".get_class($areaData),$this);
                }

                // add output to this areas output
                $this->areaOutput[$areaName] .= $mOutput;

            }

        }

    }

    /**
     * private function that will substitute the output gathered from
     * modules into their appropriate areas. also handles substitution
     * of various other SM tag output
     *
     * @return (nothing)
     */
    function _subOutput() {

        // do we have any blank areas to remove?
      $blankAreas = array_diff(array_keys($this->areaList),array_keys($this->areaMapping));

        if (sizeof($blankAreas)) {
            // yep, clean em out of the final output
            foreach($blankAreas as $areaName) {
                $this->htmlOutput = str_replace($this->areaList[$areaName], '', $this->htmlOutput);
            }
        }

        // replace AREA keywords in template with output from modules
        foreach ($this->areaMapping as $areaName => $moduleList) {
            // now replace this areas output with the final concatented area output
            $this->htmlOutput = str_replace($this->areaList[$areaName], $this->areaOutput[$areaName], $this->htmlOutput);

        }

        // HTMLBODY
        $hHead = '';
        if (!empty($this->tagList['htmlBody'])) {

            $hHead .= "<BODY";
            if (!empty($this->directive['bodyBGCOLOR']))
                $hHead .= " BGCOLOR=\"{$this->directive['bodyBGCOLOR']}\" ";
            if (!empty($this->directive['bodyLINK']))
                $hHead .= " LINK=\"{$this->directive['bodyLINK']}\" ";
            if (!empty($this->directive['bodyALINK']))
                $hHead .= " ALINK=\"{$this->directive['bodyALINK']}\" ";
            if (!empty($this->directive['bodyVLINK']))
                $hHead .= " VLINK=\"{$this->directive['bodyVLINK']}\" ";
            if (!empty($this->directive['bodyBACKGROUND']))
                $hHead .= " BACKGROUND=\"{$this->directive['bodyBACKGROUND']}\" ";
            if (!empty($this->directive['bodyONLOAD']))
                $hHead .= " ONLOAD=\"{$this->directive['bodyONLOAD']}\" ";
            if (!empty($this->directive['bodyOther']))
                $hHead .= " \"{$this->directive['bodyOther']}\" ";
            $hHead .= ">\n\n";

            $this->htmlOutput = str_replace($this->tagList['htmlBody'], $hHead, $this->htmlOutput);

        }


        // JAVASCRIPT
        if (!empty($this->tagList['js'])) {

            // check for session handlers javascript output
            if (isset($this->sessionH) && !empty($this->sessionH->jsOutput)) {
                $this->jsOutput .= $this->sessionH->jsOutput;
            }

            // if there was no output, this will blank out the SM javaScript tag
            $this->htmlOutput = str_replace($this->tagList['js'], $this->jsOutput, $this->htmlOutput);

        }

        // HTMLTITLE
        if (!empty($this->tagList['htmlTitle']) && (isset($this->directive['htmlTitle']))) {
            $this->htmlOutput = str_replace($this->tagList['htmlTitle'], $this->directive['htmlTitle'], $this->htmlOutput);
        }

        // replace @@sessionVars@@ with current session vars
        // get latest session vars
        global $SM_siteManager;
        if (isset($SM_siteManager->sessionH)) {
            $SM_sessionVars = $SM_siteManager->sessionH->getSessionVars();
            $this->htmlOutput = str_replace("@@sessionVars@@",$SM_sessionVars,$this->htmlOutput);
        }
    }

    /**
     * run the template, ie run the modules inside this template and sub in
     * their output to our HTML template
     *
     * @return (string) complete HTML output from this template
     */
    function run() {

        $this->_runModules();
        $this->_subOutput();

        return $this->htmlOutput;

    }


}

?>
