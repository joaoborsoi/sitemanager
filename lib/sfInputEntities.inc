<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 * input entity base class
 * all input entities should extend this class
 * NOTE -- setupEntity() is a child function that can exist, and will be run after setValue is through running in the
 *         base class. setValue should not be overridden! (weyrick)
 */
class __construct extends SM_object {

    /** (obj ref array) the filter list for this entity */
    var $filterList;

    /** (mixed) the current value of this entity */
    var $value;

    /** (bool) is this entity current valid? */
    var $isValid;

    /** (string) my variable name */
    var $varName;

    /** (string) variable name, sans the prefix */
    var $varNameSansPrefix;

    /** (string) instructions on how a user can fix their form */
    var $fixMessage;

    /** (string) variable prefix */
    var $varPrefix;

    /** (bool) does this input entity REQUIRE javascript to work? */
    var $requiresJS;

    /** (bool) are we allowed to use javascript in this input entity? */
    var $useJS;

    /**
     * (string) javascript output. this is the stuff that needs to go at the top,
     * not the inline stuff
     */
    var $jsOutput;

    /**
     * (obj ref) reference back to the form entity this input entity is contained in
     *
     */
    var $parentFormEntity;

    /**
      * base constructor. setup class.this should NOT be overriden!
      * use entityConfig() for class initialization
      * @param $dbH (obj ref)       - SiteManager default database handler
      * @param $sessionH (obj ref)  - SiteManager session handler
      * @param $dbHL (hash)         - SiteManager database handler hash
      */
    function SM_inputTypeEntity(&$dbH, &$sessionH, &$dbHL, &$fE) {

        // setup session handlers
        $this->dbH              =& $dbH;
        $this->dbHL             =& $dbHL;
        $this->sessionH         =& $sessionH;
        $this->parentFormEntity =& $fE;

        // entity is invalid unless filter tells us otherwise
        $this->isValid      = true;
        $this->fixMessage   = '';
        $this->requireJS    = false;
        $this->hasJS        = false;
        $this->jsOutput     = '';
        $this->useJS        = true;

        SM_sfLog($this, " [input type entity created: ".get_class($this)."]");

    }

    /**
     * return javascript output from filters attaached to this entity,
     * as well as our own
     * @return (string) javascript output
     */
    function getJS() {


        // if we have no filters, just return our javascript code
        if (!isset($this->filterList))
                return;

        // get javascript from each entity
        $fjsOutput = '';
        $fjsOutputL = '';
        foreach($this->filterList as $filter) {

            $fjsOutputL .= $filter->getJS();

            // save list for last use
            if ($fjsOutputL != '') {
                $fName = get_class($filter);
                $fName = str_replace("filter","Filter",$fName); // capitlize Filter, as the class won't for us
                $fjsList[] = $fName;
            }

            $fjsOutput .= $fjsOutputL;

        }

        // if we had javascript output, we need to setup a function to run
        // onSubmit for this entity, to run client side filter functions
        if ($fjsOutput != '') {

            $this->jsOutput .= "function check_{$this->varName}() {\n\n";


            // for each filter that has javascript, fun it's code
            foreach ($fjsList as $filt) {
                $this->jsOutput .= "\tif (!".$filt."_JS(document.SF{$this->varPrefix}.".$this->varName.".value)) return false;\n";
            }

            $this->jsOutput .= "\treturn true;\n";
            $this->jsOutput .= "}\n\n";

        }

        // return javascript for entites and filters only once
        return $fjsOutput.$this->jsOutput;

    }

     /**
      * set this entities variable name, and possibly it's originating value
      * after setting some values, it will run a function called setupEntity() if it's
      * part of this class. child classes should use it to do extra setting up at this point
      * THIS FUNCTION SHOULD NOT BE OVERRIDEN
      * @param $varName (string)    - the variable we're working with
      * @param $vP      (string)    - the prefix to use for variable name
      * @param $value   (string) [optional]     - initial value
      * @param $uJS     (bool)      - use javaScript or not
      */
    function setValue($varName, $vP, $value='', $uJS) {

        SM_sfLog($this,"setValue('$varName','$vP','$value', $uJS)");
        $this->varName      = $varName;
        $this->varPrefix    = $vP;
        $this->value        = $value;
        $this->useJS        = $uJS;

        // setup the varNameSansPrefix
        if (preg_match("/^(.+)\_SM[0-9]+$/",$this->varName,$m)) {
            $this->varNameSansPrefix = $m[1];
        }
        else {
            $this->varNameSansPrefix = 'test';
        }

        // run entityConfig method if it exists
        if (method_exists($this, 'entityConfig')) {
            $this->entityConfig();
        }

    }

     /**
      * Add a single key/value directive for configuration
      * this overrides SM_object's version, because it lets us
      * sfLog it
      * @param $key the key to use
      * @param $val the value to assign it
      */
    function addDirective($key, $val) {
        SM_sfLog($this, "addDirective($key,$val) : setting $key to $val");
        $this->directive[$key] = $val;
    }

     /**
      * add a filter to this input entity
      * @param $fType (string)  -   filter type to add
      * @param $msg (string) [optional] - message to output if filter fails
      * @param $varName [optional] sometimes an entity will want to override the variable name (see dateEntity)
      */
    function addFilter($fType, $msg='', $varName='') {

        SM_sfLog($this, "addFilter('$fType','$msg','$varName')");

        if ($varName == '')
            $varName = $this->varName;

        // verify and load this type
        if (!SM_sfLoadFilter($fType)) {
            SM_sfLog($this, "add: bad/unknown filter passed (filter module not found): $fType", true);
            return;
        }

        // good filter, add it
        SM_sfLog($this, "addFilter: adding filter of type [$fType] to varName [$varName]");

        // create the filter and add it to this entity
        $filterClass = $fType.'Filter';
        $newFilter = new $filterClass($this->dbH, $this->sessionH, $this->dbHL, $this);
        $newFilter->setInfo($varName, $msg, $this->useJS);

        // save to this entity
        $this->filterList[$fType] = $newFilter;
        return true;
    }


     /**
      * set a filters arguments
      * @param $fType the filter to edit
      * @param $args a hash of key/value arguments
      */
    function setFilterArgs($fType, $args) {

        SM_sfLog($this, "setFilterArgs()");

        if (!is_array($args)) {
            SM_sfLog($this, "setFilterArgs: args variable was not an array", true);
            return false;
        }

        if (!isset($this->filterList[$fType])) {
            SM_sfLog($this, "setFilterArgs: filter type [$fType] not found for this entity", true);
            return false;
        }
        else {
            SM_sfLog($this, "setting arguments for $this->varName, filter $fType:");
            $this->filterList[$fType]->configure($args);
        }

        return true;

    }

     /**
      * remove a filter
      * @param $fType filter type to remove
      * @return (bool) false if not found
      */
    function removeFilter($fType) {

        if (isset($filterList[$fType])) {
            // found, remove
            unset($filterList[$fType]);
            return true;
        }
        else {
            // not found
            return false;
        }

    }


     /**
      * apply filters. set's whether this entity is valid based on filter checks.
      */
    function applyFilters() {

        SM_sfLog($this, "applyFilters() : $this->varName()");

        /* if filterList isn't an array, this entity has no filters */
        if (!isset($this->filterList)) {
            $this->isValid = true;
            SM_sfLog($this, "no filters for this entity ($this->varName)");
            return;
        }

        /* apply each filter on this input entity */
        foreach($this->filterList as $filter) {
            SM_sfLog($this, "applyFilters(): running ".get_class($filter)." on variable $filter->varName");
            if (!$filter->filterThink()) {
                /* data was NOT valid */
                $this->isValid = false;
                /* compile fix message */
                $this->fixMessage .= $filter->badMessage.'<br>';
                SM_sfLog($this, "data FAILED filter check");
                // if we've found one bad, quit now
                return;
            }
            else {
                /* data was valid */
                SM_sfLog($this, "data passed filter check");
            }
        }

    }

     /**
      * output function for this entity. this should be overridden
      */
    function entityThink() {

        // nothing

    }

}


/**
 * load (include) an input entity module
 * if it's already loaded, ignore request
 * @param $iName (string) -  the input entity to load
 * @returns (bool) - if false, couldn't find it. true means successful load
 */
function SM_sfLoadEntity($iName) {

    // if already here, don't reload
    if (class_exists($iName.'Entity'))
        return true;

    // globalize
    global $SM_siteManager;

    // find file
    $iName = $SM_siteManager->findSMfile($iName.'Entity', 'sfEntities', 'inc', true);

    // load it up
    // include_once will not let duplicate modules be loaded
    include_once($iName);

    // all good and loaded, return
    return true;

}

?>