<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  data must match regular expression
 *
 *  directives:
 *  
 *      pattern     - perl compatible regular
 *                expression data must match
 *                against    /PATERN/
 *                default (undef)
 * 
 *
 *     reverse     - returns opposit value for match 
 */
class pregFilter extends SM_entityFilter {

    /** apply this filter */
    function filterThink() {
        SM_sfLog($this,"apply(): [$this->varName] contents: \"$this->data\", pattern: {$this->directive['pattern']}");
        if ($this->directive['pattern'] == '') {
            SM_sfLog($this,"apply(): warning: pattern is empty, assuming success",true);
            return true;
        }
        else {
            if ($this->directive['reverse'] == true) {
                return !(preg_match($this->directive['pattern'], $this->data));
            } 
            return preg_match($this->directive['pattern'], $this->data);
        }
    }

}

?>