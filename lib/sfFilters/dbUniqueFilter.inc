<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *                      
 *  input must be unique with respect to values in a table
 *  this filter will FAIL if the value entered for this HTML FORM field by the user is
 *  already in the database under <checkField>, in table <tableName>
 *
 *  for example, use for pre-checking for duplicating userNames or emails when signing up
 *  for membership
 *
 *  directives:
 *      tableName   - the database table to look in
 *      checkField  - the field in the table to check against
 *      whereClause - optional where clause
 *   
 */
class dbUniqueFilter extends SM_entityFilter {


    /** setup filter */
    function filterConfig() {            
    

        // require database
        if ($this->directive['dataBaseID'] == '')
            $this->directive['dataBaseID'] = 'default';       
        
        if (empty($this->dbHL[$this->directive['dataBaseID']])) {
            SM_fatalErrorPage("attempt to use dbUniqueFilter without a default database connection!",$this);
        }

    }


    /** apply this filter */
    function filterThink() {
    
        if ($this->directive['whereClause'] != '') {
            $wC = 'AND '.$this->directive['whereClause'];
        }

        $SQL = "SELECT {$this->directive['checkField']} FROM {$this->directive['tableName']} WHERE {$this->directive['checkField']}='$this->data' $wC";
        $rh = $this->dbHL[$this->directive['dataBaseID']]->simpleQuery($SQL);
        if (DB::isError($rh)) {
            SM_fatalErrorPage("unable to run query for duplicate value filter");
        }
        $rr = $this->dbH->fetchRow($rh);
        return (!is_array($rr));

    }

}

?>