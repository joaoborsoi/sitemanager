<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  data must not be empty  
 *
 */
class requiredFilter extends SM_entityFilter {

    /** apply this filter */
    function filterThink() {
        SM_sfLog($this,"apply(): [$this->varName] contents: \"$this->data\"");
        if ($this->data == '')
            return false;
        else
            return true;
    }

}

?>