<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *  data must consist entirely of digits    
 *                          
 *  directives:                     
 *   
 *  length      - must be exactly n digits long
 *            default (undef)
 *
 *  sizeMinMax  - must be between min and 
 *            max digits long. in the format 'min,max'
 *            default (undef)
 *
 *  decimal     - when true, must be a decimal number
 *            default (false)
 *            
 */
class numberFilter extends SM_entityFilter {

    /** apply this filter */
    function filterThink() {

        SM_sfLog($this,"apply(): [$this->varName] contents: \"$this->data\"");

        // build the pattern

        $pat = "/^\d";
        
        if (!isset($this->directive['decimal']))
            $this->directive['decimal'] = false;
        
        if (!$this->directive['decimal']) {
            if (isset($this->directive['length']))
                $pat .= '{'.$this->directive['length'].'}';
            elseif (isset($this->directive['sizeMinMax'])) {
                $pat .= '{'.$this->directive['sizeMinMax'].'}';
            }
            else {
                $pat .= '+';
            }
        }
        else {
            $pat .= "*\.\d+";
        }
        
        $pat .= "$/";

        SM_sfLog($this,"apply(): pattern [$pat]");      
        return preg_match($pat, $this->data);
    }

}

?>