<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**                     
 *  compare data with contents of another variable in this form
 *
 *  directives:
 *   
 *      compVar     - variable in this form to compare against
 *      cType       - compare type: equal,notEqual
 *      allowBlank  - allow comparisons to blank values
 *   
 ************************************************/
class varCompareFilter extends SM_entityFilter {

    /** setup filter */
    function filterConfig() {
            
        $this->directive['cType']       = 'equal';
        $this->directive['allowBlank']  = false;
    
    }

    /** apply this filter */
    function filterThink() {
    
        global $HTTP_POST_VARS;
        
        if (($this->directive['cType'] != 'equal')&&($this->directive['cType'] != 'notEqual')) {
            SM_sfLog($this, "cType was invalid: must be 'equal' or 'notEqual'",true);
            return;
        }       
        
        $realVar = $this->directive['compVar'].'_'.$this->getMyPrefix();
        
        // get the sumbmitted data from the variable we're looking for
        if ((isset($HTTP_POST_VARS[$realVar]))&&($HTTP_POST_VARS[$realVar] != '')) {
            if ($this->directive['cType'] == 'equal')
                return ($this->data == $HTTP_POST_VARS[$realVar]);
            else                
                return ($this->data != $HTTP_POST_VARS[$realVar]);
        }
        elseif ($this->directive['allowBlank']) {       
            if ($this->directive['cType'] == 'equal')
                return ($this->data == '');
            else                
                return ($this->data != '');
        }
        else {      
            if ($this->directive['cType'] == 'equal')
                return false;
            else
                return true;
        }
    
    }

}

?>