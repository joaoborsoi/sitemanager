<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

// defines
define("SM_UNKNOWN",0);             // undefined
define("SM_TEMPLATE",1);            // SM_layoutTemplate object
define("SM_CODEPLATE",2);           // SM_codePlate object

/**
 * SM_siteManagerRoot is the highest level class for implementing
 * a site using siteManager
 */
abstract class SM_siteManagerRoot extends SM_object {

    /** (obj ref) the Global Site Configuration object */
    var $siteConfig;

    /** (obj ref) the root layout template */
    var $rootTemplate;

    /** (obj ref array) list of modules we have loaded */
    var $moduleList;

    /** (hash) a list of inVars to propagate for each module loaded by the system */
    var $inVarPList;

    /**
     *  constructor. load global configuration
     *
     */
    function __construct() {

        global $SM_rootDir, $SM_siteID, $HTTP_SERVER_VARS;

        // check SITEID for validity
        if (!preg_match('/^\w+$/',$SM_siteID))
            SM_fatalErrorPage("\$SM_siteID was not set or invalid [$SM_siteID]");

        // detect accepted browser locale
        if (isset($HTTP_SERVER_VARS['HTTP_ACCEPT_LANGUAGE'])) {
            global $SM_clientLocale;
            $SM_clientLocale = preg_split(', *',$HTTP_SERVER_VARS['HTTP_ACCEPT_LANGUAGE']);
        }

        // inVarPList is blank array to start
        $this->inVarPList = array();

        // CREATE GLOBAL CONFIGURATION OBJECT
        $this->siteConfig = new SM_config($SM_rootDir."config/globalConfig.xsm");
        $this->_updateGlobals();

        // module list is blank
        $this->moduleList = array();

    }

    /**
     * load local site configuration file
     * @param $file (string) the site file to load. must specify path
     */
    function loadSite($file) {

        global $SM_develState;

        $this->siteConfig->loadSite($file);
        $this->_updateGlobals();

    }

    /**
     * search the GCS module paths for the requested module file, and include it
     * so it's available for instantiation. does NOT create an instance of it.
     * use this for modules that extend other modules
     * @param $moduleFile (string) the file to look for (NOT the class name of the module, the file it's in!)
     */
    function includeModule($moduleFile) {

        // find the requested file in the GCS modules path
        $moduleFile = $this->findSMfile($moduleFile, 'modules', 'mod');

        // try to require the module they've specified
        include_once($moduleFile);

    }

    /**
     * load a module and return a reference to it
     * this function will check the module paths if a path was not specified
     * @param $moduleFile (string) module to load. may include a path, in which case the specific name
     *                             passed will be included for use. otherwise it will search the module
     *                             path for the module to load
     * @param $moduleName (string) after including the module, the function will try to create an object
     *                             of the same type as the file name given - if this variable is set, however,
     *                             it will use this as the object name instead.
     * @return (obj ref) reference to new module, or null if not loaded
     */
    function &loadModule($moduleFile, $moduleName='') {

        // did they specify a path?
        if (preg_match('/[\/\\\]/',$moduleFile))  {

            // yes, they specified a path
            // default object name
            if ($moduleName == '')
                $moduleName = basename($moduleFile);

        }
        else {

            // default object name
            if ($moduleName == '') {

                // but if they included extension, leave that off the object name
                if (preg_match('/(.+)\.mod$/',$moduleFile,$m)) {
                    $moduleName = $m[1];
                }
                else {
                    $moduleName = $moduleFile;
                }

            }

        }

        // include the module file
        $this->includeModule($moduleFile);

        // generate module prefix
        $mPrefix = 'SM'.sizeof($this->moduleList);

        // did we get the module we're looking for?
        if (class_exists($moduleName)) {
            $newModule = new $moduleName($this->dbH, $this->sessionH, $mPrefix, $this->dbHL);
        }
        // check for SM_ module
        elseif (class_exists('SM_'.$moduleName)) {
            $SMmoduleName = 'SM_'.$moduleName;
            $newModule = new $SMmoduleName($this->dbH, $this->sessionH, $mPrefix, $this->dbHL);
        }
        else {
            SM_fatalErrorPage("could not load requested module: $moduleFile, $moduleName",$this);
        }

        // gather this modules list of inVars to propagate (if turned on per module)
        $miv = $newModule->getInVarP();
        if (is_array($miv)) {
            foreach ($miv as $iP) {
                if ((!empty($iP))&&(!in_array($iP, $this->inVarPList)))
                    $this->inVarPList[] = $iP;
            }
        }

        // add to our list of modules
        $this->moduleList[] =& $newModule;

        return $newModule;

    }

    /**
     * load and set the root template
     * @param $fName (string) the filename of the template/codeplate to load. will search template paths.
     *                        if type is left blank, you must include an extension so it knows which type
     *                        of template it's loading
     * @param $type (const) [optional] either SM_TEMPLATE or SM_CODEPLATE
     * @return (obj ref) layout template object
     */
    function &rootTemplate($fName, $type=SM_UNKNOWN) {

        // if type is unknown use extension
        if ($type == SM_UNKNOWN) {

            if (preg_match('/\.(.+)$/',$fName,$m)) {
                switch ($m[1]) {
                case 'tpt':
                    $type = SM_TEMPLATE;
                    break;
                case 'cpt':
                    $type = SM_CODEPLATE;
                    break;
                default:
                    SM_fatalErrorPage("SM_siteManager::rootTemplate called but unable to determine template type based on extension.
                                       Either specificy the extension, or specify the type",$this);
                }
            }
            else {
                // assume it's a template if there's no extension
                $type = SM_TEMPLATE;
            }
        }

        // load either a template or a code plate for the root template
        if ($type == SM_TEMPLATE)
            $this->rootTemplate = $this->loadTemplate($fName);
        else
            $this->rootTemplate = $this->loadCodePlate($fName);

        // return new template
        return $this->rootTemplate;

    }


    /**
     * search the GCS module paths for the requested CodePlate file, and include it
     * so it's available for instantiation. does NOT create an instance of it.
     * use this for codePlates that extend other codePlates
     * @param $cpFile (string) the file to look for (NOT the class name of the codePlate, the file it's in!)
     */
    function includeCodePlate($cpFile) {

        // find the requested file in the GCS modules path
        $cpFile = $this->findSMfile($cpFile, 'codePlates', 'cpt');

        // try to require the codePlate they've specified
        include_once($cpFile);

    }

    /**
     * load a codePlate and return a reference to it
     * this function will check the codeplate paths if a path was not specified
     * @param $cpFile (string) codePlate to load. may include a path, in which case the specific name
     *                             passed will be included for use. otherwise it will search the codePlate
     *                             path for the codePlate to load
     * @param $cpName (string) after including the codePlate, the function will try to create an object
     *                             of the same type as the file name given - if this variable is set, however,
     *                             it will use this as the object name instead.
     * @return (obj ref) reference to new SM_codePlate, or null if not loaded
     */
    function &loadCodePlate($cpFile, $cpName='') {

        // did they specify a path?
        if (preg_match('/[\/\\\]/',$cpFile))  {

            // yes, they specified a path
            // default object name
            if ($cpName == '')
                $cpName = basename($cpFile);

        }
        else {

            // default object name
            if ($cpName == '') {

                // but if they included extension, leave that off the object name
                if (preg_match('/(.+)\.cpt$/',$cpFile,$m)) {
                    $cpName = $m[1];
                }
                else {
                    $cpName = $cpFile;
                }

            }

        }

        // try to require the cp they've specified
        $this->includeCodePlate($cpFile);

        // did we get the cp we're looking for?
        if (class_exists($cpName)) {
            $newCp = new $cpName($this->dbH, $this->sessionH, $this->dbHL);
        }
        else {
            SM_fatalErrorPage("could not load requested codePlate: $cpFile, $cpName",$this);
        }

        return $newCp;

    }

    /**
     * search Global Configuratin System paths for certain SiteManager files. it can take a name
     * like "basicTemplate" and search the template paths for "basicTemplate.tpt", for example.
     *
     * @param $fName (string) the file to find
     * @param $GCSpath (multi) either a "dir" SECTION from global config, or an array of them (ie 'libs')
     * @param $extension (string) [optional] an extension to automaticall try and add
     * @param $fatalFNF (bool) when true, if the file wasn't found, will exit with SM_fatalErrorPage
     * @return (string) on success, will return the full path of the file found
     */
    function findSMfile($fName, $GCSpath, $extension='', $fatalFNF=true) {

        global $SM_develState;

        if ($SM_develState)
            SM_debugLog("attempting to load SiteManager resource file: $fName ($GCSpath, $extension)");

        // did they specify a path?
        if (!preg_match('/[\/\\\]/',$fName))  {

            // nope, they didn't specify a path. search paths from globalConfig (merge GLOBAL)
            $path = $this->siteConfig->getVar('dirs',$GCSpath,true);

            // check multiple paths
            if (is_array($path)) {
                $realPath = '';
                foreach ($path as $p) {
                    $check1 = $p.'/'.$fName;
                    $check2 = $p.'/'.$fName.'.'.$extension;
                    if (file_exists($check1)||file_exists($check2)) {
                        $realPath = $p;
                        break;
                    }
                }
                if ($realPath == '') {
                    if ($fatalFNF) {
                        if (is_array($path))
                            SM_fatalErrorPage("Requested SiteManager file ($fName) was not found in the path array:<br>".join($path,'<br>'));
                        else
                            SM_fatalErrorPage("Requested SiteManager file ($fName) was not found, and the search path array was empty.");
                    }
                    else {
                        return '';
                    }
                }

                $fName = $realPath.'/'.$fName;
            }
            else {
                $fName = $path.'/'.$fName;
            }

        }

        // add extension if not there
        if (!empty($extension) && !empty($fName)) {
            if (!preg_match('/\.'.$extension.'$/',$fName))
                $fName = $fName.'.'.$extension;
        }

        // check for file existance
        if ($fatalFNF) {
            if (!file_exists($fName)) {
                if (is_array($path))
                    SM_fatalErrorPage("Requested SiteManager file ($fName) was not found in the path array:<br>".join($path,'<br>'));
                else
                    SM_fatalErrorPage("Requested SiteManager file ($fName) was not found, and the search path array was empty.");
            }
        }

        return $fName;

    }

    /**
     * create a new instance of a template from the filename given
     * @param $fName (string) the template file to load. will automatically add .tpt if not included, and check
     *                        template path
     * @return (template)  a new SM_loadTemplate object
     */
    function &loadTemplate($fName) {

        // convert fName to SiteManager path'd version
        $fName = $this->findSMfile($fName, 'templates', 'tpt');

        // load the template
        $newTemplate = new SM_layoutTemplate($fName, $this->dbH, $this->sessionH, $this->dbHL,
					     $this->siteConfig->getVar('flags','caseSensitiveTags'));

        // return new template
        return $newTemplate;

    }

    /**
     * private function to define a database connection
     *
     */
    abstract function _defineConnection($id, $dbSettings);


    /**
     * create a new database connection using settings for this SITE
     */
    function dbConnect() {


        // check for multiple database connections
        if ($this->siteConfig->hasMultipleID('db')) {

            // multiple db connections
            $idList = $this->siteConfig->getIDlist('db');

            // loop through id's and setup database connections
            foreach ($idList as $dbID) {

                $dbSettings = $this->siteConfig->getSection('db',$dbID);
                $this->_defineConnection($dbID, $dbSettings);

            }

        }
        else {

            // setup single default database connection
            $dbSettings = $this->siteConfig->getSection('db');
            $this->_defineConnection('default',$dbSettings);

        }


    }

    /**
     * start sessions. this will create and setup an instance of SM_session
     * use addPersistent to add variables to keep persistant between sessions
     * @see SM_siteManagerRoot::addPersistent()
     */
    function startSessions() {

        $this->sessionH = new SM_session($this->siteConfig, $this->dbH, $this->dbHL);
        $this->sessionH->runSession();

    }

    /**
     * add persistent variables to the session. once variables have been added
     * to the persistent list, they will be kept alive throughout session-aware modules
     * once they obtain a value
     * @param $varName (string) the variable name to keep persistant
     */
    function addPersistent($varName) {

        if (isset($this->sessionH))
            $this->sessionH->addPersistent($varName);

    }

    /**
     * complete the page. called in a directive template to run all modules,
     * collect their output and display the final page in the root template.
     * this should be the last function that runs in a directive script
     * also saves session state upon completion
     */
    function completePage() {
        global $SM_scriptStartTime;

        // can't do this if there's no root template
        if (!isset($this->rootTemplate))
            SM_fatalErrorPage("SM_siteManager::completePage() - root template was not created",$this);

        // NOTE: we don't call the usual run() method here, because we might want to
        // do some processing between running the modules in the template and substituting
        // their values - ie, we want to be able to add javascript output to the template
        // AFTER the other modules have run, but BEFORE the javascript is actually sub'd into
        // this template

        // run all modules
        $this->rootTemplate->_runModules();

        // add javascript to the template
        // javascript debug popup window
        if ($this->siteConfig->getVar('debug','jsDebugWindow'))
            $this->rootTemplate->jsOutput .= _makeJSdebug();

        // if the root template includes an HTMLHEAD tag, and we should generate an ONLOAD
        // in the BODY tag, do it here
        if ((!empty($this->rootTemplate->tagList['htmlBody'])) && ($this->siteConfig->getVar('debug','jsDebugOnLoad'))) {
            $this->rootTemplate->directive['bodyONLOAD'] = 'SM_viewDebugLog()';
        }

        // sub in areas and such
        $this->rootTemplate->_subOutput();

        // gather output
        $output = $this->rootTemplate->htmlOutput;

        // javascript debug popup window
        if (($this->siteConfig->getVar('debug','jsDebugWindow'))&&($this->siteConfig->getVar('debug','jsDebugLink')))
            $output .= "<br><center><b>[<a href=\"javascript:SM_viewDebugLog()\">open debug window</a>]</b></center>";

        // save session state
        if (isset($this->sessionH))
            $this->sessionH->saveSession();

        // get script end execution time
        $microtime = explode(' ', microtime());
        $SM_scriptStopTime = $microtime[1].substr( $microtime[0], 1);
        $scriptLoadTime = $SM_scriptStopTime - $SM_scriptStartTime;

        // stop buffer
        $SM_outputBuffer = ob_get_contents();
        SM_debugLog('<br><b>OUTPUT BUFFER:</b><br><pre>'.$SM_outputBuffer.'</pre>');
        ob_end_clean();

        // if debugOnVar, output the debug_log first
        if (($this->siteConfig->getVar('debug','debugOnVar')) && ($GLOBALS['SM_debug']==1)) {
            SM_debugLog("<b>PAGE OUTPUT:</b><br>$output");
            $output = _makeErrorPage();
        }

        // send headers to stop browsers from caching page?
        if ($this->siteConfig->getVar('flags','sendNoCacheHeaders')) {
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");                                                                                                              // always modified
            header("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
            header("Pragma: no-cache");                          // HTTP/1.0
        }

        // echo page
        echo $output;

        // show page load time?
        if ($this->siteConfig->getVar('flags','showLoadTime'))
            echo "\n<!-- page load time: $scriptLoadTime seconds -->";

        // end!
        exit;

    }

    /**
     * private function used by the session system to get a list of inVars from the
     * modules currently loaded.
     * @return (array) a list of inVars from each module loaded
     */
    function _getModuleInVars() {

        return $this->inVarPList;

    }

    /**
     * compile a debug page that includes all current SiteManager information,
     * including configuration settings, database settings, session settings,
     * which modules are loaded, etc.
     */
    function fatalDebugPage() {

        $output = "<br><b>SITEMANAGER ROOT CLASS FATAL DEBUG PAGE</b><br><hr><br>";

        // database info
        if ($numDB = sizeof($this->dbHL)) {
            $output .= "<hr><b>DATABASE INFORMATION</b><hr><br>\n";
            $output .= "There are $numDB database connection(s) open<br><br>";
        }

        // session debug output
        if (isset($this->sessionH)) {
            $output .= "<hr><b>SESSION INFORMATION</b><hr><br>\n";
            $output .= $this->sessionH->dumpInfo();

            $output .= "Propagated InVars:<br>";
            if (is_array($this->inVarPList)) {
                foreach ($this->inVarPList as $ivpVar) {
                    $output .= "[$ivpVar]<br>";
                }
            }
            else {
                $output .= 'None<br>';
            }

            $output .= "<br><hr><br>\n";
        }

        // dump site config
        $output .= $this->siteConfig->dumpInfo();

        // run all modules, so we pickup information from modules in modules
        $this->rootTemplate->_runModules();

        // dump modules loaded information
        if (is_array($this->moduleList)) {
            $output .= "<hr><b>MODULE INFORMATION</b><hr>\n";
            foreach ($this->moduleList as $module) {

                $output .= $module->dumpInfo();
                $output .= "<hr>\n";

            }
        }


        SM_fatalErrorPage($output);

    }


    /**
     * refresh some global variables. called after reading in configuration files
     *
     */
    function _updateGlobals() {

        global $SM_develState, $SM_defaultLocale;

        $SM_develState      = $this->siteConfig->getVar('debug','develState');
        $SM_defaultLocale   = $this->siteConfig->getVar('localization','defaultLocale');

    }

}

?>
