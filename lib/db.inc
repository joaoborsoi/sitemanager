<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/* note, currently unused (weyrick) */

/**
 * retrieve a single piece of data from a database based on it's index number
 * @param $table the table to get the information from
 * @param $field the field to retrieve
 * @param $idxNum the index number of the row to retrieve
 * @return mixed - data from a table
 */
/*
function getData($table, $field, $idxNum) {

    $rh = executeSQL("SELECT $field FROM $table WHERE idxNum=$idxNum", $dbOverride); 
    if (num_rows($rh) == 0)
        return '';
    else {
        $rr = fetch_array($rh);
        return $rr[0];
    }

}
*/

/**
 * check for a duplicatate value in a table, based on the
 * information passed
 * @param $table the table to check for duplicates ing
 * @param $field the field to check against
 * @param $value the value to check
 * @param $whereClause (optional) an optional where clause to add into the query
 * @param $dbOverride (optional) a database to use instead of the default in $dbName
 * @return boolean - true if duplicate was found
 */
/*
function checkDupe($table, $field, $value, $whereClause="", $dbOverride='') {

    if ($whereClause != '')
        $whereClause = "AND $whereClause";

    $rh = executeSQL("SELECT $field FROM $table WHERE $field='$value' $whereClause", $dbOverride);
    if (num_rows($rh) > 0) {
        return 1;
    }

    return 0;

}
*/

/**
 * authenticate a user based on username / password combo sent.
 * MUST be called BEFORE any headers are sent!
 * returns the access level the user has been authenticated for
 * to authenticate, user must have $levelToAccess
 * see access.table for table definition
 * @param $levelToAccess an integer from 0-255. the level required to succesfully authenticate
 * @param $project (optional) the "Realm" to use for this authentication
 * @param $dbOverride (optional) a database to use instead of the default in $dbName
 * @return number - access level of user
 */
/*
function SM_DBauthenticate($levelToAccess, $project="Admin", $dbOverride='') {

    global $PHP_AUTH_USER, $PHP_AUTH_PW;

    if (!isset($PHP_AUTH_USER)) {
        Header("WWW-Authenticate: Basic realm=\"$project\"");
        Header("HTTP/1.0 401 Unauthorized");
        echo "You must supply a correct username / password for this page.";
        exit;
    }
    else {
        // verify username / password
        $rh = executeSQL("SELECT * FROM access WHERE userName='$PHP_AUTH_USER'",$dbOverride);
        $rr = fetch_array($rh);
        if (!$rr) {
            Header("HTTP/1.0 401 Unauthorized");
            echo "You must supply a correct username / password for this page.";
            exit;
        }
        else {
            // username ok, check password
            if ($rr["passWord"] == crypt($PHP_AUTH_PW, substr($rr["passWord"],0,2))) {
                if ($rr["level"] < $levelToAccess) {
                    Header("HTTP/1.0 401 Unauthorized");
                    echo "ACCESS DENIED: You do not have a high enough security level.";
                    exit;
                }
                return $rr["level"];
            }
            else {
                Header("HTTP/1.0 401 Unauthorized");
                echo "You must supply a correct username / password for this page.";
                exit;
            }
        }
    }
}
*/

?>
