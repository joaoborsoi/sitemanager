<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/** (definition) current object version. must match .XML files it reads */
define ("SMOBJECT_VERSION",   2);
/** (definition) current language definition version. must match .XML files it reads */
define ("SMLANGUAGE_VERSION", 1);

/**
 * base siteManager object. used to give siteManager objects common
 * configuration, database, and session functionality
 */

class SM_object {
    
    /** (array) a list of class logged items */
    var $classLog;

    /** (hash) configuration directive hash */
    var $directive;

    /** (obj ref) current session handler */
    var $sessionH;

    /** (obj ref) current default database handler */
    var $dbH;

    /** (hash) current hash of database handlers */
    var $dbHL;

    /** (string) section place holder for xml files (private)  */
    var $_XMLstate;

    /** (multihash) language independent text hash */
    var $liText;

    /** (bool) whether to override or append directives in loadConfig */
    var $owD = true;

     /**
      * this function configures the object. it accepts an associative array of 
      * configuration directive in key/value pair format
      * @param $cDir (hash) a key/value pair list of directive for this object
      * @return nothing
      */
    function configure($cDir) {
        if (is_array($cDir)) {
            foreach ($cDir as $k=>$v)
                $this->directive[$k] = $v;
        }
    }

     /**
      * Add a single key/value directive for configuration
      * @param $key (string) the key to use
      * @param $val (multi) the value to assign it
      * @param $overWrite (bool) when set to false, if the key is already present, 
      *                           the new value will be an array of the old values(s)
      *                           and the new one (instead of overwriting). defaults
      *                           to true.
      * @return nothing
      */
    function addDirective($key, $val='', $overWrite=true) {
        if ((!$overWrite)&&(isset($this->directive[$key]))) {
            if (is_array($this->directive[$key])) {
                $this->directive[$key][] = $val;
            }
            else {
                $this->directive[$key] = array($this->directive[$key], $val);
            }
        }
        else {        
            // new value
            $this->directive[$key] = $val;
        }
    }   


    /**
     * dump values of directive variable
     */
    function dumpDirectives() {

        global $_dOp;

        $_dOp = '';
        $_dOp = get_class($this)." :: directive list<br><br>\n";
        $_dOp .= "<ul>";
        array_walk($this->directive, '_ddPrint');
        $_dOp .= "</ul><br>";

        return $_dOp;

    }

    /**
     * dump object information. should be overriden by subclass for
     * anything more than just displaying the class log
     */
    function dumpInfo() {

        // should be overridden
        if (is_array($this->classLog)) {
            return $this->getClassLog();
        }
        else {
            return get_class($this).':: no information';
        }

    }

    /**
     * log a class debug message. used in dumpInfo
     *
     */
    function debugLog($msg) {
        $this->classLog[] = get_class($this)."::$msg";
    }

    /** 
     * retrieve the class log as a block of formatted text
     *
     */
    function getClassLog() {
        if (isset($this->classLog))        
            return "CLASS LOG:<BR>".join('<br>',$this->classLog).'<BR><BR>';
    }

    
    /**
     * retrieve some language independent text by ID
     * @param $id (string) the id to use to retrieve the text
     * @param $locale (string) the locale to use. defaults to directive['defaultLocale'] or GCS defaultLocale
     */
    function getText($id, $locale='') {

        global $SM_siteManager, $SM_clientLocale;

        // try to decide which locale to use
        if ($locale == '') {        

            if (($SM_siteManager->siteConfig->getVar('localization','useClientLocale'))&& 
                (isset($SM_clientLocale))) {
                // use locale specified by browser. might be array
                $locale = $SM_clientLocale;

            }
            elseif (!empty($this->directive['defaultLocale']))
                $locale = $this->directive['defaultLocale'];
            else {               
                $locale = $GLOBALS['SM_defaultLocale'];
            }
        }

        // if after all that, we still don't have a locale, default to english
        if (($locale == '')||( (is_array($locale))&&(sizeof($locale) == 0)  )) {
            $locale = 'en_US';
        }

        // loop through $locale if it's an array, return first value found
        if (is_array($locale)) {
            foreach ($locale as $l) {
                if (isset($this->liText[$l][$id]))
                    return $this->liText[$l][$id];
            }
            // no text found for this locale..
            return '(translation unavailable for your language)';
        }
        else {        
            return $this->liText[$locale][$id];
        }

    }

    /**
    * load the SMOBJECT section of an XSM XML file
    * This currently loads and sets directives for this object
    * @param $configFile (string) the xml file to load
    * @param $fatalFNF (bool) if true, execution will stop if the file is not found
    * @param $overWrite (bool) if true, values loaded will overwrite current directives
    *                          loaded from the XML file. if false, it will keep values and
    *                          make an array where keys are non-unique
    */
        
    function loadConfig($configFile,$fatalFNF = true, $overWrite = true){

        global $SM_siteManager;

        // overwrite directive
        $this->owD = $overWrite;

        // find and load config file
        if (($configFile = $SM_siteManager->findSMfile($configFile, 'config', 'xsm', $fatalFNF)) == '')
            return;

        // check cache?
        $willCache = false;
        if ($SM_siteManager->siteConfig->_siteTagList['GLOBAL']->getVar('cache','cacheSMObject','default')) {

            $willCache = true;
            if ($this->loadSerial($configFile, 'SMOBJECT', $this->directive))
                return;

        }

        // fall through: parse
        $this->_parse($configFile);

        // should we cache the file?
        if ($willCache) {
            $this->saveSerial($configFile, 'SMOBJECT', $this->directive);
        }

    }


    /**
     * load an SMLANGUAGE XML language file
     * @param $lFile (string) the file to read in     
     * @param $fatalFNF (bool) if true, execution will stop if the file is not found
     */
    function loadLanguage($langFile, $fatalFNF = true) {
    
        global $SM_siteManager;

        // find and load config file
        if (($langFile = $SM_siteManager->findSMfile($langFile, 'locale', 'xsm', $fatalFNF)) == '')
            return;

        // check cache?
        $willCache = false;
        if ($SM_siteManager->siteConfig->_siteTagList['GLOBAL']->getVar('cache','cacheSMLanguage','default')) {

            $willCache = true;
            if ($this->loadSerial($langFile, 'SMLANGUAGE', $this->liText))
                return;

        }

        // fall through: parse
        $this->_parse($langFile);

        // should we cache the file?
        if ($willCache) {
            $this->saveSerial($langFile, 'SMLANGUAGE', $this->liText);
        }
    
    }


    /**
     * cache an object currently in serialized PHP data format
     * @param $file (string) the file to write to
     * @param $prefix (string) a string to prefix the cache file name with
     * @oaran $data (mixed) the object to cache
     */
    function saveSerial($file, $prefix, &$data) {

        global $SM_siteManager, $SM_siteID;

        // must specify file. will strip directory later
        if ($file == '') {        
            SM_debugLog("saveSerial - file to cache was blank",$this);
            return;        
        }

        // use cache system?
        if (!$SM_siteManager->siteConfig->_siteTagList['GLOBAL']->getVar('cache','useCache','default')) {        
            return;
        }

        // strip directory
        if (($bnFile = basename($file)) == '')
            $bnFile = $file;

        // create file name
        $cacheDir = $SM_siteManager->siteConfig->_siteTagList['GLOBAL']->getVar('dirs','cache','default');        
        $file = $cacheDir.$SM_siteID.'-'.$prefix.'-'.$bnFile.'.inc';

        // only save if the file doesn't exist
        if (file_exists($file))
            return;

        $fp = fopen($file,'w');
        if (!$fp) {
            SM_debugLog("saveSerial: couldn't open cache file for output [$file] - check permissions",$this);
            return;
        }

        // serialize
        $ct = serialize($data);

        fwrite($fp, $ct);
        fclose($fp);

        // nobody should read it but webserver
        chmod($file,0600);

        if ($GLOBALS['SM_develState'])        
            SM_debugLog("saveSerial: created cache file $file",$this);

    }

    /**
     * read in a serialized object, assign value to $data
     * @param $file (string) the file to read in and unseralize     
     * @param $prefix (string) a string to prefix the cache file name with
     * @param $data (mixed) store results of unserialized data in this variable
     */
    function loadSerial($file, $prefix, &$data) {

        global $SM_siteManager, $SM_siteID;

        // use cache system?
        if (!$SM_siteManager->siteConfig->_siteTagList['GLOBAL']->getVar('cache','useCache','default')) {        
            return;
        }

        // make file name
        $cacheDir = $SM_siteManager->siteConfig->_siteTagList['GLOBAL']->getVar('dirs','cache','default');
        $file = $cacheDir.$SM_siteID.'-'.$prefix.'-'.basename($file).'.inc';

        if (!file_exists($file)) {        
            SM_debugLog("loadSerial: file not found [$file]",$this);
            return false;
        }
        $fp = fopen($file,'r');
        $ct = fread($fp, filesize($file));
        fclose($fp);

        $data = unserialize($ct);

        if ($GLOBALS['SM_develState'])        
            SM_debugLog("loadSerial: loaded cached file $file",$this);

        return true;

    }

    /**
    * function triggered when an XML start element appears
    * here we will parse tag's we recognize from the config file
    * NOTE: this function is private and for use by the smObject XML parser only
    * @param $parser (reference) the XML parser
    * @param $name (string) the tag found
    * @param $attrs (hash) the attributes this tag carried
    */
    function _startElement($parser, $name, $attrs) {
    
        // update state for this element
        $this->_XMLstate['in'.$name] = true;

        // examine tag
        switch ($name) {
        case 'SMOBJECT':
            // check version
            if ($attrs['VERSION'] > SMOBJECT_VERSION)
                SM_fatalErrorPage("version incompatibility with config file $this->file (I'm version ".SMOBJECT_VERSION.", it's version {$attrs['VERSION']})",$this);
            break;
        
        case 'SMLANGUAGE':
            // check version
            if ($attrs['VERSION'] > SMLANGUAGE_VERSION)
                SM_fatalErrorPage("version incompatibility with language file $this->file (I'm version ".SMLANGUAGE_VERSION.", it's version {$attrs['VERSION']})",$this);
            break;

        case 'DIRECTIVE':
            // no functionality
            break;

        case 'LDEF':
            $this->_XMLstate['cLocale'] = $attrs['LOCALE'];
            break;

        case 'TEXT':
            $this->_XMLstate['ctID'] = $attrs['ID'];
            break;

        case 'VAR':
            // we're getting vars for a section
            $name = $attrs['NAME'];

            // do bools correctly
            if (strtoupper($attrs['VALUE']) == 'TRUE')
                $val = true;
            elseif (strtoupper($attrs['VALUE']) == 'FALSE')
                $val = false;
            else
                $val = $attrs['VALUE'];
    
            // if we're in a valid SMOBJECT and DIRECTIVE tag, 
            // add directive from the directive section
            if (($this->_XMLstate['inSMOBJECT']) && ($this->_XMLstate['inDIRECTIVE'])) {
                $this->addDirective($name,$val,$this->owD);
            }
            break;

        case 'SM_XML':
            // root tag
            break;
        default:
            // ignore unknown tags
            break;
        }

    }
    
    function _endElement($parser, $name) {

        // update state for this element
        $this->_XMLstate['in'.$name] = false;

    }

    function _cDataHandler($parser, $data) {

        if (trim($data) == '')
            return;        

        // only handle data from language file
        if ((!($this->_XMLstate['inSM_XML'])&&
             ($this->_XMLstate['inSMLANGUAGE'])&&
             ($this->_XMLstate['inLDEF'])&&
             ($this->_XMLstate['inTEXT'])) ||
             (empty($this->_XMLstate['cLocale']))||(empty($this->_XMLstate['ctID']))) {
            return;
        }

        $cLocale = $this->_XMLstate['cLocale'];
        $ctID  = $this->_XMLstate['ctID'];
        $this->liText[$cLocale][$ctID] .= $data;

    }

    // PARSE XML FILES
    /**
    * private function for parsing the XML file. actually creates XML parser
    * and sets up element handlers
    * NOTE: this function is private and for use by the smObject XML parser only
    * @param $file (string) the file to parse
    */
    function _parse($file) {
    
        if (empty($file))
            SM_fatalErrorPage("XML file to parse was not set",$this);

        $xml_parser = xml_parser_create();
        xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING,true);
        xml_set_object($xml_parser, $this);
        xml_set_element_handler($xml_parser, "_startElement", "_endElement");
        xml_set_character_data_handler($xml_parser,'_cdataHandler');
        if (!($fp = fopen($file, "r", 1))) {
            SM_fatalErrorPage("smObject: could not open XML input from file $file",$this);
        }
        
        while ($data = fread($fp, 4096)) {
            if (!xml_parse($xml_parser, $data, feof($fp))) {
                SM_fatalErrorPage(sprintf("smObject: XML error: %s at line %d of $file",
                            xml_error_string(xml_get_error_code($xml_parser)),
                            xml_get_current_line_number($xml_parser)),$this);
            }
        }
        xml_parser_free($xml_parser);

    }

}

?>
