<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 *
 *  the module base class. modules are objects that are placed into templates to give a site
 *  site functionality
 *
 *  all siteManager modules should extend this class (or a descendent of it)
 *
 */
class SM_module extends SM_object {

    /**
     * (string) the prefix this module will use for variables it needs
     *          this will be a unique string generated at the modules instantiaion
     *
     */
    var $modulePrefix;

    /** (string) the final output of the module */
    var $moduleOutput;

    /**
     * (hash)   hash of possible incoming variables this module might use. should be defined in
     *          moduleConfig through addInVar
     */
    var $inVar;

    /**
     * (hash)  hash of the variable types we're expecting inVar's to be. key is inVar
     *
     */
    var $inVarType;

    /** (hash) inVar var name is the key, value is bool that determines whether an inVar should be included in URL links */
    var $inVarProp;

    /** (hash)   a hash of styles to be used in this module  */
    var $style;

    /** (string) the final output of the java script section of the module */
    var $jsOutput;

    /** (array)  a list of smart forms this module contains. array of references to smartform objects */
    var $formList;

    /** (array) a list of methods to call BEFORE moduleThink() */
    var $preThinkList;

    /** (array) a list of methods to call AFTER moduleTnink() */
    var $postThinkList;

    /** (array) a list of methods to call AFTER moduleConfig() */
    var $postConfigList;

    /**
     * construction. does some initializing. will run moduleConfig() method if it
     * exists in this class
     *
     * @param $dbH (obj ref)       - SiteManager default database handler
     * @param $sessionH (obj ref)  - SiteManager session handler
     * @param $mPrefix (string)    - SiteManager determinded module prefix
     * @param $dbHL (hash)         - SiteManager database handler hash
     */
    function __construct(&$dbH, &$sessionH, $mPrefix, &$dbHL) {

        global $SM_siteManager;

        // configure module
        $this->dbH              =& $dbH;
        $this->sessionH         =& $sessionH;
        $this->modulePrefix     = $mPrefix;
        $this->dbHL             =& $dbHL;

        // setup default required directive
        $this->directive['tableBorder']         = 0;
        $this->directive['tableWidth']          = "100%";
        $this->directive['tableCellPadding']    = 0;
        $this->directive['tableCellSpacing']    = 0;
        $this->directive['tableBgColor']        = '';
        $this->directive['tableAlignment']      = '';
        $this->directive['centerInTable']       = false;
        $this->directive['outputInTable']       = true;
        $this->directive['outputInSpan']        = true;

        $this->directive['dieOnBadInVarType']   = true;

        $this->directive['propagateInVars']     = $SM_siteManager->siteConfig->getVar('flags','propagateInVarDefault');
        $this->directive['strictInVars']        = $SM_siteManager->siteConfig->getVar('flags','strictInVars');

        // clear out javascript variable
        $this->jsOutput = '';

        // no forms to start
        $this->numSmartForms = 0;
        $this->formList      = array();

        // run config for module, if it has one
        if (method_exists($this, "moduleConfig"))
            $this->moduleConfig();

        // run postconfig methods
        if ((isset($this->postConfigList))&&(is_array($this->postConfigList))) {
            foreach ($this->postConfigList as $method) {
                call_user_func(array(&$this,$method));
            }
        }

    }


     /**
      * this function will cause the module to "run" itself.
      * that is, execute methodThink() method, and return
      * the results of the module output as a string.
      *
      * @return (string) the module output
      */
    function run() {

        // get POST and GET vars ready for examination
        global $HTTP_POST_VARS, $HTTP_GET_VARS;
        $sVars = array_merge((array)$HTTP_POST_VARS,(array)$HTTP_GET_VARS);

        // setup $inVar for access.

        // this sets up variables that were given to us via GET (without prefix), or
        // from a POST (with prefix), or that were define globally somewhere else in
        // SiteManager, possibly from sessions

        // if this module has inVars...
        if ((isset($this->inVar))&&(is_array($this->inVar))) {

            // ... loop through them
            foreach ($this->inVar as $v => $k) {


                // strict inVars?
                if (!$this->directive['strictInVars']) {

                    //
                    // run through all the HTTP POST vars  to see if we can find our variable
                    // name hidden in among variables posted from other modules forms (ie,
                    // different prefix) This will be overridden if a value is found from
                    // a variable with our prefix, or a global without our prefix has a value
                    //
                    //
                    foreach ($sVars as $vName=>$vVal) {
                        if (preg_match("/^$v\_SM[0-9]+$/",$vName)) {

                            // HTTP_*_VARS had a declared inVar with
                            // a prefix. set the value of the inVar to
                            // the POST'd value. note this might be overriden
                            // in the next section (based on current global
                            // value)

                            // check type
                            $this->_checkType($v, $vVal);

                            // setup inVar for access
                            $this->inVar[$v] = $vVal;

                        }
                    }

                }

                // check current global value of the inVar, and the prefixed
                // version of the inVar
                //
                // this is good for retrieving session variables and
                // those passed via GET
                //
                // checking the prefixed version is good for capturing
                // POST'd and propagated inVars (which both pass by prefix)
                // this will NOT capture POST'd variables from smartforms
                // that were not in our module (ie, different prefix)
                // that's handled above, and overidden here if necesary
                //
                $vP = $v.'_'.$this->modulePrefix;
                global $$v, $$vP;
                if (isset($$v)||isset($$vP)) {

                    // if we have $$vP, use that (the prefixed version),
                    // otherwise use the non prefixed version
                    if (isset($$vP))
                        $gpVal = $$vP;
                    else
                        $gpVal = $$v;

                    // check type
                    $this->_checkType($v, $gpVal);

                    // setup inVar for access
                    $this->inVar[$v] = $gpVal;

                }

            } // end foreach

        } // end if isset($this->inVar)

        // see if we should load or not
        // this allows the user to run some code that will decide
        // if this module should display anything or not
        if (!$this->canModuleLoad()) {
            $this->moduleOutput = '';
            return '';
        }

        // make the start of our table to wrap the output in
        if ($this->directive['outputInSpan'])
            $this->moduleOutput = "<SPAN ID=\"$this->modulePrefix\">\n";

        if ($this->directive['outputInTable']) {

            $this->moduleOutput .= "<TABLE WIDTH=\"{$this->directive['tableWidth']}\" BORDER=\"{$this->directive['tableBorder']}\" CELLSPACING=\"{$this->directive['tableCellSpacing']}\" CELLPADDING=\"{$this->directive['tableCellPadding']}\"";
            if ($this->directive['tableBgColor'] != '')
                $this->moduleOutput .= " BGCOLOR=\"{$this->directive['tableBgColor']}\" ";
            if (!empty($this->directive['tableHeight']))
                $this->moduleOutput .= " HEIGHT=\"{$this->directive['tableHeight']}\" ";
            $this->moduleOutput .= ">\n";

            $this->moduleOutput .= "<TR>";

            if ($this->directive['tableAlignment'] != '')
                $this->moduleOutput .= '<TD ALIGN="'.$this->directive['tableAlignment'].'">';
            elseif ($this->directive['centerInTable'])
                $this->moduleOutput .= '<TD ALIGN="CENTER">';
            else
                $this->moduleOutput .= '<TD>';

        }

        // run prethink methods
        if ((isset($this->preThinkList))&&(is_array($this->preThinkList))) {
            foreach ($this->preThinkList as $method) {
                call_user_func(array(&$this,$method));
            }
        }

        // this will get the ouput into our output variable.
        $this->moduleThink();

        // run postthink methods
        if ((isset($this->postThinkList))&&(is_array($this->postThinkList))) {
            foreach ($this->postThinkList as $method) {
                call_user_func(array(&$this,$method));
            }
        }

        // grab javascript output from any of the smartforms
        if ((isset($this->formList))&&(is_array($this->formList))) {
            foreach ($this->formList as $theForm) {
                if ($theForm->jsOutput != '') {
                    if ($this->jsOutput == '') {
                        // we need to add <SCRIPT> header since this is first js code
                        $this->jsOutput = '<SCRIPT LANGUAGE="JavaScript">';
                        $this->jsOutput .= "\n\n// ".get_class($this)."\n";
                        $this->jsOutput .= $theForm->jsOutput;

                    }
                    else {
                        // we already have stuff in there so <SCRIPT> is already there
                        $this->jsOutput .= $theForm->jsOutput;
                    }
                }
            }
        }

        // if there was any javascript output, we'll automatically add a </SCRIPT>
        if (($this->jsOutput != '')&&(!preg_match('/<\/SCRIPT>\s*$/i',$this->jsOutput)))
            $this->jsOutput .= "</SCRIPT>";

        // end table
        if ($this->directive['outputInTable']) {
            $this->moduleOutput .= "</TD></TR>\n</TABLE>\n";
        }

        // end span
        if ($this->directive['outputInSpan'])
            $this->moduleOutput .= "</SPAN>\n";

        // that's all, folks.
        return $this->moduleOutput;
    }



    /**
     * private function used to verify the type of an inVar
     *
     * @param $var (string) the inVar variable to check. must have been declared by addInVar()
     * @param $val (mixed) the value to check it against
     * @return (nothing) will not return if invalid type and dieOnBadInVarType is true
     */
    function _checkType($var, $val) {

        // if it's type isn't set, ignore
        if (!isset($this->inVarType[$var]))
            return;

        // if it's a blank string, ignore
        if ($val == '')
            return;

        $vOk = false;

        switch ($this->inVarType[$var]) {
        case 'number':
        case 'int':
        case 'integer':
            $vOk = is_numeric($val);
            break;
        case 'array':
            $vOk = is_array($val);
            break;
        case 'object':
            $vOk = is_object($val);
            break;
        case 'double':
            $vOk = is_double($val);
            break;
        case 'boolean':
        case 'bool':
            $vOk = is_bool($val);
            break;
        case 'string':
            $vOk = is_string($val);
            break;
        default:
            // these should never match, if they do it's invalid
            SM_fatalErrorPage("checkType() found unknown type: ".$this->inVarType[$var],$this);
            break;
        }

        if (!$vOk) {
            $msg = "A required variable appears to be of an invalid type ($var / $val)";
            if ($this->directive['dieOnBadInVarType'])
                SM_fatalErrorPage($msg,$this);
            else
                SM_debugLog($msg,$this);
        }

    }

    /**
     * this method is run after moduleConfig and before moduleThink. you can override this method
     * in your module to have it decide whether it should show itself or not. this might come up
     * for example, when you need to check a database a see if any values exist, and if not don't
     * display the module. since it is run after moduleConfig, you can use all declared inVars
     * and directives to make the descision.
     *
     * if this method is not overriden, it returns true which will run moduleThink as normal
     *
     * @return (bool) true if loading should continue, false if we should NOT run moduleThink() and stop here
     */
    function canModuleLoad() {
        return true;
    }

     /**
      * this function will return any javascript code associated with this module
      * use $this->sayJS() to add text to the javascript section
      *
      * @return (string) module javascript output
      */
    function getJS() {
        return $this->jsOutput;
    }


    /**
     * this function creates a link to the desired URL, and adds the session information automatically
     * NOTE: this is a wrapper for the $this->sessionH->hLink function
     * @param $link (string) the URL location to link to
     * @param $text (string) the text to link (ie, the text between <A></A>
     * @param $class (string) [optional] the CLASS (ie, style sheet element) to use for this text
     * @param $extra (string) [optional] extra data to stick into the <A> tag. may be CLASS, TARGET, etc.
     * @param $display (string) [optional] (default false) if set to true, it will echo immediately. otherwise, only returns output
     */
    function hLink($link, $text, $class='', $extra='') {
        return $this->sessionH->hLink($link, $text, $class, $extra);
    }

    /**
     * Create a new SmartForm for use in this module
     * @return (object: SM_smartForm) reference to a new SmartForm object
     */
    function &newSmartForm() {

        // create the new form
        $myForm = new SM_smartForm($this->modulePrefix, $this->dbH, $this->sessionH, $this->dbHL);

        // add it to our list
        $this->formList[] =& $myForm;

        // return the new smartform to them
        return $myForm;

    }

    /**
     * declare that we are looking for certain variables to be passed (inVar)
     * (either POST, GET, or from sessions) for use in this module
     * optionally give a default value to use if it wasn't passed
     * optionally describe which variable type the value should have (for security)
     * optionally decide whether this inVar should be propagated or not
     *
     * @param $varName (string) the variable name we're looking for
     * @param $defaultVal (mixed) [optional] the default value to use, if it wasn't passed
     * @param $expectType (string) [optional] the expected type of the inVar. must be a valid PHP type
     * @param $propagate (bool) [optional] whether to include this inVar in links and FORMS when set
     * @return nothing
     */
    function addInVar($varName, $defaultVal=NULL, $expectType='', $propagate=NULL) {

        // default some expectType's
        if (isset($defaultVal)) {
            switch (gettype($defaultVal)) {
            case 'integer':
                $expectType='integer';
                break;
            case 'array':
                $expectType='array';
                break;
            case 'object':
                $expectType='object';
                break;
            }
        }

        // inVar types
        switch ($expectType) {
        case 'bool':
        case 'boolean':
        case 'string':
        case 'double':
        case 'number':
        case 'int':
        case 'integer':
        case 'array':
        case 'object':
            $this->inVarType[$varName] = $expectType;
            break;
        case '':
            break;
        default:
            SM_fatalDebugPage("addInVar() - expectType was unknown: $expectType",$this);
        }

        // add inVar with default value
        $this->inVar[$varName]      = $defaultVal;

        // propagation?
        if ($propagate === NULL) {
            $propagate = $this->directive['propagateInVars'];
        }

        if (is_string($propagate)) {
            if ($propagate == 'true') $propagate = true;
            if ($propagate == 'false') $propagate = false;
        }

        $this->inVarProp[$varName]  = $propagate;

    }

    /**
     * get the value of a variable defined through inVar
     * optionally pass a second parameter that the value should be set to
     * if the variable you're requesting is empty
     * use this to retrieve variables declared as inVar's
     * @param $varName (string) the variable to retrieve
     * @param $default (mixed) if $varName is empty(), this default value will be returned instead
     * @return (mixed) current value of the inVar requested (or $default or NULL if not set)
     */
    function getVar($varName, $default=NULL) {
        if (isset($this->inVar[$varName]))
            return $this->inVar[$varName];
        else
            return $default;
    }


    /**
     * declare that a style sheet element will be used in this module
     * @param $sName (string) the internal style name to use
     * @param $style (string) [optional] the style sheet element to reference (external, from template)
     *                                  defaults to the same name as $sName
     * @return nothing
     */
    function addStyle($sName, $style='') {
        if ($style == '') {
            $style = $sName;
        }
        $this->style[$sName] = $style;
    }

    /**
     * retrieve the currently configured style sheet element (external) based on the
     * internal named passed
     * @param $sName (string) the internal style sheet name
     * @return (string) the currently configured style sheet element value (external)
     */
    function getStyle($sName) {
        return $this->style[$sName];
    }

     /**
      * override a style sheet value
      * @param $sName (string)  the internal module style name
      * @param $style (string)  the style element to use for this style name, which is presumably defined in the
      *                         template's CSS style sheet somewhere
      * @return (nothing)
      */
    function setStyle($sName, $style) {
        if (!isset($this->style[$sName]))
            SM_fatalErrorPage("Style name $sName was not found in this module",$this);
        else
            $this->style[$sName] = $style;
    }

     /**
      * a simple wrapper function that adds $text to this modules final output
      * @param $text (string) text to be added to final output
      * @param $sName (string) internal style name to use for this output. will wrap in SPAN tag
      * @return (nothing)
      */
    function say($text, $sName='') {
        if (($sName != '') && (isset($this->style[$sName])))
            $text = "<SPAN CLASS=\"{$this->style[$sName]}\">$text</SPAN>\n";
        $this->moduleOutput .= $text;
    }


     /**
      * same as say(), but adds a <BR> to the end of the output
      * @param $text (string) [optional] text to be added to final output. leave blank for just a <BR>
      * @param $sName (string) [optional] internal style name to use for this output. will wrap in SPAN tag
      * @return (nothing)
      */
    function saybr($text='',$sName='') {
        $this->say($text."<BR>\n",$sName);
    }


     /**
      * this wraps the say() function
      * @param $text (string) text to be added to final output
      * @param $sName (string) internal style name to use for this output. will wrap in SPAN tag
      * @return (nothing)
      */
    function write($text, $sName='') {
        $this->say($text,$sName);
    }


     /**
      * this wraps the saybr() function
      * @param $text (string) [optional] text to be added to final output. leave blank for just a <BR>
      * @param $sName (string) [optional] internal style name to use for this output. will wrap in SPAN tag
      * @return (nothing)
      */
    function writeln($text='',$sName='') {
        $this->say($text."<BR>\n",$sName);
    }

     /**
      * a simple wrapper function that adds $text to this modules
      * javascript final output
      * if it was blank when we started, we'll automatically add the javascript start tag
      * it will be ended automatically when we're run
      * @param $text (string) text to be added to final javascript output
      * @param $addHeader (bool) [optional] if true (default), it will attempt to add the <SCRIPT> tags for you
      * @return (nothing)
      */
    function sayJS($text,$addHeader=true) {

        if (empty($this->jsOutput)&& $addHeader) {

            // auto start
            $this->jsOutput = '<SCRIPT LANGUAGE="JavaScript">';
            $this->jsOutput .= "\n\n// ".get_class($this)."\n";

        }

        $this->jsOutput .= $text;
    }


    /**
     * retrieve a list of inVars that should be propagated
     * propagated inVars will always be prefixed
     *
     * @return (array) a list of inVars that should be propagated or NULL if none
     */
    function getInVarP() {

        if ((!isset($this->inVar))||(!is_array($this->inVar)))
            return NULL;

        $ivList = array();
        foreach ($this->inVar as $iV => $d) {
            if ($this->inVarProp[$iV]) {
                $ivP = $iV.'_'.$this->modulePrefix;
                $ivList[] = $ivP;
            }
        }

        return $ivList;

    }


    /**
     * dump the value of all variables we've setup as inVars
     * for debugging purposes
     * @param $echo (bool) [optional] if set to true, the debug output will be echo'd
     *                                (as opposed to added to this modules output)
     * @return (nothing)
     */
   function dumpInVars($echo=false) {

        if (!sizeof($this->inVar))
            return;

        global $_dOp;

        $_dOp = 'registered inVar information (inVar => value): <br><ul>';

        array_walk($this->inVar, '_ddPrint');

        $_dOp .= '<br>';

        foreach ($this->inVarProp as $iv => $val) {
            if ($val)
                $_dOp .= "$iv *WILL* propagate<br>\n";
            else
                $_dOp .= "$iv will NOT propagate<br>\n";
        }
        $_dOp .= '<br>';

        $_dOp .= '</ul><br>';

        if ($echo)
            echo $_dOp;
        else
            return $_dOp;

   }

   /**
    * dump information on this module, for debugging purposes
    *
    */
    function dumpInfo() {

        $output = "Module Name: ".get_class($this)."<hr><br>\n";
        $output .= $this->dumpInVars();
        $output .= $this->dumpDirectives();
        $output .= "module has ".sizeof($this->formList)." smartForm objects<br><br>\n";

        // dump smartForm information, if any
        if (sizeof($this->formList)) {
            foreach ($this->formList as $smartForm) {
                $output .= $smartForm->dumpInfo();
            }
        }

        $output .= "<br>";

        return $output;

    }


}


?>
