<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 * check the object passed to see if it's a PEAR database error.
 * if it is, call a fatal error page outputting relavent error
 * messages
 * @param $rh (object) the PEAR DB result handle to check for an error
 * @param $obj (object) the object who called the error
 * @return (nothing) will not return if there is an error
 */
function SM_dbErrorCheck($rh, $obj=NULL) {

    global $SM_develState;

    if (DB::isError($rh)) {

        $msg = "Database Query Error";

        if ($SM_develState) {
            // add SQL query if in devel state
            $msg .= ':  '.$rh->message."<br>\n".$rh->userinfo;
        }
        SM_fatalErrorPage($msg,$obj);
    }

}

/**
 * get the value of a variable that has been prefixed.
 * @param $varName (string) the variable name to retrieve 
 */
function SM_getVar($varName) {

    // check prefixed values first
    global $HTTP_POST_VARS, $HTTP_GET_VARS;
    $vArray = array_merge($HTTP_POST_VARS, $HTTP_GET_VARS);
    foreach ($vArray as $vName=>$vVal) {
        if (preg_match("/^$varName\_SM[0-9]+$/",$vName)) {
            return $vVal;
        }
    }

    // fall through, return current global
    global $$varName;
    return $$varName;

}


/**
 * return a random integer between 0 and $max 
 * @param $max (number) maximum random number to return
 * @return (number) a random number
 */
function SM_randomint($max) {
    static $startseed = 0; 
    if (!$startseed) {
        $startseed = (double)microtime()*getrandmax(); 
        srand($startseed);    
    }
    return(rand()%$max); 
}

/**
 * take a seperated list of items, and return true if $findItem is
 * one of the elements.
 * @param $findItem (string) the item to find in the list (needle)
 * @param $cdList (string) the delimited list to search (haystack)
 * @param $delim (string) [optional] the delimiter to use. default is comma
 * @return (bool) true if needle was found in haystack, else false
 */
function SM_isInSet($findItem, $cdList, $delim=',') {

    $lArray = split($delim, $cdList);
    for ($i = 0; $i < count($lArray); $i++)
        if ($findItem == $lArray[$i])
            return true;

    return false;
}

/**
 * return a list suitable for passing as a GET variable list, of the variables in the current script
 * 
 * when using the excludeList, the key for each item of the hash is what's important. it shoud be
 * set to the variable name you wish to have excluded from the list of variables returned. the value
 * for that item in the hash is not important, as long as it's not blank. see the example.
 *
 * example usage:
 *  // create a suitable GET variable list, but don't include sessionID or dontInclude
 *  $transferURL = SM_getScriptVars(array('sessionID'=>'1','dontInclude'=>'1'));
 *  header("Location: $PHP_SELF?$transferURL");
 *  exit;
 *
 * @param $excludeList (hash) [optional] a hash of variable that should NOT be included in the lsit, 
 *                      should they be found. key should be the variable name, value should be non-false
 * @return (string) a list of variables suitable for passing as a GET
 */
function SM_getScriptVars($excludeList=NULL) {

    global $HTTP_POST_VARS, $HTTP_GET_VARS;

    if ((is_array($HTTP_GET_VARS))&&(count($HTTP_GET_VARS)))
        $vars = $HTTP_GET_VARS;
    else
        $vars = $HTTP_POST_VARS;

    if (!is_array($vars))
        return '';

    reset($vars);

    // gather vars
    $varList = '';
    while (list($key, $val) = each($vars)) {
        if (is_array($excludeList)) {
            if ((!isset($excludeList[$key]))||($excludeList[$key] == ''))
                $varList .= "&$key=".urlencode($val);
        }
        else {
                $varList .= "&$key=".urlencode($val);
        }

    }

    $varList = substr($varList, 1);
    return $varList;        

}


/**
 * reload (via Location header) the current script with all variables in tact,
 * minus a hash of variables not to include and plus a hash of variabless to be included
 * 
 * when using the excludeList and includeList, the key for each item of the hash is what's important. it shoud be
 * set to the variable name you wish to have excluded/included from the list of variables used. the value
 * for that item in the hash is not important, as long as it's not blank.
 *
 * @param $exludeList (hash) a hash of variables to be excluded from the reload
 * @param $includeList (hash) a hash of variables to be included into the reload
 * @return nothing - always exits
 * @see SM_getScriptVars
 */
function SM_reloadScript($excludeList=undef,$includeList=undef) {

    global $PHP_SELF;
    
    $varList = SM_getScriptVars($excludeList);

    // new vars to add
    if (is_array($includeList)) {
        
        while (list($key, $val) = each($includeList)) {
            $varList .= "&$key=".urlencode($val);
        }
    
    }

    $newLoc = $PHP_SELF.'?'.$varList;

    header("Location: $newLoc");
    exit();

 }


/**
 * return a "pretty name". ie, split on studly caps
 * this will take a string such as "thisIsAStringTest1" and
 * return "This Is A String Test 1"
 *
 * @param $name (string) the string to convert
 * @return (string) the newly formatted string
 */
function SM_prettyName($name) {

    // firstName = First Name
    if (preg_match("/[a-z][A-Z]/",$name)) {
       $name = preg_replace("/([a-z])([A-Z])/","\\1%\\2",$name);
       $wordList = explode('%',$name);
       $name = '';
       foreach ($wordList as $word) {
            $name .= ucfirst($word).' ';
       }
       chop($name);
    }
    // FirstName = First Name
    elseif (preg_match("/[A-Z][a-z]/",$name)) {     
       $name = preg_replace("/([A-Z])([a-z])/","\\1%\\2",$name);
       
       $wordList = explode('%',$name);
       $name = '';
       foreach ($wordList as $word) {
            $name .= ucfirst($word).' ';
       }
    }
    // address1 = Address 1
    // address2 = Address 2
    elseif (preg_match("/^(\w+)(\d)\$/",$name,$m)) {
        $name = ucfirst($m[1]).' '.$m[2];
    }
    // sometext = Sometext
    else {
       $name = ucfirst($name);
    }
    
    // strip a trailing _idxNum
    if (preg_match("/^(.+)_idx\s*Num\s*\$/",$name,$m)) {
       $name = $m[1];
    }           
    
    return trim($name);

}


/**
 * debug function used in array_walk (smObject.inc)
 *
 */
function _ddPrint($val, $key) {

    global $_dOp;

    if (is_array($val)) {
        $_dOp .= "$key<ul><br>\n";
        array_walk($val, '_ddPrint');
        $_dOp .= "</ul>";
        return;
    }
    if (is_bool($val))
        ($val) ? $val = 'true' : $val = 'false';    
    if (isset($val) && $val == '') {
        $val = '[no value]';
    }
    $_dOp .= "$key => $val<br>\n";

}

?>
