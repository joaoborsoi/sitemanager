<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

// current version. must match .XML files it reads
define ("SMCONFIG_VERSION", 1.2);
define ("SM_CONFIGVAR_REGEXP", '/{\$((\w+\.?)\[*\'*(\w*)\'*\]*)}/U');

/**
 * class representation of a SITE tag
 */
class SM_siteTag {

    /**
     * (string) our SITE NAME
     */
    var $siteID;

    /**
     * (multihash) hash of SECTIONS [SECTIONAME] = SM_sectionTag
     */
    var $sectionList;

    /**
     * create new CONFIG SITE
     * @param $siteID (string) the SITEID for this class
     */
    function __construct($siteID) {
        $this->siteID = $siteID;
    }

    /**
     * add or update a section
     * @param $sectionName (string) the SECTION name to add or update
     * @param $sectionID (string) the SECTION ID to update
     */
    function updateSection($sectionName,$sectionID) {
        if (!isset($this->sectionList[$sectionName]))
            $this->sectionList[$sectionName] = new SM_sectionTag($sectionName, $sectionID);
    }

    /**
     * add a variable to a section
     * @param $sectionName (string) SECTION to add this variable to
     * @param $sectionID   (string) which ID this section belongs to
     * @param $varKey      (string) the variables KEY (NAME)
     * @param $varVal      (mixed) the variables VALUE (VAL)
     */
    function addVar($sectionName, $sectionID, $varKey, $varVal) {
        if (isset($this->sectionList[$sectionName]))
            $this->sectionList[$sectionName]->addVar($sectionID, $varKey, $varVal);
    }

    /**
     * get a variable from a section. if sectionID is left blank, and there is
     * more than one ID, and the requested variable shows up in more than one ID,
     * all values will be returned as an array
     * @param $sectionName (string) the SECTION to retrieve the variable from
     * @param $varKey      (string) the KEY pointing to the VALUE to be retrieved
     * @param $sectionID   (string) [optional] which sectionID to retrieve this value from
     * @return (mixed) the requested variable
     */
    function getVar($sectionName, $varKey,  $sectionID='') {
        if (isset($this->sectionList[$sectionName]))
            return $this->sectionList[$sectionName]->getVar($sectionID, $varKey);
    }

    /**
     * return an entire section. if sectionID is left blank, and there are multiple ID's
     * for this section, all ID's will be combined into a singular SECTION
     * @param $sectionName  (string) the SECTION to retrieve
     * @param $sectionID    (string) [optional] which sectionID to retrieve this value from
     * @return (array) an array of all variabes in the requested SECTION
     */
    function getSection($sectionName,$sectionID='') {
        if (isset($this->sectionList[$sectionName]))
            return $this->sectionList[$sectionName]->getSection($sectionID);
    }

    /**
     * discover whether the specified section has multiple id's, or only default
     * @param $sectionName  (string) the SECTION to examine
     * @return (bool) true of the specified SECTION has multiple ID's
     */
    function hasMultipleID($sectionName) {
        if (isset($this->sectionList[$sectionName]))
            return $this->sectionList[$sectionName]->hasMultipleID();
    }

    /**
     * return the list of id's used by this SECTION
     * @param $sectionName (string) the SECTION whose id's we should retrieve
     * @return (array) a list of id's used by this SECTION
     */
    function getIDlist($sectionName) {
        if (isset($this->sectionList[$sectionName]))
            return $this->sectionList[$sectionName]->getIDlist();
    }

    /**
     * dump info for debug purposes
     */
    function dumpInfo() {
        foreach ($this->sectionList as $sectionName=>$sec) {
            $op .= $sec->dumpInfo();
        }
        return $op;
    }

}

/**
 * class representation of a SECTION tag
 */
class SM_sectionTag {

    /**
     * (string) our SECTION NAME
     */
    var $sectionName;

    /**
     * (multihash) variable representation of a VAR tag [SECTIONID][VARNAME] = DATA
     */
    var $varList;

    /**
     * new section tag
     * @param $sectionName (string) our new SECTION name
     */
    function __construct($sectionName) {
        $this->sectionName = $sectionName;
    }

    /**
     * add a variable to a section ID
     * @param $sectionID    (string) sectionID to add this variable to
     * @param $varKey       (string) KEY of the variable
     * @param $varVal       (mixed)  value of the vairable
     */
    function addVar($sectionID, $varKey, $varVal) {

        // if it's already there, make it or add it to an array of values
        if (isset($this->varList[$sectionID][$varKey])) {
            if (is_array($this->varList[$sectionID][$varKey]))
                // add to array already there
                $this->varList[$sectionID][$varKey][] = $varVal;
            else
                // create array
                $this->varList[$sectionID][$varKey] = array($this->varList[$sectionID][$varKey],$varVal);
        }
        else {
            // value wasn't there before, so it's a single value
            $this->varList[$sectionID][$varKey] = $varVal;
        }

    }

    /**
     * get a variable from a section. if sectionID is left blank, and there is
     * more than one ID, and the requested variable shows up in more than one ID,
     * all values will be returned as an array
     * @param $sectionID    (string) sectionID to retrieve this variable from
     * @param $varKey       (string) key used to retrieve the value requested
     * @return (mixed) the value of the requested variable
     */
    function getVar($sectionID, $varKey) {
        if (($sectionID == '')&&(isset($this->varList))&&(sizeof($this->varList)>1)) {
            // combine all sectionID's (pretend no sectionID's)
            $allVars = array();
            foreach ($this->varList as $secID=>$secVal) {
                foreach ($secVal as $vK=>$vV) {
                    $allVars[$vK] = $vV;
                }
            }
            return $allVars[$varKey];
        }
        else {
            if ($sectionID == '')
                $sectionID = 'default';
            if (isset($this->varList[$sectionID][$varKey]))
                return $this->varList[$sectionID][$varKey];
            else
                return NULL;
        }
    }

    /**
     * return an entire section. if sectionID is left blank, and there are multiple ID's
     * for this section, all ID's will be combined into a singular SECTION
     * @param $sectionID (string) the ID of the section to retrieve
     * @return (array) value of the section
     */
    function getSection($sectionID) {
        if (($sectionID == '')&&(sizeof($this->varList)>1)) {
            // combine all sectionID's (pretend no sectionID's)
            $allID = array();
            foreach ($this->varList as $secID=>$secVal) {
	      $allID = array_merge((array)$allID,
				   (array)$secVal);
            }
            return $allID;
        }
        else {
            if ($sectionID == '')
                $sectionID = 'default';
            if (isset($this->varList[$sectionID]))
                return $this->varList[$sectionID];
            else
                return NULL;
        }
    }

    /**
     * determine whether this section has multiple ID's
     * @return (int) number of id's this section holds
     */
    function hasMultipleID() {
        return sizeof($this->varList);
    }

    /**
     * return a list of id's used by this section
     * @return (array) list of id's
     */
    function getIDlist() {
        return array_keys($this->varList);
    }

    /**
     * dump info for debug purposes
     */
    function dumpInfo() {
        global $_dOp;
        $op = "SECTION: $this->sectionName<br>";
        $_dOp = '';
        foreach ($this->varList as $secID => $vars) {
            $op .= "  -- section ID: $secID<br>";
            $_dOp .= "<ul>";
            array_walk($vars, '_ddPrint');
            $_dOp .= "</ul><br>";
            $op .= $_dOp;
            $_dOp = '';
        }
        return $op;
    }

}

/**
 * SM_config is a class that reads in a siteManager XML configuration file
 * the settings in that file may then be accessed through the getVar() method
 *
 */
class SM_config extends SM_object {

    /** (multihash) a hash of config vars, per SITEID */
    var $_siteTagList;

    /** (string) current site in config file (used only for parsing) */
    var $_site;

    /** (string) current section in config file (used only for parsing) */
    var $_section;

    /** (string) current section ID in config file (used only for parsing) */
    var $_sectionID;

    /** (string) current file we're parsing */
    var $_cFile;

    /** (bool) to determine whether GLOBAL has been loaded already */
    var $_globalLoaded;

    /**
     * constructor accepts config file to read, parses it.
     * @param $file (string) the config file to read
     */
    function __construct($file) {

        $this->_globalLoaded = false;

        // parse the file
        $this->parse($file);

        // if we did NOT load a GLOBAL config file, setup a blank one here
        if (!$this->_globalLoaded) {
            $this->_siteTagList['GLOBAL'] = new SM_siteTag('GLOBAL');
        }

    }


    /**
     * get a variable from this config class
     * TODO optimize this (weyrick)
     * @param $section (string) the section to use for lookup
     * @param $name    (string) the "key" from the key/value pair you're looking for
     * @param $mergeGlobal  (bool) [optional] when true, GLOBAL and local siteID values will be merged,
     *                                          and the results returned as an array. otherwise, local
     *                                          siteID would be used by default, only going to GLOBAL if
     *                                          a local value wasn't set
     * @param $sectionID (string) [optional] the sectionID to examine
     * @return the value for the variable requested
     */
    function getVar($section, $name, $mergeGlobal=false, $sectionID='') {

        global $SM_siteID;

        $gVar = $this->_siteTagList['GLOBAL']->getVar($section,$name,$sectionID);

        // if for some reason a local site hasn't been loaded, just return global
        if (!isset($this->_siteTagList[$SM_siteID])) {
            return $gVar;
        }

        $lVar = $this->_siteTagList[$SM_siteID]->getVar($section,$name,$sectionID);

        if ($mergeGlobal && isset($lVar)) {

            // we should mergeglobal, AND there's already a value in
            // siteID -- so merge

            // local must come before global in the array

            // four possibilities:
            // 1) global is single, local is single
            // 2) global is an array, local is array
            // 3) global is an array, local is single
            // 4) global is single, local is an array

            // 1
            if (!is_array($gVar) && !is_array($lVar)) {

                // both single, so merge to array
                $endVar = array($lVar, $gVar);

            }
            // 2
            elseif (is_array($gVar) && is_array($lVar)) {

                // both arrays, so merge arrays
	      $endVar = array_merge((array)$lVar,
				    (array)$gVar);

            }
            // 3
            elseif (is_array($gVar) && !is_array($lVar)) {

                // GLOBAL is array, local is not
                $endVar = $gVar;
                $endVar[] = $lVar;
                $endVar = array_reverse($endVar);   // reverse so local overrides global

            }
            // 4
            elseif (!is_array($gVar) && is_array($lVar)) {

                // GLOBAL is single, local is array
                $lVar[] = $gVar;
                $endVar = $lVar;

            }

            // whew!
            return $endVar;

        }
        elseif ($mergeGlobal && !isset($lVar)) {

            // they want mergeglobal, but local wasn't set, just return global
            return $gVar;

        }
        elseif (!$mergeGlobal && isset($lVar)) {

            // they only want local, and it's set so return it
            return $lVar;

        }
        elseif (!$mergeGlobal && !isset($lVar)) {

            // they only want local, but it's not set, return global instead
            return $gVar;

        }
        else {

            // shouldn't get here!
            SM_fatalErrorPage("smConfig->getVar(): shouldn't reach this!",$this);

        }

    }

    /**
     * return an entire section at a time
     * it will combine GLOBAL with the current SITEID, with SITEID takeing precedence over GLOBAL
     * @param $section (string) the section to return
     * @param $sectionID (string) [optional] the section ID to examine
     * @return (hash) a hash of all values in the section requested
     */
    function getSection($section, $sectionID='') {

        global $SM_siteID;

        $gSection = $this->_siteTagList['GLOBAL']->getSection($section, $sectionID);


        if (!empty($SM_siteID)) {
            $sSection = $this->_siteTagList[$SM_siteID]->getSection($section, $sectionID);
            $section = array_merge((array)$gSection, (array)$sSection);
        }
        else {
            $section = $gSection;
        }

        return $section;

    }

    /**
     * discover whether a particular section has mutltiple ID's
     * @param $section (string) the SECTION to examine
     * @return (bool) true if this SECTION has mutliple ID's in either GLOBAL or LOCAL
     */
    function hasMultipleID($section) {
        global $SM_siteID;
        if ($this->_siteTagList['GLOBAL']->hasMultipleID($section) ||
            $this->_siteTagList[$SM_siteID]->hasMultipleID($section))
            return true;
        else
            return false;
    }

    /**
     * retrieve the list of ID's a SECTION contains
     * @param $section      (string) the SECTION to examine
     * @return (array) a list of id's
     */
    function getIDlist($section) {
        global $SM_siteID;
        $lList = $this->_siteTagList[$SM_siteID]->getIDlist($section);
        $gList = $this->_siteTagList['GLOBAL']->getIDlist($section);
        $idList = array_merge((array)$gList, (array)$lList);
        return array_unique($idList);
    }

    /**
     * set a variable, based on section, name and siteID
     * @param $section (string) the section to use for lookup
     * @param $name    (string) the "key" from the key/value pair you want to write
     * @param $siteID  (string) [optional] the siteID to use. current SITEID by default
     * @param $sectionID (string) [optional] the section ID to update
     * @return nothing
     */
    function setVar($section, $name, $newVal, $siteID='SITEID', $sectionID='default') {

        global $SM_siteID;

        if ($siteID == 'SITEID')
            $siteID = $SM_siteID;

        // security, can't set global
        if ($siteID == 'GLOBAL')
            return;

       $this->_siteTagList[$siteID]->addVar($section, $sectionID, $name, $newVal);

    }


    /**
     * load SITEID specific variables
     * @param $file (string) the local configuration file to read
     */
    function loadSite($file) {

        global $SM_siteID;

        // check cache?
        $willCache = false;
        if ($this->_siteTagList['GLOBAL']->getVar('cache','cacheSMConfig','default')) {

            $willCache = true;
            if ($this->loadSerial($file, 'SMCONFIG', $this->_siteTagList[$SM_siteID]))
                return;

        }

        // fall through: parse
        $this->parse($file);

        // should we cache the config file?
        if ($willCache) {
            $this->saveSerial($file, 'SMCONFIG', $this->_siteTagList[$SM_siteID]);
        }

    }

    /**
     * function triggered when an XML start element appears
     * here we will parse tag's we recognize from the config file
     * NOTE: this function is private and for use by the XML parser only
     * @param $parser (reference) the XML parser
     * @param $name (string) the tag found
     * @param $attrs (hash) the attributes this tag carried
     */
    function startElement($parser, $name, $attrs) {

        global $SM_siteID;

        switch ($name) {
        case 'SMCONFIG':
            // check version
            if ($attrs['VERSION'] > SMCONFIG_VERSION)
                SM_fatalErrorPage("version incompatibility with config file $this->file (I'm version ".SMCONFIG_VERSION.", it's version {$attrs['VERSION']})",$this);
            break;
        case 'SITE':
            // we're excluding to a specific site
            if (isset($attrs['SITEID']))
                $this->_site = $attrs['SITEID'];
            elseif (isset($attrs['NAME']))
                $this->_site = $attrs['NAME'];   // old

            // if it's set, use it
            if (isset($this->_site)) {

                if (($this->_site != 'GLOBAL')&&($this->_site != $SM_siteID)) {
                    SM_debugLog("warning: SITEID specified in local config file ($this->_cFile - site SITEID='$this->_site') does not match the one configured earlier in SM_siteID ($SM_siteID)");
                    SM_debugLog("values loaded from this file will not be visible for the current site, unless the SITEID is changed in the config file");
                    SM_debugLog("if you're viewing this message in an error window, you probably want to change $this->_cFile and set SITE SITEID='$SM_siteID'");
                    SM_debugLog("instead of SITE NAME='$this->_site'");
                }
                // if global was already loaded, and this siteID is GLOBAL, fail
                if (($this->_site == 'GLOBAL')&&($this->_globalLoaded)) {
                    SM_fatalErrorPage("GLOBAL config was already loaded, won't load twice (from $this->_cFile) - check SITEID",$this);
                }

            }
            else {
                // otherwise, use current siteID
                $this->_site = $SM_siteID;
            }

            // create the new SITE tag, if it's not already created
            if (empty($this->_siteTagList[$this->_site])) {
                $this->_siteTagList[$this->_site] = new SM_siteTag($this->_site);
                if ($this->_site == 'GLOBAL')
                    $this->_globalLoaded = true;
            }

            break;
        case 'SECTION':
            // we're in a specific section
            $this->_section = $attrs['NAME'];
            if (!empty($attrs['ID']))
                $sectionID = $attrs['ID'];
            else
                $sectionID = 'default';
            $this->_sectionID = $sectionID;
            $this->_siteTagList[$this->_site]->updateSection($attrs['NAME'], $sectionID);
            break;
        case 'VAR':
            // we're getting vars for a section
            if (!isset($this->_section)||!isset($this->_site)) {
                SM_fatalErrorPage("malformed config file: not in section or site for VAR tag",$this);
            }

            // do bools correctly
            if (strtoupper($attrs['VALUE']) == 'TRUE')
                $val = true;
            elseif (strtoupper($attrs['VALUE']) == 'FALSE')
                $val = false;
            else
                $val = $attrs['VALUE'];

            // if there's a variable in the value, do a replacement
            if (preg_match_all(SM_CONFIGVAR_REGEXP,$val,$varMatch)) {
                foreach ($varMatch[1] as $key => $vm) {

                    $pReg = preg_quote($varMatch[0][$key]);

                    // if there is a dot, we might use another section's var
                    if (strstr($vm,'.'))
                        list($vm, $tmpSection) = array_reverse(explode('.', $vm));
                    else
                        $tmpSection = $this->_section;

                    // first, try to sub a value from this file,section
                    $sVal = $this->_siteTagList[$this->_site]->getVar($tmpSection,$vm);

                    if ($sVal == '') {

                        // if it's blank, try to use the global version

                        // if $varMatch[0][0] contains a [, assume it's a hash
                        if (strstr($varMatch[0][$key],'[')) {
                            $gHash = $varMatch[2][$key];
                            $gKey = $varMatch[3][$key];
                            global $$gHash;
                            $sgHash = $$gHash;
                            $sVal = $sgHash[$gKey];
                        }
                        else {
                            // regular global
                            global $$vm;
                            $sVal = $$vm;
                        }

                    }

                    $val = preg_replace("/$pReg/U",$sVal,$val);
                }
            }

            $this->_siteTagList[$this->_site]->addVar($this->_section,$this->_sectionID,$attrs['NAME'],$val);

            break;
        case 'SM_XML':
            // root tag
            break;
        default:
            SM_fatalErrorPage("unknown tag in SMCONFIG: \"$name\"",$this);
            break;
        }

    }

    function endElement($parser, $name) {
        // nothing - for compatibility with XML parser only
    }



    /**
     * dump config values for this SITE and GLOBAL
     *
     */
    function dumpInfo() {

        global $SM_siteID;

        $_dOp = '';
        $_dOp .= get_class($this)." :: configuration information:<br>\n";

        $_dOp .= "<br>- GLOBAL VALUES -<br>\n";
        $_dOp .= $this->_siteTagList['GLOBAL']->dumpInfo();
        $_dOp .= '<hr>';
        $_dOp .= "<br>- LOCAL SITE VALUES -<br>\n";
        $_dOp .= $this->_siteTagList[$SM_siteID]->dumpInfo();

        return $_dOp;

    }

    /**
     * private function for parsing the XML file. actually creates XML parser
     * and sets up element handlers
     * @param $file (string) the file to parse
     */
    function parse($file) {

        global $SM_develState;

        $this->_cFile = $file;

        if ($SM_develState)
            SM_debugLog("parse: parsing XML configuration file: $file",$this);

        if (empty($file))
            SM_fatalErrorPage("XML file to parse was not set",$this);

        $xml_parser = xml_parser_create();
        xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING,true);
        xml_set_object($xml_parser, $this);
        xml_set_element_handler($xml_parser, "startElement", "endElement");
        if (!($fp = fopen($file, "r", 1))) {
            SM_fatalErrorPage("could not open XML input",$this);
        }

        while ($data = fread($fp, 4096)) {
            if (!xml_parse($xml_parser, $data, feof($fp))) {
                SM_fatalErrorPage(sprintf("XML error: %s at line %d of $file",
                            xml_error_string(xml_get_error_code($xml_parser)),
                            xml_get_current_line_number($xml_parser)),$this);
            }
        }
        xml_parser_free($xml_parser);

    }

}

?>
