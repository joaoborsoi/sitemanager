<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/


/**
 *  a class to handle member logins
 *  works with session code
 *
 */
class SM_memberSystem extends SM_object {

    /**
     * (bool) if the current user is a guest, this is true
     */
    var $isGuest;

    /**
     * (bool) if the current user is a logged in member, this is true
     */
    var $isMember;

    /**
     * (string) the session ID we're working with
     */
    var $sessionID;

    /**
     * (hash) member info, from member table
     */
    var $memberData;

    /** (string) ID of the database connection we're using */
    var $dbID;

    /**
     * setup the member system
     *
     */
    function __construct(&$dbH, $sessionID, &$dbHL) {

        global $SM_siteManager;

        // database reference
        $this->dbH      =& $dbH;
        $this->dbHL     =& $dbHL;

        // current session ID
        $this->sessionID = $sessionID;

        // retrieve session configuration
        $this->directive            = $SM_siteManager->siteConfig->getSection('members');
        if ($this->directive['dataBaseID'] == '')
            $this->directive['dataBaseID'] = 'default';
        $this->dbID = $this->directive['dataBaseID'];
        $this->directive['dbType']  = $SM_siteManager->siteConfig->getVar('db','dbType',false,$this->dbID);

        if (!isset($this->dbHL[$this->dbID])) {
            SM_fatalErrorPage("database connection ID '$this->dbID' does not exist. check dataBaseID in your config.",$this);
        }

        // by default, member is NOT logged in
        $this->isGuest  = true;
        $this->isMember = false;

    }

    /**
     * attempt to create a member session with the username/password combo provided
     * @param $userName (string) the username to use for authentication
     * @param $passWord (string) the password to use for authentication
     * @return (bool) true if the user authenticated succesfully, false otherwise
     */
    function _attemptLogin($userName, $passWord) {

        // define some configured variables we'll be using
        $memberTable            = $this->directive['memberTable'];
        $dbType                 = $this->directive['dbType'];
        $uIDField               = $this->directive['uIDField'];
        $userNameField          = $this->directive['userNameField'];
        $passWordField          = $this->directive['passWordField'];

        switch ($dbType) {
        case 'pgsql':
            $SQL = "SELECT
                        *
                    FROM
                        \"{$memberTable}\"
                    WHERE
                        \"{$userNameField}\" = '$userName' AND
                        \"{$passWordField}\" = '$passWord'
                   ";
            break;
        case 'mysql':
        case 'oracle':
            $SQL = "SELECT
                        *
                    FROM
                        {$memberTable}
                    WHERE
                        {$userNameField} = '$userName' AND
                        {$passWordField} = '$passWord'
                   ";
            break;
        default:
            SM_fatalErrorPage("you are attemping to use member sessions with an unsupported database type: ($dbType)");
            break;
        }

        // issue query
        $rr = $this->dbHL[$this->dbID]->GetRow($SQL);

        // if $rr has data, it's a good session
        if ($rr) {

            // create a member session
            $this->_createMemberSessionEntry($rr[$uIDField]);

            // finally, indicate they are now a member
            $this->isGuest      = false;
            $this->isMember     = true;
            $this->memberData   = $rr;
            $this->memberData['TTL']    = time();

            // if they are using persistent members, update var
            if ($this->directive['usePersistentMembers']) {
                global $SM_siteManager;
                $SM_siteManager->sessionH->setPersistent(SM_MEMBER_PVAR, $this->memberData);
            }

            return true;

        }
        else {
            // invalid login
            return false;
        }


    }


    /**
     * attempt to create remove a valid member session, based on current sessionID
     * @return nothing
     */
    function _attemptLogout() {

        // define some configured variables we'll be using
        $memberSessionsTable    = $this->directive['memberSessionsTable'];
        $dbType                 = $this->directive['dbType'];
        $sIDField               = $this->directive['sIDField'];
        $userNameField          = $this->directive['userNameField'];
        $passWordField          = $this->directive['passWordField'];

        switch ($dbType) {
        case 'pgsql':
            $SQL = "DELETE
                    FROM
                        \"{$memberSessionsTable}\"
                    WHERE
                        \"{$sIDField}\" = '$this->sessionID'
                   ";
            break;
        case 'oracle':
        case 'mysql':
            $SQL = "DELETE
                    FROM
                        {$memberSessionsTable}
                    WHERE
                        {$sIDField} = '$this->sessionID'
                   ";
            break;
        default:
            SM_fatalErrorPage("you are attemping to use member sessions with an unsupported database type: ($dbType)");
            break;
        }

        // issue query
        $rh = $this->dbHL[$this->dbID]->ExecSQL($SQL);

        // member session removed
        $this->isGuest  = true;
        $this->isMember = false;
        unset($this->memberData);

        // if using persistent member data, unset that persistent
        if ($this->directive['usePersistentMembers']) {
            global $SM_siteManager;
            $SM_siteManager->sessionH->setPersistent(SM_MEMBER_PVAR);
        }

    }

    /**
     * a member has authenticated, create a member session for them
     * @param uID (string) the unique global user ID for the user to login
     */
    function _createMemberSessionEntry($uID) {

        // define some configured variables we'll be using
        $memberSessionsTable    = $this->directive['memberSessionsTable'];
        $dbType                 = $this->directive['dbType'];
        $uIDField               = $this->directive['uIDField'];
        $sIDField               = $this->directive['sIDField'];
        $dcField                = $this->directive['dcField'];
	$simultaneousAccess     = $this->directive['simultaneousAccess'];

        // QUERY: REMOVE ANY CURRENT SESSIONS FOR THIS MEMBER
	if(!$simultaneousAccess)
	{
	  switch ($dbType) {
	  case 'pgsql':
            $SQL = "DELETE FROM
                        \"{$memberSessionsTable}\"
                    WHERE
                        \"{$uIDField}\" = '$uID'
                   ";
            break;
	  case 'oracle':
	  case 'mysql':
            $SQL = "DELETE FROM
                        {$memberSessionsTable}
                    WHERE
                        {$uIDField} = '$uID'
                   ";
            break;
	  default:
            SM_fatalErrorPage("you are attemping to use member sessions with an unsupported database type: ($dbType)");
            break;
	  }

	  // issue query
	  $rh = $this->dbHL[$this->dbID]->ExecSQL($SQL);
	}


        // QUERY: ADD SESSION ENTRY
        switch ($dbType) {
        case 'pgsql':
            $SQL = "INSERT INTO
                        \"{$memberSessionsTable}\"
                        (
                            \"{$uIDField}\",
                            \"{$sIDField}\",
                            \"{$dcField}\"
                        )
                    VALUES (
                        '$uID',
                        '$this->sessionID',
                        timestamp('now')
                    )
                   ";
            break;
        case 'mysql':
        case 'oracle':
            $SQL = "INSERT INTO
                        {$memberSessionsTable}
                    ({$uIDField},{$sIDField},{$dcField}) VALUES
                    ('$uID','$this->sessionID',NOW())
                   ";
            break;
        }

        // issue query
        $rh = $this->dbHL[$this->dbID]->ExecSQL($SQL);
    }


    /**
     * will clean out old sessions (past their TTL) if this option is turned on
     * (maintainTables var in 'members' SECTION of GCS)
     * this really shouldn't be done by SiteManager, as it adds an extra db query
     * for every page view. it should be done externally by a cron script.
     * but, for convenience ...
     */
    function _maintainTables() {

        if (!$this->directive['maintainTables'])
            return;

        // define some configured variables we'll be using
        $memberSessionsTable    = $this->directive['memberSessionsTable'];
        $dbType                 = $this->directive['dbType'];
        $dcField                = $this->directive['dcField'];
        $loginTTL               = $this->directive['loginTTL'];

        // QUERY: REMOVE MEMBERSESSIONS PAST THEIR TTL
        switch ($dbType) {
        case 'pgsql':
            $SQL = "DELETE FROM
                        \"{$memberSessionsTable}\"
                    WHERE
                        date_part('epoch',timestamp('now')) - date_part('epoch',\"{$dcField}\") >= {$loginTTL}
                   ";
            break;
        case 'mysql':
        case 'oracle':
            $SQL = "DELETE FROM
                        {$memberSessionsTable}
                    WHERE
                        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP({$dcField}) >= {$loginTTL}
                   ";
            break;
        default:
            SM_fatalErrorPage("you are attemping to use member sessions with an unsupported database type: ($dbType)");
            break;
        }

        // issue query
        $rh = $this->dbHL[$this->dbID]->ExecSQL($SQL);
    }

    /**
     * load a members information from the database, if the current
     * session ID is a member session (otherwise, user is a guest)
     *
     * favor a persistent member data load rather than a database query,
     * if that feature is turned on
     *
     */
    function _attemptLoadMember() {

        // if using persistent members, try that first
        if ($this->directive['usePersistentMembers']) {
            $mpv = SM_MEMBER_PVAR;
            global $$mpv;
            $pmData = $$mpv;
            if (is_array($pmData) && (isset($pmData['TTL']))) {

                // appears to be a valid member data structure, check TTL
                if ($this->directive['loginTTL'] > 0) {

                    $now = time();
                    if ($now - $pmData['TTL'] > $this->directive['loginTTL']) {
                        // expire data. clear the persistent
                        global $SM_siteManager;
                        $SM_siteManager->sessionH->setPersistent(SM_MEMBER_PVAR);
                        SM_debugLog("warning: valid member login persistent session found, but it was expireed, not logging in. ($now - ".$pmData['TTL']." < {$this->directive['loginTTL']})",$this);
                        return;
                    }
                }

                // good persistent member
                $this->memberData = $pmData;
                $this->isGuest    = false;
                $this->isMember   = true;

                $this->debugLog("member data loaded from a persistent variable");

                return;
            }

        }

        // define some configured variables we'll be using
        $memberTable            = $this->directive['memberTable'];
        $memberSessionsTable    = $this->directive['memberSessionsTable'];
        $dbType                 = $this->directive['dbType'];
        $loginTTL               = $this->directive['loginTTL'];
        $uIDField               = $this->directive['uIDField'];
        $sIDField               = $this->directive['sIDField'];
        $dcField                = $this->directive['dcField'];

        switch ($dbType) {
        case 'pgsql':
            $SQL = "SELECT
                        \"{$memberTable}\".*, date_part('epoch',\"{$memberSessionsTable}\".\"{$dcField}\") AS \"TTL\"
                    FROM
                        \"{$memberTable}\",\"{$memberSessionsTable}\"
                    WHERE
                        \"{$sIDField}\"='$this->sessionID' AND
                        \"{$memberTable}\".\"{$uIDField}\" = \"{$memberSessionsTable}\".\"{$uIDField}\"";
            break;
        case 'mysql':
        case 'oracle':
            $SQL = "SELECT
                        {$memberTable}.*, UNIX_TIMESTAMP({$memberSessionsTable}.{$dcField}) AS TTL
                    FROM
                        {$memberTable},{$memberSessionsTable}
                    WHERE
                        {$sIDField}='$this->sessionID' AND
                        {$memberTable}.{$uIDField} = {$memberSessionsTable}.{$uIDField}";
            break;
        default:
            SM_fatalErrorPage("you are attemping to use member sessions with an unsupported database type: ($dbType)");
            break;
        }

        // issue query
        $rr = $this->dbHL[$this->dbID]->GetRow($SQL);
        if ($rr) {

            // good, check TTL
            if ($this->directive['loginTTL'] > 0) {

                $now = time();
                if ($now - $rr['TTL'] > $this->directive['loginTTL']) {
                    // expired. leave the entry there, if they have maintainTables turned on
                    // it will be removed by that function
                    SM_debugLog("warning: valid member login session found, but it was expireed, not logging in. ($now - {$rr['TTL']} < {$this->directive['loginTTL']})",$this);
                    return;
                }
            }

            // found a member session
            $this->memberData = $rr;

            // they are now a member
            $this->isMember = true;
            $this->isGuest  = false;

            $this->debugLog("member information loaded from database");

        }

    }


    /**
     * dump debug session information
     *
     */
    function dumpInfo() {

        $op = "current user is a: ";
        ($this->isMember) ? $op .= "MEMBER" : $op .= "GUEST";

        $op .= "<br><br>";

        if ($this->isMember) {
            global $_dOp;
            $_dOp = '';
            $op .= "member info:<br>";
            array_walk($this->memberData, '_ddPrint');
            $op .= $_dOp;
        }

        $op .= $this->dumpDirectives();
        $op .= $this->getClassLog();

        return $op;

    }

}

?>