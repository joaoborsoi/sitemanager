<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file
* LICENSE, and is available through the world wide web at
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

/**
 * filter base class
 * all filters should extend this class
 */
class SM_entityFilter extends SM_object {

    /**  (string) the data we'll be looking at */
    var $varName;

    /** (string) if not, what should we tell the user? */
    var $badMessage;

    /** (mixed) the current contents of $varName */
    var $data;

    /** (bool) does this filter REQUIRE javascript to work? */
    var $requiresJS;

    /**
     * (string) javascript output. this is the stuff that needs to
     *          go at the top, not the inline stuff
     */
    var $jsOutput;

    /** (bool) use javascript or not */
    var $useJS;

    /** (obj ref) my parent input entity */
    var $parentInputEntity;

    /**
     * constructor setups up class. this should NOT be overriden!
     * use filterConfig() for class initialization.
     * @param $dbH (obj ref)       - SiteManager default database handler
     * @param $sessionH (obj ref)  - SiteManager session handler
     * @param $dbHL (hash)         - SiteManager database handler hash
     * @param $pE (obj ref)        - parent input entity of this filter
     */
    function __construct(&$dbH, &$sessionH, &$dbHL, &$pE) {

        SM_sfLog($this, " [filter class created: ".get_class($this)."]");
        unset($args);

        // setup session handlers
        $this->dbH                  =& $dbH;
        $this->dbHL                 =& $dbHL;
        $this->sessionH             =& $sessionH;
        $this->parentInputEntity    =& $pE;

        // javascript default settings
        $this->requireJS    = false;
        $this->jsOutput     = '';

    }

    /**
     * set info on this filter
     * @param $vN   (string)    - variable name that contains data we'll be looking at
     * @param $msg  (string)    - string to display if filter fails
     * @param $uJS  (bool)      - use JavaScript in this filter or not
     */
    function setInfo($vN, $msg='', $uJS) {

        global $HTTP_POST_VARS;

        // use javascript?
        $this->useJS = $uJS;

        // if we're using javascript, load it up
        if ($uJS)
            $this->loadJS();

        // if the filter fails, this message will be displayed to the user
        if ($msg != '')
            $this->badMessage = $msg;
        else
            $this->badMessage = "This field appears to be invalid";  /* default */

        // the data the filter will operate on
        $this->varName = $vN;

        // set data based on $varName
        if (isset($HTTP_POST_VARS[$this->varName]))
            $this->data = $HTTP_POST_VARS[$this->varName];
        else
            $this->data = '';

        // run filterConfig method if it exists
        if (method_exists($this, 'filterConfig')) {
            $this->filterConfig();
        }

        SM_sfLog($this,"setInfo('$this->varName', '$this->badMessage')");

    }

    /**
     * apply this filter. if it fails, return the false
     *
     * this should be overridden by filters when they subclass this class
     * @return (bool) whether filter passed or not
     */
    function filterThink() {
        return true;
    }

    /**
     * return javscript code we might have
     * check global list to make sure we haven't already been added to this script's js code
     * @return (string) JavaScript output, if any
     */
    function getJS() {

        global $filterDefList;

        $cName = get_class($this);

        if (isset($filterDefList[$cName]))
            return;
        else {
            $filterDefList[$cName] = true;
            return $this->jsOutput;
        }

    }

    /**
     * return our prefix
     * @return (string) our variable prefix
     */
    function getMyPrefix() {

        // rip our prefix
        if (preg_match("/^.+\_(SM[0-9]+)$/",$this->varName,$m)) {
            $prefix = $m[1];
        }
        else {
            $prefix = '';
        }

        return $prefix;

    }

    /**
     * attempt load javascript code for this filter. do this my trying to load
     * our filter name +.js in the php path. use it as javscript output
     * FIXME: security hazards??
     */
    function loadJS() {

        global $SM_siteManager;

        $fName = get_class($this).'.js';
        $fName = $SM_siteManager->siteConfig->getVar('dirs','sfFilters').'/'.str_replace("filter","Filter",$fName); // capitlize Filter, as the class won't for us

        SM_sfLog($this, "loading javascript code from $fName");

        // load javascript, check include path.
        // ignore errors
        if (file_exists($fName))
            $jsO = file($fName,1);

        if ((isset($jsO))&&(is_array($jsO))) {
            $this->jsOutput = implode('',$jsO);
            SM_sfLog($this, "javascript load succeded");
        }
        else {
            //SM_sfLog($this, "javascript load failed! (ignore if there is no javascript for this filter)",true);
        }

    }

}



/**
 * load (include) a filter module
 * if it's already loaded, ignore request
 * @param $fName    (string) - the input entity to load
 * @return (bool)   if false, couldn't find it. true means successful load
 */
function SM_sfLoadFilter($fName) {

    // if already here, don't reload
    if (class_exists($fName.'Filter'))
        return true;

    // globalize
    global $SM_siteManager;

    // find file
    $fName = $SM_siteManager->findSMfile($fName.'Filter', 'sfFilters', 'inc', true);

    // load it up
    // include_once will not let duplicate modules be loaded
    include_once($fName);

    // all good and loaded, return
    return true;

}

?>
