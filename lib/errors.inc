<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

// override the PHP error handler
set_error_handler("SM_errorHandler");

/**
 * submit a message to the debug log. it will only show up when viewing
 * debug output
 *
 * @param $msg (string) message to log
 * @param $obj (object) [optional] the caller object. specify to display which object called
 *                                 the debug function
 * @return (nothing)
 */
function SM_debugLog($msg, $obj=NULL) {

    global $SM_debugOutput;

    if (is_object($obj))
        $msg = get_class($obj).":: ".$msg;
    else
        $msg = "$msg";

    $SM_debugOutput[] = $msg;

}

/** 
 *  a fatal error has occured. last ditch effort to output an error message.
 *
 *  @param $msg (string) error message to display 
 *  @param $obj (object) [optional] the caller object. specify to display which object called the error routine
 *  @return (nothing) this routine never returns
 */
function SM_fatalErrorPage($msg='', $obj=NULL) {
    
    global $SM_outputBuffer;

    SM_debugLog("<font color=\"red\">$msg</font>",$obj);

    $SM_outputBuffer = ob_get_contents();
    ob_end_clean();

    echo _makeErrorPage();
    exit;
    
}

/**
 * create a javascript function that will open a new window and display
 * the debug log
 * @return (string) the javascript output
 */
function _makeJSdebug() {

        global $SM_debugOutput, $SM_siteManager;

        $dPage = _makeErrorPage();

        $dPage = addslashes($dPage);

        // if debug output window is on, send the debug output to the root template
        $output = "
                    <SCRIPT LANGUAGE=\"JavaScript\">
                    function SM_viewDebugLog() {

                        debugWindow = window.open('', 'debugWindow','scrollbars=1,resizeable=1,menubar=no,status=no,toolbar=no,height=480,width=640');
                        debugWindow.document.write('$dPage');
                        debugWindow.document.close();";

        if ($SM_siteManager->siteConfig->getVar('debug','jsDebugAutoFocus'))
            $output .= "                        debugWindow.focus();\n";

        $output .="
                    }
                    </SCRIPT>
            ";

        return $output;

}

/**
 * create the HTML for the error page
 * doesn't echo anything, it returns it's output
 *
 * @return (string) complete HTML page output
 */
function _makeErrorPage() {

    global $SM_siteName, $SM_siteID, $PHP_SELF, $SM_debugOutput, $SM_outputBuffer, $SM_noticeLog, $SM_siteManager;
    
    if ($SM_siteName == '')
        $SM_siteName = 'UNKNOWN SITENAME';
    
    if ($SM_siteID == '')
        $SM_siteID = 'UNKNOWN SITEID';
    
    $output = "<html><body bgcolor=ffffff><br><b><font color=\"red\">";
    $output .= "$SM_siteName ($SM_siteID) :: $PHP_SELF<br></font><hr>";

    if (!empty($SM_noticeLog)) {

        if (isset($SM_siteManager) && ($SM_siteManager->siteConfig->getVar('debug','showNotices'))) {        

            $output .= "<b>NOTICE LOG:</b><br>\n";
            foreach ($SM_noticeLog as $line) {
                $output .= "$line<br>\n";
            }

        }
    }

    if (!empty($SM_debugOutput)) {
        foreach ($SM_debugOutput as $line) {
            $output .= "$line<br>\n";
        }
    }

    if (!empty($SM_outputBuffer)) {
        $output .= "Output Buffer:<br><pre>".$SM_outputBuffer.'</pre>';
    }

    $output .= "<hr><b>Roadsend SiteManager v".SM_VERSION." (<a href=\"http://www.roadsend.com/siteManager\">http://www.roadsend.com/siteManager</a>) (PHP v".PHP_VERSION.'/'.PHP_OS.')';
    $output .= "</b></br></body></html>";

    return $output;

}

/**
 * this debug function will dump all variables that were
 * passed to the script, including GET, POST and cookies
 * will traverse arrays
 * @return (nothing)
 */
function SM_dumpVars() {

    global $HTTP_POST_VARS, $HTTP_GET_VARS;

    if ((is_array($HTTP_GET_VARS))&&(count($HTTP_GET_VARS)))
      $vars = $HTTP_GET_VARS;
    else
      $vars = $HTTP_POST_VARS;

    if (!is_array($vars))
        return;
    
    while (list($key, $val) = each ($vars)) {
        if (!is_array($val))
            echo "$key => $val<br>";
        else {
            echo "$key (array):<br>";
            foreach($val as $k => $v) {
                echo " ..... $k -> $v<br>";
            }
        }
    }

}   


/**
 *  override PHP's main error handler function
 *  this should NOT be called directly
 */
function SM_errorHandler($errno, $errstr, $errfile, $errline) {

    global $SM_noticeLog;

    switch ($errno) {
    case E_PARSE:
    case E_USER_ERROR:
    case E_COMPILE_ERROR:
    case E_CORE_ERROR:
    case E_ERROR:
        $msg = "<b>FATAL</b> [$errno] $errstr<br>\n";
        $msg .= "  Fatal error in line $errline of file ".$errfile;
        $msg .= ", PHP ".PHP_VERSION." (".PHP_OS.")<br>\n";
        SM_fatalErrorPage($msg);
        break;
    case E_USER_WARNING:
    case E_COMPILE_WARNING:
    case E_CORE_WARNING:
    case E_WARNING:
    case E_STRICT:
        $msg = "<b>PHP WARNING:</b> $errstr on line $errline in $errfile<br>\n";
        SM_debugLog($msg);
        $SM_debug=1;
        break;
    case E_NOTICE:
    case E_USER_NOTICE:
        $SM_noticeLog[] = '<b>PHP NOTICE:</b> '.$errstr." on line $errline in $errfile\n";
        break;
    default:
        SM_fatalErrorPage("Unkown error type: [$errno] $errstr on line $errline in $errfile<br>\n");
        break;
    }

}


?>
