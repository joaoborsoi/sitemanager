<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/

// -----------------------------------------------------------------------------

// if SiteManager is being used on a system where you
// are unable to place the main SiteManager root directory
// into the php.ini include_path, set the following variable
// to the main root siteManager directory
// for example:

// $SM_rootDir = '/var/www/user/siteManager/';

$SM_rootDir = '';   // when set, make sure it ends with a slash!

// -----------------------------------------------------------------------------
// don't touch below here
// -----------------------------------------------------------------------------

// Version
$SM_versionTag = "2.2.0 production";
define('SM_VERSION', '2.2.0');

// turn on all warnings and notices
error_reporting(E_ALL);

// catch all output
ob_start();

// script start time
$microtime = split(' ', microtime());
$SM_scriptStartTime = $microtime[1].substr( $microtime[0], 1);

require($SM_rootDir."lib/errors.inc");
require($SM_rootDir."lib/smObject.inc");
require($SM_rootDir."lib/smConfig.inc");
require($SM_rootDir."lib/smRoot.inc");
require($SM_rootDir."lib/dataManip.inc");
require($SM_rootDir."lib/support.inc");
require($SM_rootDir."lib/security.inc");
require($SM_rootDir."lib/auth.inc");
require($SM_rootDir."lib/smartForm.inc");
require($SM_rootDir."lib/layoutTemplate.inc");
require($SM_rootDir."lib/modules.inc");
require($SM_rootDir."lib/smMembers.inc");
require($SM_rootDir."lib/sessions.inc");
require($SM_rootDir."lib/codePlate.inc");

// ROOT SITEMANAGER CLASS
$SM_siteManager = new SM_siteManagerRoot();

if ($SM_develState)
    SM_debugLog("Initializing SiteManager...");

?>