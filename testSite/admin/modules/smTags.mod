<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class smTags extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {


    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $SM_siteManager;

        // sample javascript output
        $this->sayJS("function test() {\n\t// test function\n }\n",false);

        // create a new module. notice, returns a reference!!
        $mod1 =& $SM_siteManager->loadModule('rawHTML');
         
        // configure the module
        $op = "This is a test of the HTMLTITLE, HTMLBODY, JAVASCRIPT, and MODULE SM template tags:<br><br>
               The title of the page should be ROADSEND SITEMANAGER TEST SITE [htmltitle]<br>
               The background color of the page should be gray [htmlbody]<br>
               There should be a javascript function in the html source [javascript]<br>
               There should be a module on the page that says SM MODULE TAG TEST [module]<br>";
        $mod1->addDirective('output',$op);
         
        // run
        $output = $mod1->run();

        // output
        $this->say($output);

    }
    
}


?>
