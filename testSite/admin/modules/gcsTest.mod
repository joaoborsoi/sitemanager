<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class gcsTest extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {


    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $adminDir, $SM_siteManager;

        $this->saybr("This module tests the Global Configuration System. The following lines should match:<br>");

        $test1 = $adminDir."test/test2/";
        $test2 = $SM_siteManager->siteConfig->getVar('test','testDir');
        $test3 = $SM_siteManager->siteConfig->getVar('test','testDir2');
        $test4 = $SM_siteManager->siteConfig->getVar('test','testDir3');

        $this->saybr($test1);
        $this->saybr($test2);

        if ($test1 != $test2) {
            $this->say("<font color=\"red\"><b>The test failed!!</b></font>");
        }
        else {
            $this->saybr("<b>Test Passed</b><br>");
        }

        $this->saybr("<br>The second line was created in the XML configuration file using variable replacement with a SECTION backreference. See admin/config/localConfig.xsm for more info.<br>");

    }
    
}


?>
