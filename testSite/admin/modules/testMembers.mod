<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class testMembers extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        $this->addInVar('l',0,'int');

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $SM_siteManager;

        $this->saybr("<CENTER><B>MEMBER TEST</B></CENTER><BR>");

        // if they want to logout, do so here
        if ($this->getVar('l') == 1) {

            $this->sessionH->attemptLogout();

        }

        if (!$this->sessionH->isMember()) {

            $loginMod = $SM_siteManager->loadModule('userLogin');
            $output = $loginMod->run();

            // after running this module, if they are still not logged in, tell them.
            if (!$this->sessionH->isMember())            
                $this->saybr("You are not logged in. You may login below (entry in members table required)");

            $this->say($output);

        }
        else {

            $member = $this->sessionH->getMemberData();
            $this->say("You are logged in as: ".$member['userName']);
            $this->say(" [".$this->hLink($PHP_SELF.'?l=1','logout','blueViewSource').']');

        }


    }
    
}


?>
