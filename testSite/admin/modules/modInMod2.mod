<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class modInMod2 extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // Directives
        $this->addDirective('tableBgColor','#DDDDDD');

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        $this->saybr("This output is being displayed from modInMod2");
        $this->saybr($this->directive['testText']);

    }
    
}


?>
