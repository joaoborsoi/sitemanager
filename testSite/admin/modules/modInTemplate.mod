<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class modInTemplate extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // Directives
        $this->addDirective('tableBorder','1');
        $this->addDirective('tableBgColor','#eeeeee');

        $this->addInVar('SM_menuID');


    }

    // only load if in template section
    function canModuleLoad() {

        return ($this->getVar('SM_menuID') == 8);

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        $this->saybr("<BR>SM MODULE TAG TEST<BR>");

        $this->saybr("directives assigned through template: ".$this->directive['var1']);
        $this->saybr("directives assigned through template: ".$this->directive['var2']);

    }
    
}


?>
