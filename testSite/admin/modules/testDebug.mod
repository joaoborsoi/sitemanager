<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class testDebug extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        $this->addInVar('SM_debug');
        $this->addInVar('switch',0,'int');
        $this->addInVar('dbgmsg',0,'int');

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $PHP_SELF, $SM_siteManager, $HTTP_SERVER_VARS;        

        $this->saybr("<CENTER><B>DEBUG TEST</B></CENTER><BR>");

        $switch     = $this->getVar('switch');
        $SM_debug   = $this->getVar('SM_debug');

        if ($switch == 1) {
            $SM_debug = !$SM_debug;
            $this->sessionH->setPersistent('SM_debug',$SM_debug);
        }

        if ($SM_debug != 1) {
            $msg = 'Turn debug header ON';
        }
        else {
            $msg = 'Turn debug header OFF';
        }

        $this->saybr($this->hLink($PHP_SELF.'?switch=1',$msg));

        $this->saybr("<br>To help Roadsend and the SiteManager project grow, you can click the link below which will
                    send an email to Roadsend with a small amount of debug information (PHP VERSION, Operating System, Database Type). This
                    will help us guage how often and how SiteManager is being used. Feel free to view the code used to do this by looking 
                    in the testSite/admin/modules/testDebug.mod file. This is completely optional.<br>");

        if ($this->getVar('dbgmsg') != 1)        
            $this->saybr($this->hLink($PHP_SELF.'?dbgmsg=1',"Click here to help the SiteManager project and send a debug message to Roadsend"));

        if ($this->getVar('dbgmsg') == 1) {

            // ** here is the code to send a small amount of information to Roadsend for debugging purposes
            $mail  = "New Debug Message\n\n";
            $mail .= "SiteManager Version: ".SM_VERSION."\n";
            $mail .= "PHP Version        : ".PHP_VERSION."\n";
            $mail .= "Operating System   : ".PHP_OS."\n";
            $mail .= "Server Software    : ".$HTTP_SERVER_VARS['SERVER_SOFTWARE']."\n";
            $mail .= "Database Type      : ".$SM_siteManager->siteConfig->getVar('db','dbType')."\n";
            $mail .= "\n\n";

            mail('smsurvey@roadsend.com','[SiteManager Debug Survey]',$mail,"From: anonymous@roadsend.com\n");

            $this->saybr("the debug mail was sent, thanks!!");

        }

    }
    
}


?>
