<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class modInMod1 extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // Directives
        $this->addDirective('tableBgColor','#AAAAAA');

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $SM_siteManager;

        $this->saybr("This is modInMod1, which will load modInMod2 and display it's output below");

        // load it
        $mod2 =& $SM_siteManager->loadModule('modInMod2');

        // configure it
        $mod2->addDirective('testText','this text was configured from modInMod1');

        // run it
        $output = $mod2->run();

        // display it
        $this->say($output);

    }
    
}


?>
