<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class inVarSecurity extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        $this->addInVar('numInVar',0,'int');            
    
    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $PHP_SELF;

        $this->saybr("<CENTER><B>MODULE INVAR SECURITY TEST</B></CENTER><BR><BR>");

        $this->saybr("the current value of numInVar is: ".$this->getVar('numInVar'));

        $this->saybr("numInVar is set to be an integer. clicking below sets it to a string, which should generate an error<br>");

        $this->saybr($this->hLink($PHP_SELF."?numInVar=test",'click here to generate security error'));

    }
    
}


?>
