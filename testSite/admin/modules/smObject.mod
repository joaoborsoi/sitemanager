<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class smObject extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // Directives
        $this->addDirective('testDirective','functionality is broken!!');

        // default language is danish here
        //$this->addDirective('defaultLocale','da_DK');


    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        $this->saybr("<CENTER><B>SM OBJECT TESTS</B></CENTER><BR><BR>");

        $this->loadConfig('smObjectTest');

        $this->saybr("Directive Loading From XML File: The following text should say 'Test Value'. It will have been loaded from an XML SMOBJECT XSM file.<br>");
        $this->saybr($this->directive['testDirective']);

        $this->saybr('<br>');


        $this->saybr("<CENTER><B>LOCALIZATION TEST</B></CENTER><BR><BR>");

        $this->loadLanguage('testSite.lang');

        $this->saybr("A language file was loaded.<br>
                     ");

        global $SM_clientLocale;       
        if (is_array($SM_clientLocale)) {
            $this->saybr("Your detected locales: ".join(',',$SM_clientLocale));
        }
        else {
            $this->saybr("Your detected locale was: $SM_clientLocale");
        }

        $this->say('<br>');

        $bText = $this->getText('text1');         // browser detected language

        $eText = $this->getText('text1','en_US'); // test specification of english
        $dText = $this->getText('text1','da_DK'); // test specification of danish

        if (($eText == '') || ($dText == '')) {
            $this->saybr('<B>LANGUAGE LOAD FAILED!</B>');
        }

        $this->saybr("Browser Detected Language: $bText");

        $this->saybr("English: $eText");
        $this->saybr("Danish: $dText");

    }
    
}


?>
