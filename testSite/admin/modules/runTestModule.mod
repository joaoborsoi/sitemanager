<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class runTestModule extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // no table for us
        $this->addDirective('outputInTable',false);
        $this->addDirective('propagateInVars',true);

        // modulename to run
        $this->addInVar('moduleName');

        // directives to give to module
        $this->addInVar('directives','','string',false);

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $SM_siteManager, $PHP_SELF;

        // grab module, setup defaults
        $moduleName = $this->getVar('moduleName');
        $directives = $this->getVar('directives');

        if ($moduleName == '') {
            $moduleName = 'rawHTML';
            $directives = 'output#Select an item on the left to test';
        }

        // load the test module
        $testModule =& $SM_siteManager->loadModule($moduleName);
        
        // parse directives
        $dList = split('@',$directives);
        foreach ($dList as $keyVal) {
            $kv = split('#',$keyVal);
            (isset($kv[0])) ? $key = $kv[0] : $key = '';
            (isset($kv[1])) ? $val = $kv[1] : $val = '';            
            $testModule->addDirective($key, $val);
        }

        // run it
        $output = $testModule->run();

        // output option to view in debug mode
        $this->saybr('<center>'.$this->hLink($PHP_SELF.'?fatalDebug=1','(view fatal debug page)').'</center>');

        // output it
        $this->say($output);
        $this->sayJS($testModule->jsOutput);

    }
    
}


?>
