<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends baseMod class.
 *
 */

// include base module
global $SM_siteManager;
$SM_siteManager->includeModule('baseMod');

class extendedMod extends baseMod {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // configure
        parent::moduleConfig();

        // other configuration here...
        $this->directive['title'] = 'Extended Module Test';

    }

    /**
     * this function contains the core functionality entry point for the module.
     */
    function moduleThink() {        

        $this->saybr("This output is being displayed by the extended module. It is wrapped in a table generated by the base module");
        $this->saybr("The generation of the table was done automatically through preThinkList and postThinkList methods");

    }
    
}


?>
