<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class smModuleTest extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // Directives
        $this->addDirective('tableBorder','1');
        $this->addDirective('tableBgColor','#FFFFCC');
        $this->addDirective('tableWidth','80%');
        $this->addDirective('centerInTable',false);

        // InVars
        $this->addInVar('testInVar');

        // Styles
        $this->addStyle('testStyle');

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
           
        $this->saybr("<CENTER><B>MODULE TEST</B></CENTER><BR>");
        
        $this->saybr("This output is showing up in a module.");

        $this->saybr('The background color is yellow. The table width is 80%. It should not be centered.<br>');

        $testInVar = $this->getVar('testInVar');

        if ($testInVar != 'testValue') {
            $this->saybr('declared inVar \'testInVar\' was not set correctly - <b>PROBLEM WITH INVARS!</b>');
        }
        else {
            $this->saybr('declared inVar \'testInVar\' was set to: ['.$testInVar.'] (correctly)');
        }

        $this->saybr('this text should be blue and bold, using the \'testStyle\' CSS style',$this->getStyle('testStyle'));

    }
    
}


?>
