<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): pym
*
*/

/**
 * a skeleton module. extends base SM_module class.
 *
 */
class SM_fileUpload extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // Directives
        $this->addDirective('path','/tmp/');
        $this->addDirective('controlsOnRight',true);
        $this->addDirective('tableWidth','400');
        
    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
        global $PHP_SELF, $HTTP_POST_FILES;
        
        
        // New Smart Form
        $myForm =& $this->newSmartForm();
        
        // configure form
        $myForm->addDirective("requiredTag","");
        
        if ($this->directive['controlsOnRight'])
            $myForm->addDirective("controlsOnRight","true");
        
        
        // New smartform file upload
        $myForm->addDirective("enctype","multipart/form-data");
        $myForm->add('fileName','File Name','fileUpload',false,'',array('size'=>'20'),0);
        
        // run the form
        $myForm->runForm();

        // was data verified?        
        if ($myForm->dataVerified()) {
        
            // yes!
            $prefix = $this->modulePrefix;

            $fileName = $HTTP_POST_FILES['fileName_'.$prefix]['name'];
            $tmpName =  $HTTP_POST_FILES['fileName_'.$prefix]['tmp_name'];
            $this->saybr("$fileName has been added to ".$this->directive['path']);

            move_uploaded_file($tmpName,$this->directive['path'].$fileName);

            
        }
        else {
        
            // no! output the form
            $this->say($myForm->output());
            
        }

    }
    
}

?>
