<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a login module for logging users into the SiteManager Member System
 *
 */
class SM_userLogin extends SM_module {
     
     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // no config

    }

    function canModuleLoad() {

        // don't load if member system isn't turned on
        return $this->sessionH->directive['useMemberSystem'];

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $SM_siteManager;

        // create and configure the login form
        $myForm =& $this->newSmartForm();

        $un =& $myForm->add('userName','Username','text',true);
        $un->configure(array('size'=>'30','maxLength'=>'30'));

        $pw =& $myForm->add('passWord','Password','text',true);
        $pw->configure(array('size'=>'30','maxLength'=>'30','passWord'=>true));

        // run the form
        $myForm->runForm();

        // was data verified?        
        if ($myForm->dataVerified()) {
        
            // attempt to login
            if ($this->sessionH->attemptLogin($myForm->getVar('userName'),$myForm->getVar('passWord'))) {

                // they logged in succesfully
                $this->say("successful login!");

            }
            else {

                // unsuccessful login
                $this->say("Sorry, login was incorrect");
                $this->say($myForm->output("Login"));

            }

        }
        else {
        
            // no! output the form
            if (!$this->sessionH->isMember()) {
                $this->say($myForm->output("Login"));
            }
            
        }
    
    }
    
}


?>
