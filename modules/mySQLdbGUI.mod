<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/


// load base module
global $SM_siteManager;
$SM_siteManager->includeModule('baseDbEditor');

/**
 * A module to examine a database table structure and 
 * create a smart form that will be able to edit the table
 * see dbRecordSelector.mod
 */
class SM_mySQLdbGUI extends SM_baseDbEditor {

    /** (multihash) table definition stored here */
    var $tableDef;
                  
     /**
      * configure the module. run before moduleThink
      */        
    function moduleConfig() {
        
        // directives       
        $this->addDirective('maxVisibleSize',30);       // max size of a form element (default)     
        $this->addDirective('displayAsXML',false);       // display the form as XML     
        $this->addDirective('dumpTemplate',false);       // display the forms as a template
        $this->addDirective('dbOverRide');       // display the forms as a template    
                
        // parents
        parent::moduleConfig();

    }   

    // get table format
    // this will read a description of the table and
    // setup some data structures for later use
    function getTableFormat() {
                
        // ask for field list
        $SQL = "SHOW FIELDS FROM {$this->directive['tableName']}";
        $rh = $this->dbH->simpleQuery($SQL);
        if (DB::isError($rh)) {        
            SM_debugLog($SQL, $this);
            SM_fatalErrorPage("query failed");
        }

        // loop through each field and gather information
        while ($rr = $this->dbH->fetchRow($rh)) {
    
            // get initial name and type
            $fName      = $rr['Field'];
            $dataType   = $rr['Type'];
                                    
            // size
            (preg_match("/\\((\d+)(.*?)\\)/",$dataType,$m)) ? $size  = $m[1] : $size = 0;

            //if there was more after the number (i.e. a comma) it is probiblly a double,
            //float, etc that nees an extra space for the decimal point.
            if($m[2] !="" && $size!=0) {
                $size=$size+1;
            }
                            
            // max size is always size for this element
            $this->tableDef[$fName]['maxSize'] = $size;
                        
            // don't let shown size go past max size for this form
            if ($size > $this->directive['maxVisibleSize'])
                $this->tableDef[$fName]['size'] = $this->directive['maxVisibleSize'];
            else
                $this->tableDef[$fName]['size'] = $size;
            
            // type
            (preg_match("/^(\w+)\\(*.*\\)*\$/",$dataType,$m)) ? $type = trim($m[1]) : $type = 'unknown';
            $this->tableDef[$fName]['dataType'] = $type;
            
            // enum/set list
            if ($type == 'set' || ($type == 'enum'))
                (preg_match("/\\('(.+)'\\)/",$dataType,$m)) ? $set = $m[1] : $set = 0;
            else
                $set = 0;
            
            // can be null?
            if ($this->inVar['sType'] == 'remove') {
                // if we're in remove mode, none of these are required
                $this->tableDef[$fName]['required'] = false;
            }
            else {            
                ($rr['Null'] == 'YES') ? $this->tableDef[$fName]['required'] = false : $this->tableDef[$fName]['required'] = true;
            }

            // set the pretty name 
            $this->tableDef[$fName]['prettyName'] = SM_prettyName($fName);
            
            // if it's a primary key, don't let them change it
            if ($rr['Key'] == 'PRI')
                $this->tableDef[$fName]['display'] = false;
            else
                $this->tableDef[$fName]['display'] = true;
            
            
            // start off with this field not needing quotes in an SQL query by default  
            $this->tableDef[$fName]['needQuotes'] = false;
            
            // debug."foo"
            //echo "[ $fName, $type, $size, $null, $set ]<br>\n";
    
            // figure out which smart form element to use           
            switch ($type) {
            
                // text
                case 'char':
                case 'varchar':
                $this->tableDef[$fName]['needQuotes'] = true;
                
                case 'int':
                case 'smallint':
                case 'bigint':
                case 'tinyint':
                default:
                $this->tableDef[$fName]['sfType'] = 'text';
                
                break;
                
                // date
                case 'date':
                case 'datetime':                                
                $this->tableDef[$fName]['needQuotes'] = true;
                $this->tableDef[$fName]['sfType'] = 'date';
                break;
                
                // textarea
                case 'text':                
                $this->tableDef[$fName]['needQuotes'] = true;
                $this->tableDef[$fName]['sfType'] = 'textArea';
                break;
                                
                // select
                case 'set': 
                $this->tableDef[$fName]['multiple'] = true;
                case 'enum':                                
                $this->tableDef[$fName]['sfType'] = 'select';               
                $this->tableDef[$fName]['needQuotes'] = true;
                break;
                
                // ignore
                case 'unknown':
                case 'timestamp':               
                $this->tableDef[$fName]['sfType'] = 'none';
                $this->tableDef[$fName]['display'] = false;
                break;
            }

            // if it's a set of enum, populate the select list
            if ($set)
                $this->tableDef[$fName]['sfSet'] = explode("','",$set);

            // if it's a link to another table, change it's type to dbSelect
            // we tell this by looking for "_idxNum" on the end of a field name
            if (preg_match("/(\w+)_idxNum\$/",$fName,$m)) {
                $this->tableDef[$fName]['sfType']       = 'dbSelect';
                $this->tableDef[$fName]['tableName']        = $m[1];
                $this->tableDef[$fName]['dataField']        = 'idxNum';

                // handle dbOverride on table lookup
                if(!empty($this->directive['dbOverRide'])){
                    $this->tableDef[$fName]['tableName']  =  $this->directive['dbOverRide'].".".$m[1];
                }
            }
            
            
            // try to pick some more appropriate input entities based on it's name
            
            // STATE
            if (eregi('state',$fName))
                $this->tableDef[$fName]['sfType']       = 'stateList';
            
            // COUNTRY
            if (eregi('country',$fName))
                $this->tableDef[$fName]['sfType']       = 'countryList';
            
            // try to guess some filters to add based on it's name          
            
            // EMAIL
            if (eregi('email',$fName))
                $this->tableDef[$fName]['filter']['email'] = array();
                            
                    
        }
        
        
    }
    
    
    /**
     *  this function will configure fields within the form based on user input
     *  from directive scripts
     */
    function configureForm() {
    
        // loop through each field and see if we have any config directives for it
        foreach ($this->tableDef as $fName => $field) {
        
            // do we have a directive for this field?
            if (empty($this->directive[$fName]))
                continue;

            // yep, configure this field by merging their directives
            // with ours
            if (is_array($this->directive[$fName])) {
                $field = array_merge($field, $this->directive[$fName]);
                $this->tableDef[$fName] = $field;
            }
        }
    }
    

    /**
     * called before moduleThink() - MUST be defined!
     *
     */
    function defineForm() {

        // get the table format
        $this->getTableFormat();

        // configure the fields
        $this->configureForm();

        // if dbu flag is set, notify the user of a database update
        if ($this->inVar['dbu'] == '1') {       
            $this->myForm->setHeader("THE DATABASE HAS BEEN UPDATED");
        }
        else {
            // it wasn't set, so no database write. but, if it's not a first time load,
            // tell them there was a problem with the form
            if (!$this->myForm->ftl)
                $this->myForm->setHeader("THE DATABASE WAS NOT UPDATED -- THERE IS A PROBLEM WITH THE INPUT");
        }

        // alt colors
        $this->myForm->addDirective('rowColorAlt1','#EEEEEE');
        $this->myForm->addDirective('rowColorAlt2','#FFFFFF');

        // add hiddens
        $this->myForm->addDirective('hiddens',$this->directive['hiddens']);

        // if we've got a record we're viewing, load it up (if they dont want add)
        if (($this->inVar['rNum'] != 0)&&($this->inVar['sType'] != 'add')) {
            $SQL = "SELECT * FROM {$this->directive['tableName']} WHERE {$this->directive['dataField']}={$this->inVar['rNum']}";
            $rh = $this->dbH->simpleQuery($SQL);
            if (DB::isError($rh)) {            
                SM_debugLog($SQL, $this);
                SM_fatalErrorPage("query failed");
            }
            $rr = $this->dbH->fetchRow($rh);
        }
        
        // loop through each field we found
        foreach ($this->tableDef as $fName => $field) {
                                
            // if none, we ignore
            if ($field['sfType'] == 'none')
                continue;
            
            // if told not to display, skip it
            if ($field['display'] == false)
                continue;
            
            // add to smart form    
            ($this->inVar['sType'] == 'remove') ? $_type = 'presetText' : $_type = $field['sfType'];
            $ie = $this->myForm->add($fName, $field['prettyName'], $_type, $field['required'], $rr[$fName]);
        
            // when type 'remove'
            // do no further processing
            if ($this->inVar['sType'] == 'remove')
                continue;

            // if this field has a set, populate it
            if (is_array($field['sfSet'])) {
                foreach ($field['sfSet'] as $dI)
                    $ie->addOption($dI);
                // allow multiple?
                if ($field['multiple'])
                    $ie->configure(array('multiple'=>true));
            }
                
            // if this is a dbSelect or dbComboBox, set it up some more
            if (($field['sfType'] == 'dbSelect')||($field['sfType'] == 'dbComboBox')) {
                            
                // if table or viewField is blank, use current              
                if (empty($field['tableName']))
                    $field['tableName'] = $this->directive['tableName'];
                    
                if (empty($field['viewField'])) {
                    // get the second field from the assoc table and use that
                    $this->dbH->setFetchMode(DB_FETCHMODE_ORDERED);

                    $SQL = "SHOW FIELDS FROM ".$field['tableName'];
                    $rhField = $this->dbH->simpleQuery($SQL);
                    if (DB::isError($rhField)) {        
                        SM_debugLog($SQL, $this);
                        //SM_fatalErrorPage("DB SELECT TABLE DOES NOT EXIST".mysql_Error());
                    }
                    
                    // Ship the first row.
                    $rrField = $this->dbH->fetchRow($rhField);
                    
                    $rrField = $this->dbH->fetchRow($rhField);
                    
                    $field['viewField'] = $rrField[0];
                    $this->dbH->setFetchMode(DB_FETCHMODE_ASSOC);
                }
                
                // Not a valid foriegn key, just show a text box.
                if (empty($field['viewField'])) {
                    $this->myForm->remove($fName);
                    $this->myForm->add($fName,$field['prettyName']."_idxNum", 'text', $field['required'], $rr[$fName]);
                }

                $ie->configure(array('tableName'    =>$field['tableName'],
                                     'dataField'    =>$field['dataField'],
                                     'viewField'    =>$field['viewField'],
                                     'orderBy'      =>$field['orderBy'],
                                     'whereClause'  =>$field['whereClause'],
                                     'addNoOption'  =>$field['addNoOption']));

            }               

                        
            // if it's a date, set the year to be freeform
            if ($field['sfType'] == 'date') {
                $ie->configure(array('yearSelect'=>false));
            }
            
            // if we can limit by size (ie, text) do so here
            if ($field['sfType'] == 'text') {
                $ie->configure(array('size'=>$field['size'],'maxLength'=>$field['maxSize']));
            }
                        
            
            // finally, configure entity with potential user overriden directives
            $ie->configure($field['directive']);

            // if we have filters to add, do them here
            if (isset($field['filter'])) {
            
                foreach ($field['filter'] as $fType=>$fArgs) {
                
                    // add the filter
                    $ie->addFilter($fType);
                    
                    // add arguments if any
                    if (is_array($fArgs))
                        $ie->filterList[$fType]->configure($fArgs);
                
                }
            
            }
            
                        
            // commit
            $this->myForm->commit($fName, $ie);
            
        }               


        // include an xml format of the form.
        if ($this->directive['displayAsXML'] == true) {
            $xmlPage = SM_saveForm($this->myForm);

            $this->say("YOUR XML PAGE <BR> <FORM>");
            $this->say("<TEXTAREA ROWS=50 COLS='150'>");
            $this->say("$xmlPage");
            $this->say("</TEXTAREA>");
            $this->say("</FORM>");
        }
        
        if ($this->directive['dumpTemplate'] == true) {
            
            $this->saybr("TEMPLATE DUMP");
            $this->myForm->addDirective['dumpTemplate'];
            $this->myForm->buildTemplate();
            $dump = $this->myForm->template;

            $this->say("<TEXTAREA WRAP='VIRTUAL' ROWS=20 COLS='80'>");
            $this->say("$dump");
            $this->say("</TEXTAREA>");
            $this->say("</FORM>");


        }   
    }

    /**
     * Wrapper function for adding a directive to an entity on the smart form.
     * i.e. we have a databse field named myText that mySQLdbGuUI recognizes as
     * a 'textAreaEntity' and we want to make sure that the 'textAreaEntity' 
     * directive 'rows' was set to '15' insted of the default '5'. the call 
     * would look like this:
     *
     *  $mod->addEntityDirective('myText','rows','15');
     *
     * @param $entity - Name of the database field we are adding to
     * @param $key - directive key
     * @param $value - directive value
     */
    function addEntityDirective($entity, $key, $val){
        if(isset($this->directive[$entity]['directive'])) {
            $new = array_merge(array($key=>$val),$this->directive[$entity]['directive']);
            $this->addDirective($entity , array('directive'=>$new));
        } else {
            $this->addDirective($entity , array('directive'=>array($key=>$val)));
        }
    }
    
}

?>
