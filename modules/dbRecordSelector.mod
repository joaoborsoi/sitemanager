<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): Shannon Weyrick (weyrick@roadsend.com)
*
*/


/**
 * A module to select a particular record from a table. use in conjuction with
 * mySQLdbGUI to crate a system to edit any MySQL table with SmartForms, by readin
 * the table definition and creating a SmartForm on the fly
 */
class SM_dbRecordSelector extends SM_module {

     /**
      * configure the module. run before moduleThink
      */    
    function moduleConfig() {
        
        // directives       
        $this->addDirective('propagateInVars',false);
        $this->addDirective('tableName');   // table name to operate on     
        $this->addDirective('viewField');   // field to show
        $this->addDirective('dataField','idxNum');   // field to operate with (primary key)
        $this->addDirective('extraOptions');   // other options as array
        


        // layout
        $this->addDirective('tableBorder','1');
        $this->addDirective('formTableWidth','800');
        
    
        // incoming variable to let us know what record we're on
        // this is shared with the dbGUIModule
        $this->addInVar('rNum',0,'int');
        $this->addInVar('sType','add');
        
    }


     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
                        
        // generate a list to use to select a record
        $myForm =& $this->newSmartForm();
        
        // setup
        $myForm->addDirective('requiredTag',0);
        $myForm->addDirective('requiredStar','');
        $myForm->addDirective('controlsOnRight',true);
        $myForm->addDirective('tableWidth',$this->directive['formTableWidth']);
        
        // select list
        $sel =& $myForm->add('rNum','Select Record','dbSelect',true,$this->inVar['rNum']);
        $sel->configure(array(  'tableName'     =>$this->directive['tableName'],
                                'dataField'     =>$this->directive['dataField'],
                                'viewField'     =>$this->directive['viewField'],
                                'orderBy'       =>$this->directive['orderBy'],
                                'whereClause'   =>$this->directive['whereClause']));
        
        
        // action type
        if ($this->inVar['sType'] == '')
            $this->inVar['sType'] = 'add';
            
        $st =& $myForm->add('sType','Action','radio',true,$this->inVar['sType']);
        $st->addOption('add');
        $st->addOption('edit');
        $st->addOption('remove');
        if (!empty($this->directive['extraOptions'])) {
            $st->addDirective('optionBreak',"<BR>");
            foreach($this->directive['extraOptions'] as $newOption){
                $st->addOption($newOption);
            }

        }


        // run form     
        $myForm->runForm();
        $this->say($myForm->output("Select"));      
        
    }
    
}

?>
