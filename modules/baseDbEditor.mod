<?php

/*********************************************************************
*  Roadsend SiteManager
*  Copyright (c) 2001 Roadsend, Inc.(http://www.roadsend.com)
**********************************************************************
*
* This source file is subject to version 1.0 of the Roadsend Public
* License, that is bundled with this package in the file 
* LICENSE, and is available through the world wide web at 
* http://www.roadsend.com/license/rpl1.txt
*
**********************************************************************
* Author(s): weyrick
*
*/

/**
 * a base module for creating editiable database-based smartForms
 *
 */
class SM_baseDbEditor extends SM_module {
     
    // the form used in this module
    var $myForm;

     /**
      * configure the module. run before moduleThink
      */
    function moduleConfig() {

        // directives       
        $this->addDirective('propagateInVars',false);
        $this->addDirective('tableName','');            // table name to operate on
        $this->addDirective('dataField','idxNum');            // field to operate with (primary key)
                
        // incoming record indicator. shared with recordSelectorModule
        $this->addInVar('rNum',0,'int');
        $this->addInVar('sType');
        $this->addInVar('dbu');        // 'database has been updated' flag

        if (!isset($this->dbH)) {
            SM_fatalErrorPage("SM_mySQLdbGUI::moduleConfig() - database connection was not setup");
        }

        $this->myForm =& $this->newSmartForm();

        // must include function to define a form
        $this->preThinkList[] = 'defineForm';

    }

     /**
      * this function contains the core functionality entry point for the module.
      */
    function moduleThink() {
    
        global $SM_siteManager;

        // get edit type
        $sType = $this->inVar['sType'];

        // default to add
        if (empty($sType)) {
            $sType = 'add';
        }

        // if we're removing, add a confirm box
        if ($this->inVar['sType'] == 'remove') {

            $this->myForm->addDirective('requiredTag','');
            $rC =& $this->myForm->add('_removeConfirm','','checkBox',true);
            $rC->addOption('Are you SURE you wish to remove this item?','yes');
            $this->myForm->setLayout('_removeConfirm',SF_LAYOUT_SINGLE);

        }

        // run form     
        $this->myForm->runForm();
        
        // check for verification
        if ($this->myForm->dataVerified()) {

            if (($sType != 'add')&&($sType != 'edit')&&($sType != 'remove'))
                SM_fatalErrorPage("unrecognized type: $sType");
    
            // REMOVE

            if ($sType == 'remove') {

                // make sure they confirmed
                if ($this->myForm->getVar('_removeConfirm') != 'yes') {

                    // redisplay form
                    $this->myForm->setHeader("YOU MUST CHECK THE BOX TO CONFIRM REMOVAL");
                    $sMsg = SM_prettyName($this->inVar['sType']);
                    $this->say($this->myForm->output($sMsg,array('sType'=>$this->inVar['sType'],'rNum'=>$this->inVar['rNum'])));
                    return;
                }
                
                // support for external remove function
                if (method_exists($this->directive['removeObject'],$this->directive['removeFunction'])) {
                    $object = $this->directive['removeObject'];
                    $fName = $this->directive['removeFunction'];
                    $object->$fName($this->inVar['rNum']);
                }

                // verified, remove it
                $this->dbH->simpleQuery("DELETE FROM {$this->directive['tableName']} WHERE idxNum={$this->inVar['rNum']}");                
                // reload the page
                global $PHP_SELF;
                header("Location: $PHP_SELF?dbu=1&".$SM_siteManager->sessionH->getSessionVars());
                exit;

            }


            // ADD & EDIT

            if ($sType == 'edit')
                $SQL = "UPDATE {$this->directive['tableName']} SET \n";
            elseif ($sType == 'add')
                $SQL = "INSERT INTO {$this->directive['tableName']} SET \n";            
            
            // loop through the variables in the form and update the database
            $fList = $this->myForm->getVarList();
    
            // build the update query               
            foreach ($fList as $f) {
               
                // pull the value
                $val = $this->myForm->getVar($f);
                
                // see how we need to write out the variable in the query based on it's type
                if ($val=='' && $sType=='add') {
                    
                    // if it's empty and it can be null, "make it so"
                    // if Smart forms does it's job, this should always 
                    // this if statment should always be false
                    if ($this->tableDef[$f]['required']) { // ie, cannot be NULL
                        $data = "''";
                    }
                    else { // ie, can be NULL
                        continue;
                    }
                }
                else {
                    $data = "'".$val."'";
                }
                
// ***** DELETE ME ****
//$data = "'".$val."'";
                
                // append to SQL query  
                $SQL .= "$f=".$data.",\n";  
            
            }
            
            // chop off that final comma
            $SQL = substr($SQL, 0, -2);
            
            // add where clause if editing
            if ($sType == 'edit')
                $SQL .= " WHERE {$this->directive['dataField']}={$this->inVar['rNum']}";                        
                                            
            // execute the query
            $this->dbH->simpleQuery($SQL);
            
            // if it was an add, set them to edit the newly added record
            if ($sType == 'add')
                $this->inVar['rNum'] = mysql_insert_id();

            // reload the page
            global $PHP_SELF;
            header("Location: $PHP_SELF?rNum={$this->inVar['rNum']}&sType=edit&dbu=1&".$SM_siteManager->sessionH->getSessionVars());
            exit;
                    
        }
        else {
            $sMsg = SM_prettyName($sType);
            $this->say($this->myForm->output($sMsg,array('sType'=>$this->inVar['sType'],'rNum'=>$this->inVar['rNum'])));
            return;
        }

    }
    
}


?>
