#
# Table structure for table 'access'
#
# used for login/passwords for admin pages
#
CREATE TABLE access (
  idxNum smallint(5) unsigned DEFAULT '0' NOT NULL auto_increment,
  userName varchar(20),
  passWord varchar(20),
  level tinyint(3) unsigned,
  PRIMARY KEY (idxNum)
);
