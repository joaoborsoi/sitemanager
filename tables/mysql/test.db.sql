# MySQL dump 8.8
#
# Host: localhost    Database: test
#--------------------------------------------------------
# Server version	3.23.22-beta-log

#
# Table structure for table 'memberSessions'
#

CREATE TABLE memberSessions (
  idxNum int(10) unsigned NOT NULL auto_increment,
  uID char(32) DEFAULT '' NOT NULL,
  sID char(32) DEFAULT '' NOT NULL,
  dateCreated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  PRIMARY KEY (idxNum),
  UNIQUE uID (uID),
  UNIQUE sID (sID)
);

#
# Table structure for table 'members'
#

CREATE TABLE members (
  idxNum int(10) unsigned NOT NULL auto_increment,
  uID varchar(32) DEFAULT '' NOT NULL,
  userName varchar(30) DEFAULT '' NOT NULL,
  passWord varchar(30) DEFAULT '' NOT NULL,
  emailAddress varchar(60) DEFAULT '' NOT NULL,
  firstName varchar(50),
  lastName varchar(50),
  dateCreated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  PRIMARY KEY (idxNum),
  UNIQUE emailAddress (emailAddress),
  UNIQUE uid (uID),
  KEY userName (userName)
);

#
# Table structure for table 'news'
#

CREATE TABLE news (
  idxNum int(10) unsigned NOT NULL auto_increment,
  nDate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  news text,
  userName varchar(50),
  PRIMARY KEY (idxNum)
);

#
# Table structure for table 'sessions'
#

CREATE TABLE sessions (
  idxNum int(10) unsigned NOT NULL auto_increment,
  sessionID varchar(32) DEFAULT '' NOT NULL,
  hashID varchar(32) DEFAULT '' NOT NULL,
  createStamp datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  remoteHost varchar(20),
  dataKey varchar(255),
  dataVal varchar(255),
  PRIMARY KEY (idxNum),
  KEY createStamp (createStamp),
  KEY sessionID (sessionID),
  UNIQUE hashID (hashID)
);

#
# Table structure for table 'testTable'
#

CREATE TABLE testTable (
  idxNum int(10) unsigned NOT NULL auto_increment,
  firstName varchar(30),
  lastName varchar(30),
  phone varchar(30),
  addDate date,
  description text,
  PRIMARY KEY (idxNum)
);

